% xy error estimation
% the program grabs trajectories and calculates the error in the xy position calculation based on
% the assumption that displacement of the locus between 2 consecutive frames is negligeable

close all;
clear;

%% params
useConfidence = 1; % reject tracks with low confidence
minConf = 1.4; % define treshold for rejection
verbose = 0; % How talkative should the program be ?

%% load the files containing the trajectories

% jython produced file
adress = pwd;
folderName = uigetdir(pwd,'Select folder where all the _data.txt files are');
%dimension = 2;
cd(folderName)
filesInFolder = dir('*_data.txt');

traj_number = {};
for i = 1:length(filesInFolder)
	clear datas

	datas = importdata(filesInFolder(i).name);
	traj_number{i} = regexprep(filesInFolder(i).name,'yeast_ROI','traj ');
	traj_number{i} = regexprep(traj_number{i},'_data.txt','');

	data_all{i} = datas.data;
end
header = datas.textdata;
clear datas

% load settings
settings = importdata('condition_settings.txt');
pixelSize = settings.data(2)/1000; % in um


% mise a l'echelle et sauvegarde des trajectoires
for i=1:length(filesInFolder)
	track_all_locus{i} = data_all{i}(:,1:2)*pixelSize;
	track_all_nucleus{i} = data_all{i}(:,4:5)*pixelSize;
end

%% extraction des valeurs de confiance
for i=1:length(filesInFolder)
	track_all_conf{i} = data_all{i}(:,6);
end

cd ..
Folder = dir();
create = 0;
for i = 1:length(Folder)
	if strcmp(Folder(i).name,'results_analysis')
		create = 1;
		break
	end
end
if create ==0;mkdir results_analysis;end
cd results_analysis/


%% delete false trajectories
if useConfidence == 1
	deletedTraj = 0;
	%deletedTrajs = [''];
	deletedTrajectories = fopen('deleted_trajectories.txt', 'w');
	fprintf(deletedTrajectories, 'Deleted trajectories\n');
	for trackNbr = length(track_all_locus):-1:1
		if min(track_all_conf{trackNbr})<minConf
			if verbose>0; fprintf('%s\nmin(track_all_conf{trackNbr}=%d)\n\n',traj_number{trackNbr},min(track_all_conf{trackNbr})); end
			fprintf(deletedTrajectories, '%s\n',traj_number{trackNbr});
			deletedTraj = deletedTraj+1;
			track_all_locus(trackNbr)=[];
			traj_number(trackNbr)=[];
			%deletedTrajs = horzcat(deletedTrajs,int2str(trackNbr),' ');
		end
	end
	fprintf('%d trajectories got deleted (confidence test)\n',deletedTraj);
	% 	fprintf('deleted trajectories : %s \n',deletedTrajs);
	fclose(deletedTrajectories);
end

%% calcul de l'erreur en xy

errX = 99999;
errY = 99999;
clear sigX;
sigX = [];
sigY = [];
for trackNbr = 1:length(track_all_locus)
%  	sigXPerTraj(:,trackNbr) = track_all_locus{trackNbr}(1:end-1,1)-track_all_locus{trackNbr}(2:end,1);
%  	sigYPerTraj(:,trackNbr) = track_all_locus{trackNbr}(1:end-1,2)-track_all_locus{trackNbr}(2:end,2);
 	for n = 1:length(track_all_locus{trackNbr})-1
 		sigX(length(sigX)+1) = track_all_locus{trackNbr}(n,1)-track_all_locus{trackNbr}(n+1,1); 	
 		sigY(length(sigY)+1) = track_all_locus{trackNbr}(n,2)-track_all_locus{trackNbr}(n+1,2);
 	end
end
% for trackNbr = 1:length(track_all_locus)
% 	errXPerTraj = sum(abs(sigXPerTraj),1)/(size(sigX,1)*size(sigX,2));
% 	errYPerTraj = sum(abs(sigYPerTraj),1)/(size(sigY,1)*size(sigY,2));
% end
errX = mean(abs(sigX));
errY = mean(abs(sigY));
eucDist = mean(sqrt(sigX.^2+sigY.^2));
fprintf('mean error along X = %dum\n',errX);
fprintf('mean error along Y = %dum\n',errY);
fprintf('mean 2D distance = %dum\n',eucDist);

%% ecriture des resultats
loc_error = fopen('localisation_error.txt', 'w');
fprintf(loc_error,'mean error along X = %dum\n',errX);
fprintf(loc_error,'mean error along Y = %dum\n',errY);
fprintf(loc_error,'mean 2D distance = %dum\n',eucDist);
fclose(loc_error);


%% go back to original results folder
cd(folderName)






