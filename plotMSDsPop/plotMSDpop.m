function allMSDs = plotMSDpop()
% Load global MSDs from different data set and plot them together

clear;
close all;

%% Grab every MSD to be plotted
% jython produced file
folderName = uipickfiles('REFilter','_results','REDirs',1,'Prompt','Select _results folders');
allMSDs = {};
FOV = 0;
for i=1:length(folderName)
	cd(folderName{i});
	if exist('results_analysis_1third_MSD_for_fit','dir')
		FOV = FOV+1;
		cd results_analysis_1third_MSD_for_fit/
		load(strcat(pwd,'/MSDs.mat'))
		allMSDs{FOV}(1,:) = MSD_all.mean;
%		deltaTs = [200:200:299*200];
		allMSDs{FOV}(2,:) = deltaTs;
		clear MSD_all deltaTs;
	end
end


%% Plot MSDs together

% choose if you want a single color for n different population of MSDs or 1 color per MSD
% code_couleur = hsv(length(allMSDs)); % one color per MSD
pop1 = 2;
pop2 = 3;
for couleur = 1:length(allMSDs) % one color per population
	if couleur<=pop1
		code_couleur(couleur,:) = [0 0 1]; % blue
	elseif couleur<=pop1+pop2
		code_couleur(couleur,:) = [1 0 0]; % red
	end
end

figure;
subplot(2,1,1); % loglog graph
h(1) = loglog(allMSDs{1}(2,:),allMSDs{1}(1,:),'color',code_couleur(1,:));
hold on
for i = 2:length(allMSDs)
	h(i) = loglog(allMSDs{i}(2,:),allMSDs{i}(1,:),'color',code_couleur(i,:));
end
title('loglog plot of global MSD','FontSize',20);
xlabel('DeltaT(sec)','FontSize',12);ylabel('MSD(um^2)','FontSize',12);
legend([h(pop1),h(pop1+pop2)],'Not induced','Induced'); % add a legend for different pop
%legend(h(pop1),'Not ind'); % add a legend for different pop
% legend(h); % add a legend for different pop

subplot(2,1,2); % linear graph
plot(allMSDs{1}(2,:),allMSDs{1}(1,:),'color',code_couleur(1,:));
hold on
for i = 2:length(allMSDs)
	plot(allMSDs{i}(2,:),allMSDs{i}(1,:),'color',code_couleur(i,:));
end
title('linear plot of global MSD','FontSize',20);
xlabel('DeltaT(sec)','FontSize',12);ylabel('MSD(um^2)','FontSize',12);
% legend([h(pop1),h(pop1+pop2)],'Not ind','Ind'); % add a legend for different pop



end

