function allMSDs = plotMSDindiv()
% Load all the MSDs from different data set and plot them together

clear;
close all;

%% Grab every MSD to be plotted
% jython produced file
folderName = uipickfiles('REFilter','_results','REDirs',1,'Prompt','Select _results folders');
allMSDs = {};
FOV = 0;
for i=1:length(folderName)
	cd(folderName{i});
	if exist('results_analysis_1third_MSD_for_fit','dir')
		FOV = FOV+1;
		cd results_analysis_1third_MSD_for_fit/
		load(strcat(pwd,'/MSDs.mat'));
		%allMSDs{FOV}.indivMSD = MSD_traj.mean;
		%allMSDs{FOV}.deltaTs = deltaTs;
		allMSDs{FOV}.indivMSD = MSD_traj.mean(:,1:round(length(deltaTs)/3)); % third of the MSD
		allMSDs{FOV}.deltaTs = deltaTs(1,1:round(length(deltaTs)/3)); % third of the MSD
		allMSDs{FOV}.folderName =  regexprep(folderName{i},'.*/','');
		allMSDs{FOV}.alpha = alphas;
		allMSDs{FOV}.diffs = diffs;
		clear MSD_all deltaTs;
	end
end


%% Plot MSDs together

% choose if you want a single color for n different population of MSDs or 1 color per MSD
%  code_couleur = hsv(length(allMSDs)); % one color per MSD
pop1 = 2;
pop2 = 3;
for couleur = 1:length(allMSDs) % one color per population
	if couleur<=pop1
		code_couleur(couleur,:) = [0 0 1]; % blue
	elseif couleur<=pop1+pop2
		code_couleur(couleur,:) = [1 0 0]; % red
	end
end

figure;
% subplot(2,1,1); % loglog graph
for dataset = 1:size(allMSDs,2)
	h(dataset) = loglog(allMSDs{dataset}.deltaTs(1,:),allMSDs{dataset}.indivMSD(1,:),'color',code_couleur(dataset,:)); 
	% trajectory 1 is being plotted separately so it's color can be used for the legend
	hold on
	loglog(allMSDs{dataset}.deltaTs(1,:),allMSDs{dataset}.indivMSD(2:end,:),'color',code_couleur(dataset,:));
end
title('loglog plot of individual MSDs','FontSize',15);
xlabel('DeltaT(sec)','FontSize',12);ylabel('MSD(um^2)','FontSize',12);
leg = legend([h(pop1),h(pop1+pop2)],'Not induced','Induced','location','NorthWest'); % add a legend for different pop
% leg = legend(h(1), 'Induced','location','NorthWest'); % add a legend for different pop
set(leg,'FontSize',12); clear leg;



figure
% subplot(2,1,2); % linear graph
for dataset = 1:size(allMSDs,2)
	h(dataset) = plot(allMSDs{dataset}.deltaTs(1,:),allMSDs{dataset}.indivMSD(1,:),'color',code_couleur(dataset,:));
	% trajectory 1 is being plotted separately so it's color can be used for the legend
	hold on
	plot(allMSDs{dataset}.deltaTs(1,:),allMSDs{dataset}.indivMSD(2:end,:),'color',code_couleur(dataset,:));
end
title('linear plot of individual MSDs','FontSize',15);
xlabel('DeltaT(sec)','FontSize',12);ylabel('MSD(um^2)','FontSize',12);
leg =legend([h(pop1),h(pop1+pop2)],'Not induced','Induced','location','NorthWest'); % add a legend for different pop
% leg =legend(h(1), 'Induced','location','NorthWest')
set(leg,'FontSize',12); clear leg;



end
