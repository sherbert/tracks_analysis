

%function msd_call_jm_raw()

maxdt = 1500;
minmaxz = 1; %  0.4um depth
nbrTraj = 60;


for subTraj = 1:nbrTraj
	traj_full{subTraj} = teloR_4(:,subTraj*maxdt-maxdt+1:subTraj*maxdt+1)'/16.6;  % distance in um
end

for subTraj = 1:nbrTraj
	for dt = 1:maxdt
				if abs(traj_full{subTraj}(dt,3)) > minmaxz
					fake_conf{subTraj}(dt,1) = 1;
				else
					fake_conf{subTraj}(dt,1) = 10;
				end

% 		fake_conf{subTraj}(dt,1) = 10;
		track_all_maxInt{subTraj}(dt,1) = 10000;
		track_all_nucleus{subTraj}(dt,1:2) = [1;1];
	end
	traj_number{subTraj} = strcat('Traj_',int2str(subTraj));
	traj_full{subTraj} = traj_full{subTraj}(:,1:2);
end

full_data = {};
full_data.track_all_locus = traj_full;
full_data.track_all_conf = fake_conf;
full_data.track_all_maxInt = track_all_maxInt;
full_data.track_all_nucleus = track_all_nucleus;
full_data.traj_number = traj_number;

exp_params = {};
exp_params.timeStep = 0.5; % in sec 
exp_params.pixelSize = 1.0; % in um
exp_params.do3D = 0;

dumpFolder = strcat(pwd,'/teloR_4_results');
% mkdir(dumpFolder);
% cd(dumpFolder);

msd_analysis_locus_vBitbucket(full_data,exp_params,dumpFolder,'model_Hua');
