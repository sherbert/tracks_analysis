function plot_analysis_simu()
% 
% 

folderName = uigetdir(pwd,'Select the desired chromosome');
cd(folderName)
filesInFolder = dir();


%%%%%%%%%%%%%%%%%%%     PARAMS    %%%%%%%%%%%%%%%%%%%%%%%%%%%
segments = [100,9999];
%%%%%%%%%%%%%%%%%%%     PARAMS    %%%%%%%%%%%%%%%%%%%%%%%%%%%

if segments(1,2)>length(filesInFolder)
	segments(1,2) = length(filesInFolder)-2; % don't take into account folders '.' and '..'
end



workingFolders = regexprep(regexprep(folderName,'.*/',''),'_analysis','_segmentXX_analysis');
all_values = {};
position = 1;

for i = segments(1,1):segments(1,2)
	cd(regexprep(workingFolders,'XX',num2str(i)));
	load('fit_values_short.mat');
	all_values.segment(position) = i;
	all_values.alphas(position) = alphas;
	all_values.stdalphas(position) = std(alphas);
	all_values.diffs(position) = diffs;
	all_values.stddiffs(position) = std(diffs);
	
	clear alphas diffs
	position = position+1;
	cd ..
end

figure;
errorbar([segments(1,1):segments(1,2)]*5,all_values.alphas,all_values.stdalphas)
title('alphas and their stds along the chromosome','FontSize',15)
xlabel('Genomic position -kb','FontSize',10);
saveas(gcf,strcat('alphas.png'));
saveas(gcf,strcat('alphas.fig'));

figure;
errorbar([segments(1,1):segments(1,2)]*5,all_values.diffs,all_values.stddiffs)
title('Diffusion coefficients and their stds along the chromosome','FontSize',15)
xlabel('Genomic position -kb','FontSize',10);ylabel('Diffusion (um^2/sec^{alpha})','FontSize',10);
saveas(gcf,strcat('diffs.png'));
saveas(gcf,strcat('diffs.fig'));

end


