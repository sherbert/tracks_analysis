function [sd] = sd_calculation(data,min_average,noOverlap)
% makes the calculation of a MSD for one track
% alpha version of the MSD calculation
% recup sur internet :
% http://stackoverflow.com/questions/7489048/calculating-mean-squared-displacement-msd-with-matlab
% Sebastien Herbert



   
nData = size(data,1); % number of data points
numberOfDeltaT = floor(nData/min_average);

sd = {};
% calculate sd for all deltaT's

if noOverlap==1
	for dt = 1:numberOfDeltaT-1
		tp = 1;
		i = 1;
		while (tp+dt) <= nData
			deltaCoords(i,:) = data(tp+dt,:) - data(tp,:);
			if isnan(deltaCoords(i,1))
				tp=tp+1;
			else
				tp=tp+dt;
				i=i+1;				
			end
		end
		sd{dt} = sum(deltaCoords.^2,2); % dx^2+dy^2+dz^2 or dx^2+dy^2
		clear deltaCoords
	end
else
	for dt = 1:numberOfDeltaT-1
		deltaCoords = data(1+dt:end,:) - data(1:end-dt,:); % end - beginning !!
		sd{dt} = sum(deltaCoords.^2,2); % dx^2+dy^2+dz^2 or dx^2+dy^2
	end
end
end

