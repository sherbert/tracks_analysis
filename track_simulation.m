function [traj] = track_simulation(dim,maxstep,confinement_radius,NbrOfTracks,potentiel)

timeStep = 100;
pixelSize = 108;
dimensions = dim;
diffusion = 2e-3; %um2/sec

save('condition_settings_simu.mat', 'pixelSize', 'timeStep', 'dim','diffusion','maxstep','confinement_radius','NbrOfTracks','potentiel');


tracks = {};

for i = 1:NbrOfTracks
	tracks{i} = simulate_one_track(dim,maxstep,confinement_radius,diffusion,potentiel);
end

save('tracks.mat','tracks');

end



function traj = simulate_one_track(dim,maxstep,confinement_radius,diffusion,potentiel)

traj = zeros(maxstep,dim);
for step = 2:maxstep
	if strcmp(potentiel,'creneau') %confinement : potentiel creneau
		traj(step,:) = traj(step-1,:)+randn(1,dim)*diffusion;
		while sqrt(sum(traj(step,:).^2))>confinement_radius;
			traj(step,:) = traj(step-1,:)+randn(1,dim)*diffusion;
		end
		
	elseif strcmp(potentiel,'parabollic') %confinement : potentiel parabollic
		traj(step,:) = traj(step-1,:) + randn(1,dim)*diffusion - sign(traj(step-1,:)).*(traj(step-1,:)/confinement_radius).^2*diffusion;
	end
end

end