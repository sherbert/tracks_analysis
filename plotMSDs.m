
function plotMSDs(deltaTs,MSD_all,alpha,max_window,MSD_traj,B,traj_number,fit_curve)
% Plots 2 figures (log log/linear plots and precise linear plot + traj number)


forceAxis = 0;
% % set simu plots
% xLimitsLogLog = [deltaTs(1) max(deltaTs)];                           % Limits for the x axis (loglog plot)
% yLimitsLogLog = [1e-5 1e5];                        % Limits for the y axis (loglog plot)
% xLimitsLinear = [1 max(deltaTs)];                          % Limits for the x axis (linear plot)
% yLimitsLinear = [0 3];                        % Limits for the y axis (linear plot)

% set exp plots
xLimitsLogLog = [deltaTs(1) max(deltaTs)];                           % Limits for the x axis (loglog plot)
yLimitsLogLog = [1e-5 1e1];                        % Limits for the y axis (loglog plot)
xLimitsLinear = [1 max(deltaTs)];                          % Limits for the x axis (linear plot)
yLimitsLinear = [0 0.5];                        % Limits for the y axis (linear plot)



% log log
figure; 
% loglog axis, MSD for all DeltaT + fit
subplot(2,2,2)
loglog(deltaTs,MSD_all.mean);
hold on; loglog(fit_curve(:,1),fit_curve(:,2),'Color','r');
loglog(fit_curve(:,1),fit_curve(:,3),'Color','k');

if forceAxis == 1
	set(gca,'Xlim',xLimitsLogLog,'YLim',yLimitsLogLog)       % Set the limits
end

title('MSD (loglog)','FontSize',15)
xlabel('DeltaT(sec)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);
legend(1) = {sprintf('median(alpha) = %.2f',alpha)};
legend(2) = {'Fitted curve (mean) - Red'};
legend(3) = {'Fitted curve (median) - Black'};
legend(4) = {'Experimental curve - Blue'};
if forceAxis == 1
	text(xLimitsLogLog(1),yLimitsLogLog(2)/100,legend,'Color','k','BackGround','w','FontSize',7);
else
	text(deltaTs(2),MSD_all.mean(max_window),legend,'Color','k','BackGround','w','FontSize',7);
end
clear legend;
% loglog axis, MSD for all DeltaT + separate MSDs
subplot(2,2,1);
loglog(deltaTs,MSD_traj.mean'); hold on; loglog(deltaTs,MSD_all.mean,'LineWidth',2)
title('MSD (all traj loglog)','FontSize',15)
xlabel('DeltaT(sec)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);

if forceAxis == 1
	set(gca,'Xlim',xLimitsLogLog,'YLim',yLimitsLogLog)       % Set the limits
end
% linear axis, MSD for all DeltaT + fit
subplot(2,2,4);
% plot(deltaTs,MSD_all.mean);
shadedErrorBar(deltaTs,MSD_all.mean,MSD_all.std,'b');
hold on; plot(fit_curve(:,1),fit_curve(:,2),'Color','r');
plot(fit_curve(:,1),fit_curve(:,3),'Color','k');
if forceAxis == 1
	set(gca,'Xlim',xLimitsLinear,'YLim',yLimitsLinear)       % Set the limits
end
title('MSD (linear)','FontSize',15)
xlabel('DeltaT(sec)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);
legend(1) = {sprintf('median(B) = %.2eum/sec^{alpha}',B)};
legend(2) = {'Fitted curve (mean) - Red'};
legend(3) = {'Fitted curve (median) - Black'};
legend(4) = {'Experimental curve - Blue'};
if forceAxis == 1
	text(xLimitsLinear(1),yLimitsLinear(2)/2,legend,'Color','k','BackGround','w','FontSize',7);
else
	text(deltaTs(10),MSD_all.mean(max_window)*96/100,legend,'Color','k','BackGround','w','FontSize',7);
end
% linear axis, MSD for all DeltaT + separate MSDs
subplot(2,2,3);
plot(deltaTs,MSD_traj.mean'); hold on; plot(deltaTs,MSD_all.mean,'LineWidth',2)
if forceAxis == 1
	set(gca,'Xlim',xLimitsLinear,'YLim',yLimitsLinear)       % Set the limits
end
title('MSD (all traj linear)','FontSize',15)
xlabel('DeltaT(sec)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);

saveas(gcf,strcat('MSDs_loglog_lineaire.png'));
saveas(gcf,strcat('MSDs_loglog_lineaire.fig'));


%%%%%%% linear axis, MSD for all DeltaT + separate MSDs
figure;
plot(deltaTs,MSD_traj.mean'); 
hold on; plot(deltaTs,MSD_all.mean,'LineWidth',2);
if forceAxis == 1
	set(gca,'Xlim',xLimitsLinear,'YLim',yLimitsLinear)       % Set the limits
end
title('MSD (all traj linear)','FontSize',15)
xlabel('DeltaT(sec)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);

for i =1:size(MSD_traj.mean,1)
	for dt=length(MSD_traj.mean(i,:)):-1:1
		if ~isnan(MSD_traj.mean(i,dt))
			last_position = dt;
			break
		end
	end
	text(deltaTs(last_position),MSD_traj.mean(i,last_position),traj_number(i),'FontSize',12)
end
title('All trajectories');

saveas(gcf,strcat('All_trajectories.png'));
saveas(gcf,strcat('All_trajectories.fig'));

%%% fitted part only
figure;
plot(deltaTs(1:size(fit_curve,1)),MSD_traj.mean(:,1:size(fit_curve,1))'); 
hold on; plot(deltaTs(1:size(fit_curve,1)),MSD_all.mean(1:size(fit_curve,1)),'LineWidth',2);
if forceAxis == 1
	set(gca,'Xlim',xLimitsLinear,'YLim',yLimitsLinear)       % Set the limits
end
title('MSD (all traj linear)','FontSize',15)
xlabel('DeltaT(sec)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);

for i =1:size(MSD_traj.mean,1)
	for dt=length(MSD_traj.mean(i,:)):-1:1
		if ~isnan(MSD_traj.mean(i,dt))
			last_position = size(fit_curve,1);
			break
		end
	end
	text(deltaTs(last_position),MSD_traj.mean(i,last_position),traj_number(i),'FontSize',12)
end
title('All trajectories');

saveas(gcf,strcat('All_trajectories_fitted_part.png'));
saveas(gcf,strcat('All_trajectories_fitted_part.fig'));



end