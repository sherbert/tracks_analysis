
function msd_call_simu()
%% load the tracks.mat file that is containing simulated trajectories

clear;
close all;


%% jython produced file
[fileName,pathName,~] = uigetfile(pwd,'Select folder where all the tracks.mat file is','tracks.mat');
cd(pathName);
temp = load('tracks.mat');
track_all_locus = temp.tracks;
clear temp

traj_number = {};
for i = 1:length(track_all_locus)
	traj_number{i} = sprintf('simu%i',i);
end


%% load settings
settings_simu = load('condition_settings_simu.mat');
timeStep = settings_simu.timeStep/1000; % in msec
pixelSize = settings_simu.pixelSize/1000; % in um
if settings_simu.dim == 2
	do3D = 0;
elseif settings_simu.dim == 3 
	do3D = 1;
else 
	disp('Unknown number of dimensions')
end


%% mise a l'echelle et sauvegarde des trajectoires

full_data_1_locus = {};
track_all_conf = {};
track_all_maxInt = {};
track_all_nucleus = {};

for i = 1:length(track_all_locus)
	track_all_locus{i} = track_all_locus{i}*pixelSize;
	track_all_conf{i}(1:length(track_all_locus{i}),1) = Inf;
	track_all_maxInt{i}(1:length(track_all_locus{i}),1) = Inf;
	track_all_nucleus{i}(1:length(track_all_locus{i}),1:2) = 10*pixelSize;
end	

full_data_1_locus.track_all_conf = track_all_conf;
full_data_1_locus.track_all_locus = track_all_locus;
full_data_1_locus.track_all_maxInt = track_all_maxInt;
full_data_1_locus.track_all_nucleus = track_all_nucleus;
full_data_1_locus.traj_number = traj_number;

exp_params = {};
exp_params.timeStep = timeStep;
exp_params.pixelSize = pixelSize;
exp_params.do3D = do3D;


cd ..
dumpFolder = strcat(pathName(1:end-1),'_analysis');
msd_analysis_locus_vBitbucket(full_data_1_locus,exp_params,dumpFolder,'simu_numeric');

end