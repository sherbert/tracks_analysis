


for i =1:20
	cd('/media/Disk_3T_seb/simulation_hua/WT_30000pas_every/');
	simu_number = i;
	
	
	adress = strcat('./traj',num2str(simu_number),'/trajectory_analysis_noOverlap/', ...
		'MSD_simu_chr4_analysis/');
	cd(adress);
	
	cd('./MSD_simu_chr4_segment1_analysis/');
	temp = load('./MSDs_simu.mat');
	MSDs_4L_seg1.mean(simu_number,:) = temp.MSD_traj.mean;
	MSDs_4L_seg1.std(simu_number,:) = temp.MSD_traj.std;
	clear temp
	
	cd ..;
	cd('./MSD_simu_chr4_segment90_analysis/');
	temp = load('./MSDs_simu.mat');
	MSDs_CEN_seg90.mean(simu_number,:) = temp.MSD_traj.mean;
	MSDs_CEN_seg90.std(simu_number,:) = temp.MSD_traj.std;
	clear temp;
	
	cd ..;
	cd('./MSD_simu_chr4_segment200_analysis/');
	temp = load('./MSDs_simu.mat');
	MSDs_MIDDLE_seg200.mean(simu_number,:) = temp.MSD_traj.mean;
	MSDs_MIDDLE_seg200.std(simu_number,:) = temp.MSD_traj.std;
	clear temp;
	
	cd ..;
	cd('./MSD_simu_chr4_segment306_analysis/');
	temp = load('./MSDs_simu.mat');
	MSDs_4R_seg306.mean(simu_number,:) = temp.MSD_traj.mean;
	MSDs_4R_seg306.std(simu_number,:) = temp.MSD_traj.std;
	clear temp;
	
end










