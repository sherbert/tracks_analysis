



% linear errorbars of one traj for 4 segments
errorbar(MSDs_4L_seg1.mean(1,1:1000:end)',MSDs_4L_seg1.std(1,1:1000:end)','Color','b');
hold on; errorbar(MSDs_CEN_seg90.mean(1,1:1000:end)',MSDs_CEN_seg90.std(1,1:1000:end)','Color','k');
hold on; errorbar(MSDs_MIDDLE_seg200.mean(1,1:1000:end)',MSDs_MIDDLE_seg200.std(1,1:1000:end)','Color','g');
hold on; errorbar(MSDs_4R_seg306.mean(1,1:1000:end)',MSDs_4R_seg306.std(1,1:1000:end)','Color','r');
title('MSD (linear)','FontSize',15)
xlabel('DeltaT(UA)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);

% loglog plot of one traj for 4 segments
loglog(MSDs_4L_seg1.mean(1,:)','Color','b')
hold on;
loglog(MSDs_CEN_seg90.mean(1,:)','Color','k')
loglog(MSDs_MIDDLE_seg200.mean(1,:)','Color','g')
loglog(MSDs_4R_seg306.mean(1,:)','Color','r')
title('MSD (loglog)','FontSize',15)
xlabel('DeltaT(UA)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);

% loglog plot of the mean of all the traj for 4 segments
loglog(mean(MSDs_4L_seg1.mean),'Color','b')
hold on;
loglog(mean(MSDs_CEN_seg90.mean),'Color','k')
loglog(mean(MSDs_MIDDLE_seg200.mean),'Color','g')
loglog(mean(MSDs_4R_seg306.mean),'Color','r')
title('MSD (loglog)','FontSize',15)
xlabel('DeltaT(UA)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);


% loglog plot of 3 trajs for 1 segment
loglog(MSDs_MIDDLE_seg200.mean(1:2,:)');
title('MSD (loglog) MIDDLE\_seg200','FontSize',15)
xlabel('DeltaT(UA)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);


