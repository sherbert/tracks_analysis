
function msd_call_Sage()
%% load the tracks.mat file that is containing trajectories produced by Sage's program

clear;
close all;


%% jython produced file
folderName = uigetdir(pwd,'Select folder where all the .txt files are');
cd(folderName)
filesInFolder = dir('*.txt');

traj_number = {};
for i = 1:length(filesInFolder)
	clear datas
	datas = importdata(filesInFolder(i).name);
	traj_number{i} = regexprep(filesInFolder(i).name,'ROI','traj ');
	traj_number{i} = regexprep(traj_number{i},'.txt','');
	data_all{i} = datas.data;
end
header = datas.textdata;
clear datas



%% load settings
timeStep = 0.1; % in sec
pixelSize = 0.108; % in um
do3D = 2;


%% mise a l'echelle et sauvegarde des trajectoires

full_data_1_locus = {};
track_all_conf = {};
track_all_maxInt = {};
track_all_nucleus = {};

for i = 1:length(data_all)
	track_all_locus{i} = data_all{i}(:,2:3)*pixelSize;
	track_all_conf{i}(1:length(data_all{i}),1) = data_all{i}(:,4)*100; % To be taken into account eventually
	track_all_maxInt{i}(1:length(data_all{i}),1) = data_all{i}(:,5); % To be taken into account eventually
	track_all_nucleus{i}(1:length(data_all{i}),1:2) = 10*pixelSize; % Not acquired
end	

full_data_1_locus.track_all_conf = track_all_conf;
full_data_1_locus.track_all_locus = track_all_locus;
full_data_1_locus.track_all_maxInt = track_all_maxInt;
full_data_1_locus.track_all_nucleus = track_all_nucleus;
full_data_1_locus.traj_number = traj_number;

exp_params = {};
exp_params.timeStep = timeStep;
exp_params.pixelSize = pixelSize;
exp_params.do3D = do3D;


cd ..
dumpFolder = strcat(folderName,'_analysis');
msd_analysis_locus_vBitbucket(full_data_1_locus,exp_params,dumpFolder,'Sage');

end