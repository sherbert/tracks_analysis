
function msd_call_exp_v1p00()
%% load the files produced by the jython program (Fiji), that are containing the experimental 
%trajectories :

clear;
close all;

% isOpen = matlabpool('size');
% if isOpen==0
%     matlabpool(8);
% end

%% jython produced file
folderName = uigetdir(pwd,'Select folder where all the _data.txt files are');
cd(folderName)
filesInFolder = dir('*_data.txt');

traj_number = {};
for i = 1:length(filesInFolder)
	clear datas
	datas = importdata(filesInFolder(i).name);
	traj_number{i} = regexprep(filesInFolder(i).name,'yeast_ROI','traj ');
	traj_number{i} = regexprep(traj_number{i},'_data.txt','');
	data_all{i} = datas.data;
end
header = datas.textdata;
clear datas

%% load settings
settings = importdata('condition_settings.txt');
timeStep = settings.data(1)/1000; % in msec
pixelSize = settings.data(2)/1000; % in um
do3D = settings.data(3); % 0 for no, 1 for yes


%% mise a l'echelle et sauvegarde des trajectoires

% import only a part of the trajectory
frameEnd = 600;
frameEnd = min(frameEnd,size(data_all{1},1)); % test if frameEnd is not superior to the number of time point

if do3D == 1
 	for i=1:length(filesInFolder)
		track_all_locus{i} = data_all{i}(1:frameEnd,1:3).*pixelSize;
		track_all_nucleus{i} = data_all{i}(1:frameEnd,4:5).*pixelSize;
		
 	end
elseif do3D == 0
	for i=1:length(filesInFolder)
		track_all_locus{i} = data_all{i}(1:frameEnd,1:2)*pixelSize;
		track_all_nucleus{i} = data_all{i}(1:frameEnd,4:5)*pixelSize;
	end
else
	disp('In which dimension are you living ???')
	return
end

%% extraction des valeurs de confiance
for i=1:length(filesInFolder)
	% track_all_conf{i} = data_all{i}(1:frameEnd,6); % uses the intensity ratio
	track_all_conf{i} = data_all{i}(1:frameEnd,8); % uses the SNRmax
end

%% extraction des valeurs d'intensite max
for i=1:length(filesInFolder)
	track_all_maxInt{i} = data_all{i}(1:frameEnd,7);
end


full_data_1_locus = {};
full_data_1_locus.track_all_conf = track_all_conf;
full_data_1_locus.track_all_locus = track_all_locus;
full_data_1_locus.track_all_maxInt = track_all_maxInt;
full_data_1_locus.track_all_nucleus = track_all_nucleus;
full_data_1_locus.traj_number = traj_number;

exp_params = {};
exp_params.timeStep = timeStep;
exp_params.pixelSize = pixelSize;
exp_params.do3D = do3D;


cd ..
workingFolder = regexprep(folderName,'.*/','');
dumpFolder = strcat(workingFolder,'_analysis');

msd_analysis_locus_vBitbucket(full_data_1_locus,exp_params,dumpFolder,'experimental');

% matlabpool close;


end