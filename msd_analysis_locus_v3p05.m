
function msd_analysis_locus_v3p05()
% The program recquires trajectories of equal length

close all;
clear;

%%params
useConfidence = 1; % reject tracks with low confidence
minConf = 1.4; % define treshold for rejection
min_averaging = 1; % minimum number of windows
DParam = 2; % number of timepoints used for the calculation of the diffusion coefficient D
alphaParam = 3; % 1/alphaParam is the proportion of the total trajectory used for the calculation of the power low aplha
dumpAllMSDs = 0; % write all MSDs in a new file... not yet there
verbose = 0; % How talkative should the program be ?
useNucDisp = 0; % use the center of the nucleoplasm to correct for a nucleus drift
errorMeasure = 1; % calculate the localisation error
simulated_data = 0; % calculate supplementary informations of measured vs absolute values

%% load the files containing the trajectories :

% test file
% - track_all : contains locus x,y (z in case of 3D) position (in micron) vs time(frame)
% - timestep (msec)
% load('rand_tracks.mat'); 

% jython produced file
adress = pwd;
folderName = uigetdir(pwd,'Select folder where all the _data.txt files are');
%dimension = 2;
cd(folderName)
filesInFolder = dir('*_data.txt');

traj_number = {};
for i = 1:length(filesInFolder)
	clear datas
	datas = importdata(filesInFolder(i).name);
	traj_number{i} = regexprep(filesInFolder(i).name,'yeast_ROI','traj ');
	traj_number{i} = regexprep(traj_number{i},'_data.txt','');
	data_all{i} = datas.data;
end
header = datas.textdata;
clear datas

% load settings
settings = importdata('condition_settings.txt');
timeStep = settings.data(1)/1000; % in msec
pixelSize = settings.data(2)/1000; % in um
do3D = settings.data(3); % 0 for no, 1 for yes


% mise a l'echelle et sauvegarde des trajectoires
if do3D == 1
 	for i=1:length(filesInFolder)
		track_all_locus{i} = data_all{i}(:,1:3).*pixelSize;
		track_all_nucleus{i} = data_all{i}(:,4:5).*pixelSize;
 	end
elseif do3D == 0
	for i=1:length(filesInFolder)
		track_all_locus{i} = data_all{i}(:,1:2)*pixelSize;
		track_all_nucleus{i} = data_all{i}(:,4:5)*pixelSize;
	end
else
	disp('In which dimension are you living ???')
	return
end

% % mise a l'echelle
% for i=1:length(filesInFolder)
% 	track_all_locus{i} = data_all{i}(:,1:5).*pixelSize;
% end

% extraction des valeurs de confiance
for i=1:length(filesInFolder)
	track_all_conf{i} = data_all{i}(:,6);
end

% extraction des valeurs d'intensite
for i=1:length(filesInFolder)
	track_all_maxInt{i} = data_all{i}(:,7);
end


cd ..
workingFolder = regexprep(folderName,'.*/','');
dumpFolder = strcat(workingFolder,'_analysis');
Folder = dir();
for i = 1:length(Folder)
	if strcmp(Folder(i).name,dumpFolder)
		disp('deleting old results');
		rmdir(dumpFolder,'s');
		break
	end
end
 mkdir(dumpFolder)
 cd(dumpFolder)

%% open output_MSD_analysis.txt file
Output = fopen('output_MSD_analysis.txt', 'w');
fprintf(Output, 'Program version = %s\n', mfilename);
fprintf(Output, 'Use the confidence parameter : %d\n', useConfidence);
fprintf(Output, 'Minimum value of the confidence parameter : %d\n', minConf);
fprintf(Output, 'Use the nucleoplasm center to correct the drift = %d\n', useNucDisp);
fprintf(Output, 'Maximum time window is 1/%d of the complete trajectory\n', min_averaging);
fprintf(Output, 'Number of timepoints used for the calculation of the diffusion coefficient D : %d\n', DParam);
fprintf(Output, 'Proportion of the total trajectory used for the calculation of the power low aplha : 1/%d\n', alphaParam);
fprintf(Output, 'Write all MSDs in a new file : %d\n', dumpAllMSDs);
fprintf(Output, 'Interframe time (in msec): %d\n', timeStep);
fprintf(Output, 'Pixel size (in um) : %d\n', pixelSize);
fprintf(Output, '3D images : %d\n', do3D);
fprintf(Output, 'Number of trajectories (total) = %d\n', length(track_all_locus));
fprintf(Output, 'Measure localisation error : %d\n', errorMeasure);



%% delete tracks with confidence coefficient too low (<=> MAX(locus)/MEAN(nucleoplasm)<minConf)
if useConfidence == 1
	deletedTraj = 0;
	%deletedTrajs = [''];
	deletedTrajectories = fopen('deleted_trajectories.txt', 'w');
	fprintf(deletedTrajectories, 'Deleted trajectories\n');
	for trackNbr = length(track_all_locus):-1:1
		if min(track_all_conf{trackNbr})<minConf
			if verbose>0; fprintf('%s\nmin(track_all_conf{trackNbr}=%d)\n\n',traj_number{trackNbr},min(track_all_conf{trackNbr})); end
			fprintf(deletedTrajectories, '%s\n',traj_number{trackNbr});
			deletedTraj = deletedTraj+1;
			track_all_locus(trackNbr)=[];
			traj_number(trackNbr)=[];
			track_all_nucleus(trackNbr)=[];
			%deletedTrajs = horzcat(deletedTrajs,int2str(trackNbr),' ');
		end
	end
	fprintf('%d trajectories got deleted (confidence test)\n', deletedTraj);
	fprintf(Output, 'Number of trajectories (kept) = %d\n', length(track_all_locus));
	fprintf(Output, 'Number of trajectories (deleted) = %d\n', deletedTraj);
	% 	fprintf('deleted trajectories : %s \n',deletedTrajs);
	fclose(deletedTrajectories);
end


%% Fill in any gaps in track with NaN positions : external function

%% Correct for nucleus displacement
if useNucDisp == 1
	track_all_locus = drift_correction(track_all_locus,track_all_nucleus);
end

%% Measure the localisation error
if errorMeasure ==1
	errorMeasurement(track_all_locus,track_all_conf,timeStep,track_all_maxInt,pixelSize,simulated_data);
end

%% Calcul of MSD
%MSD_calculation();

% Find length of longest track and the maximum step number
% that will be calculated
max_length = 1;
for trackNbr = 1:length(track_all_locus)
	if (length(track_all_locus{trackNbr})>max_length)
		max_length=length(track_all_locus{trackNbr});
	end	
end
max_window = floor(max_length/min_averaging);
timeBar = [timeStep:timeStep:timeStep*max_window];
clear trackNbr;


SD_track = cell(length(track_all_locus),1); % squared displacement NOT averaged
SD_DeltaT = cell(max_window,1);

%for each track all SDs are measured
tic
disp('SD calculation = ')
for trackNbr = 1:length(track_all_locus)
	current_track = track_all_locus{trackNbr};
	SD_track{trackNbr} = sd_calculation(current_track,min_averaging);
	for deltaT = 1:length(SD_track{trackNbr})
		SD_DeltaT{deltaT} = [SD_DeltaT{deltaT};SD_track{trackNbr}{deltaT}];		
	end
end
clear trackNbr;
toc

% overall MSD calculation
tic
disp('overall MSD calculation = ')
for j = 1:max_window
	MSD_all.mean(j) = mean(SD_DeltaT{j});
	MSD_all.std(j) = std(SD_DeltaT{j});
 	MSD_all.median(j) = median(SD_DeltaT{j});
 	MSD_all.iqr(j) = iqr(SD_DeltaT{j});	
end
toc

% single trajectories MSDs calculation
tic
disp('single trajectories MSDs calculation = ')
for i = 1:length(track_all_locus)
	for j = 1:length(SD_track{i})
		MSD_traj.mean(i,j) = mean(SD_track{i}{j});
		MSD_traj.std(i,j) = std(SD_track{i}{j});
	end
end
toc

% % mean MSD calculation => is the same as long as the samples are the same size !!!
% tic
% disp('mean MSD calculation = ')
% mean_MSD = mean(MSD_traj.mean,);
% 	
	
%% calculation of the mean power law and diffusion coefficient
D = (MSD_all.mean(1+DParam)-MSD_all.mean(1))/(timeBar(1+DParam)-timeBar(1));
alphaLength = ceil(min_averaging*length(timeBar)/alphaParam);
alpha = log(MSD_all.mean(alphaLength)/MSD_all.mean(1))/log(timeBar(alphaLength)/timeBar(1));

fprintf(Output, 'Mean alpha coefficient : %d\n', alpha);
fprintf(Output, 'Mean diffusion coefficient (um^2/sec): %d\n', D);

%% Dump calculated values
% dumpMSDs();

%% plot MSD
plotMSDs(timeBar,MSD_all,alpha,max_window,MSD_traj,D,traj_number);

%% autocorrelation of angles


%% bayesian comparison of MSD versus model (free diffusion, constraint and subdiffusion)


%% sliding window for switching behaviour


%% closing the function
cd(folderName)
fclose(Output);

disp('Done !')

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%      End of the main program
%%%%%%%%%%%%%%%%      Beginning of the related functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%
% function dumpMSDs(MSDs, MSDmean)
% % 
% %   Detailed explanation goes here
% 
% 
% end


function errorMeasurement(track_all_locus,track_all_conf,timeStep,track_all_maxInt,pixelSize,simulated_data)
%% calcul de l'erreur en xy

clear sig*;
clear timeStamp;
sigX = [];
sigY = [];
for trackNbr = 1:length(track_all_locus)
 	for timePoint = 1:length(track_all_locus{trackNbr})-1
		sigConf(trackNbr,timePoint) = track_all_conf{trackNbr}(timePoint);
		sigMaxInt(trackNbr,timePoint) = track_all_maxInt{trackNbr}(timePoint);
		timeStamp(trackNbr,timePoint) = timePoint*timeStep;
		
		sigX(trackNbr,timePoint) = track_all_locus{trackNbr}(timePoint,1)-track_all_locus{trackNbr}(timePoint+1,1);
		sigY(trackNbr,timePoint) = track_all_locus{trackNbr}(timePoint,2)-track_all_locus{trackNbr}(timePoint+1,2);
		eucDists(trackNbr,timePoint) = sigX(trackNbr,timePoint)^2+sigY(trackNbr,timePoint)^2;
		%sigX(length(sigX)+1) = track_all_locus{trackNbr}(timePoint,1)-track_all_locus{trackNbr}(timePoint+1,1);
		%sigY(length(sigY)+1) = track_all_locus{trackNbr}(timePoint,2)-track_all_locus{trackNbr}(timePoint+1,2);
 	end
end
distX = mean(abs(sigX(:)));
distY = mean(abs(sigY(:)));
eucDist = mean(sqrt(sigX(:).^2+sigY(:).^2));
fprintf('mean dist along X = %dum\n',distX);
fprintf('mean dist along Y = %dum\n',distY);
fprintf('mean 2D distance = %dum\n',eucDist);

mean_deltaX = mean(sigX(:));
mean_deltaY = mean(sigY(:));
std_deltaX = std(sigX(:));
std_deltaY = std(sigY(:));
%mean_eucDelta = mean(sqrt(sigX(:).^2+sigY(:).^2));
%std_eucDelta = std(sqrt(sigX(:).^2+sigY(:).^2));
fprintf('mean delta along X (bias) = %dum\n',mean_deltaX);
fprintf('mean delta along Y (bias) = %dum\n',mean_deltaY);
fprintf('std delta along X = %dum\n',std_deltaX);
fprintf('std delta along Y = %dum\n',std_deltaY);
fprintf('FWHM delta along X = %dum\n',(std_deltaX*2.354));
fprintf('FWHM delta along Y = %dum\n',(std_deltaY*2.354));
fprintf('mean FWHM 2D = %dum\n',((std_deltaY+std_deltaX)*2.354/2));

%% plot hist of errors
figure; hist(sigX(:),100); title('Sigma X','FontSize',15);
xlabel('Distance(um)','FontSize',10); ylabel('Population','FontSize',10);
saveas(gcf,strcat('Sigma_X.png'));
saveas(gcf,strcat('Sigma_X.fig'));

figure; hist(sigY(:),100); title('Sigma Y','FontSize',15);
xlabel('Distance(um)','FontSize',10); ylabel('Population','FontSize',10);
saveas(gcf,strcat('Sigma_Y.png'));
saveas(gcf,strcat('Sigma_Y.fig'));

figure; hist(eucDists(:),100); title('Interframe distance','FontSize',15);
xlabel('Distance(um)','FontSize',10); ylabel('Population','FontSize',10);
saveas(gcf,strcat('Interframe_distance.png'));
saveas(gcf,strcat('Interframe_distance.fig'));

figure; plot(timeStamp,eucDists,'*'); title('Dist vs time','FontSize',15);
xlabel('Time(sec)','FontSize',10); ylabel('Distance(um)','FontSize',10);
saveas(gcf,strcat('Dist_vs_time.png'));
saveas(gcf,strcat('Dist_vs_time.fig'));

figure; plot(sigConf,eucDists,'*'); title('Dist vs Intensity ratio (maxLocus/meanNuc)','FontSize',15);
xlabel('Intensity ratio','FontSize',10); ylabel('Distance(um)','FontSize',10);
saveas(gcf,strcat('Dist_vs_Iratio.png'));
saveas(gcf,strcat('Dist_vs_Iratio.fig'));

figure; plot(sigMaxInt,eucDists,'*'); title('Dist vs Intensity Locus max','FontSize',15);
xlabel('Locus Max intensity','FontSize',10); ylabel('Distance(um)','FontSize',10);
saveas(gcf,strcat('Dist_vs_Imax.png'));
saveas(gcf,strcat('Dist_vs_Imax.fig'));

% boxplots (average for 50bins) of distances, Iratio and Imax as a function of time
% downRatio
downRatio = 50;
miniSize = size(eucDists,2)-mod(size(eucDists,2),downRatio);
miniEucDists = reshape(eucDists(:,1:miniSize),[],floor(size(eucDists,2)/downRatio));
miniTimeStamp = timeStamp(1,5:downRatio:end);
miniTimeStamp = miniTimeStamp(1:size(miniEucDists,2));
figure;boxplot(miniEucDists,miniTimeStamp,'symbol',''); title('Dist vs time','FontSize',15);
set(gca,'YLim',[0  max(iqr(miniEucDists))*5]);
xlabel('Time(sec)','FontSize',10); ylabel('Distance(um)','FontSize',10);
saveas(gcf,strcat('Dist_vs_time_boxplots.png'));
saveas(gcf,strcat('Dist_vs_time_boxplots.fig'));

miniSigConf = reshape(sigConf(:,1:miniSize),[],floor(size(sigConf,2)/downRatio));
figure; boxplot(miniSigConf,miniTimeStamp,'symbol',''); title('Intensity ratio (maxLocus/meanNuc) vs time','FontSize',15);
xlabel('Time(sec)','FontSize',10); ylabel('Intensity ratio','FontSize',10);
saveas(gcf,strcat('Iratio_vs_time.png'));
saveas(gcf,strcat('Iratio_vs_time.fig'));

miniSigMax = reshape(sigMaxInt(:,1:miniSize),[],floor(size(sigConf,2)/downRatio));
figure; boxplot(miniSigMax,miniTimeStamp,'symbol',''); title('Locus Max Intensity vs time','FontSize',15);
xlabel('Time(sec)','FontSize',10); ylabel('Locus Max intensity','FontSize',10);
saveas(gcf,strcat('Imax_vs_time.png'));
saveas(gcf,strcat('Imax_vs_time.fig'));


%% ecriture des resultats
if simulated_data==1
	errorplot;
end

loc_error = fopen('localisation_error.txt', 'w');
fprintf(loc_error,'mean dist along X = %dum\n',distX);
fprintf(loc_error,'mean dist along Y = %dum\n',distY);
fprintf(loc_error,'mean 2D distance = %dum\n',eucDist);
fprintf(loc_error,'mean delta along X (bias) = %dum\n',mean_deltaX);
fprintf(loc_error,'mean delta along Y (bias) = %dum\n',mean_deltaY);
fprintf(loc_error,'std delta along X = %dum\n',std_deltaX);
fprintf(loc_error,'std delta along Y = %dum\n',std_deltaY);
fprintf(loc_error,'FWHM delta along X = %dum\n',(std_deltaX*2.354));
fprintf(loc_error,'FWHM delta along Y = %dum\n',(std_deltaY*2.354));
fprintf(loc_error,'mean FWHM 2D = %dum\n',((std_deltaY+std_deltaX)*2.354/2));

fclose(loc_error);
end


function track_all_locus_corrected = drift_correction(track_all_locus,track_all_nucleus)
% Corrects the locus position for nucleoplasm xy drift
% center nucleoplasm positions around 0.0
for trackNbr = 1:length(track_all_locus)
	track_all_nucleus_centered{trackNbr}(:,1) = track_all_nucleus{trackNbr}(:,1)-track_all_nucleus{trackNbr}(1,1);
	track_all_nucleus_centered{trackNbr}(:,2) = track_all_nucleus{trackNbr}(:,2)-track_all_nucleus{trackNbr}(1,2);
end
% apply correction to the locus position
for trackNbr = 1:length(track_all_locus)
	track_all_locus_corrected{trackNbr}(:,1) = track_all_locus{trackNbr}(:,1)-track_all_nucleus_centered{trackNbr}(:,1);
	track_all_locus_corrected{trackNbr}(:,2) = track_all_locus{trackNbr}(:,2)-track_all_nucleus_centered{trackNbr}(:,2);
end
end


function plotMSDs(timeBar,MSD_all,alpha,max_window,MSD_traj,D,traj_number)
% Plots 2 figures (log log/linear plots and precise linear plot + traj number)
% log log
figure; 
subplot(2,2,2)
loglog(timeBar,MSD_all.mean);
title('MSD (loglog)','FontSize',15)
xlabel('DeltaT(sec)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);
legend = sprintf('alpha = %.2f',alpha);
text(timeBar(2),MSD_all.mean(max_window)*2,legend,'Color','k','BackGround','w');
clear legend;

subplot(2,2,1);
loglog(timeBar,MSD_traj.mean'); hold on; loglog(timeBar,MSD_all.mean,'LineWidth',2)
title('MSD (all traj loglog)','FontSize',15)
xlabel('DeltaT(sec)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);

subplot(2,2,4);
plot(timeBar,MSD_all.mean);
% hold on; plot(timeBar,MSD_all.median);
title('MSD (linear)','FontSize',15)
xlabel('DeltaT(sec)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);
legend = sprintf('D = %.2eum^2/sec',D);
text(timeBar(10),MSD_all.mean(max_window)*96/100,legend,'Color','k','BackGround','w');

% linear axis, MSD for all DeltaT + separate MSDs
subplot(2,2,3);
plot(timeBar,MSD_traj.mean'); hold on; plot(timeBar,MSD_all.mean,'LineWidth',2)
title('MSD (all traj linear)','FontSize',15)
xlabel('DeltaT(sec)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);

saveas(gcf,strcat('MSDs_loglog_lineaire.png'));
saveas(gcf,strcat('MSDs_loglog_lineaire.fig'));


figure;
% linear axis, MSD for all DeltaT + separate MSDs
% figure;
plot(timeBar,MSD_traj.mean'); hold on; plot(timeBar,MSD_all.mean,'LineWidth',2)
title('MSD (all traj linear)','FontSize',15)
xlabel('DeltaT(sec)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);

for i =1:size(MSD_traj.mean,1)
	text(timeBar(end),MSD_traj.mean(i,end),traj_number(i),'FontSize',12)
end
title('All trajectories');

saveas(gcf,strcat('All_trajectories.png'));
saveas(gcf,strcat('All_trajectories.fig'));

end



