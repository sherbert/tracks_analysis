

disp('Are you sure these are the right simulated values ?')
temp = importdata('/media/data2/Datas_microscopie/dynamique/simulation/Yeast6/log.txt');
simVal = temp.data;

% for i = 1:length(track_all_locus)
% 	trackPix{i} = track_all_locus{i}./108*1000;
% end
% 
% for trackNbr = 1:length(track_all_locus)
%  	for timePoint = 1:length(track_all_locus{trackNbr})
% 		floatValueX = trackPix{trackNbr}(timePoint,1);
% 		floatValueY = trackPix{trackNbr}(timePoint,2);
% 		floatPosX(trackNbr,timePoint) = floatValueX-floor(floatValueX);
% 		floatPosY(trackNbr,timePoint) = floatValueY-floor(floatValueY);
% 	end
% end


trackPix = track_all_locus{1}/pixelSize;
errorSimX = abs(trackPix(:,1)-simVal(:,1));
deltaX = trackPix(:,1)-simVal(:,1);
floatPosX = trackPix(:,1)-floor(trackPix(:,1));
errorSimY = abs(trackPix(:,2)-simVal(:,2));
deltaY = trackPix(:,2)-simVal(:,2);
floatPosY = trackPix(:,2)-floor(trackPix(:,2));

save('variables', 'floatPosX', 'floatPosY', 'errorSimX', 'errorSimY', 'deltaX', 'deltaY', 'trackPix', 'simVal')

figure; plot(floatPosX,errorSimX,'*'); title('ErrorX vs sub pix pos (measured)','FontSize',15);
saveas(gcf,'ErrorX_vs_sub_pix_pos(measured).png');
saveas(gcf,'ErrorX_vs_sub_pix_pos(measured).fig');

figure; plot(simVal(:,1)-floor(simVal(:,1)),errorSimX,'*'); title('ErrorX vs sub pix pos (absolute)','FontSize',15);
saveas(gcf,'ErrorX_vs_sub_pix_pos(abs).png');
saveas(gcf,'ErrorX_vs_sub_pix_pos(abs).fig');

figure; plot(floatPosY,errorSimY,'*'); title('ErrorY vs sub pix pos (measured)','FontSize',15);
saveas(gcf,'ErrorY_vs_sub_pix_pos(measured).png');
saveas(gcf,'ErrorY_vs_sub_pix_pos(measured).fig');

figure; plot(simVal(:,2)-floor(simVal(:,2)),errorSimY,'*'); title('ErrorY vs sub pix pos (absolute)','FontSize',15);
saveas(gcf,'ErrorY_vs_sub_pix_pos(abs).png');
saveas(gcf,'ErrorY_vs_sub_pix_pos(abs).fig');

figure; plot(simVal(:,1),'Color','b'); title('traj X : sim(blue) exp(red) error(green)','FontSize',15);
ylabel('position(pixel)','FontSize',10);
hold on; plot(trackPix(:,1),'Color','r'); plot(errorSimX,'Color','g');
saveas(gcf,'traj_X.png');
saveas(gcf,'traj_X.fig');

figure; plot(simVal(:,2),'Color','b'); title('traj Y : sim(blue) exp(red) error(green)','FontSize',15);
ylabel('position(pixel)','FontSize',10);
hold on; plot(trackPix(:,2),'Color','r'); plot(errorSimY,'Color','g');
saveas(gcf,'traj_Y.png');
saveas(gcf,'traj_Y.fig');



simX = simVal(:,1)-floor(simVal(:,1));
simY = simVal(:,1)-floor(simVal(:,1));
figure; plot(simX,floatPosX,'*'); title('measured vs abs Xposition','FontSize',15);
xlabel('absolute position (pixel)','FontSize',10); ylabel('measured position (pixel)','FontSize',10);
saveas(gcf,'measured_vs_abs_Xposition.png');
saveas(gcf,'measured_vs_abs_Xposition.fig');


%% boxplot deltaX and deltaY vs sub pix position
clear subPixErrX subPixErrY

subPixErrX(:,1) = simVal(:,1)-floor(simVal(:,1));
subPixErrX(:,2) = deltaX;
subPixErrX = sortrows(subPixErrX,1);
clear i count bin histX
binning = [0:0.05:1];
bin = 1;
count = 0;
for i = 1:size(subPixErrX,1)
	if subPixErrX(i,1)<binning(bin+1)
		count = count+1;
		histX(count,bin) = subPixErrX(i,2);
	else 
		bin = bin+1;
		count = 0;
	end
end

histX(histX==0)=NaN;
xlegend = binning(:)+.025;
xlegend = xlegend(1:20);

figure;boxplot(histX,xlegend); title('deltaX vs sub pix pos (abs)','FontSize',15);
xlabel('X subPix position (pixel)','FontSize',10); ylabel('deltaX','FontSize',10);
saveas(gcf,strcat('deltaX_vs_sub_pix_pos_(abs).png'));
saveas(gcf,strcat('deltaX_vs_sub_pix_pos_(abs).fig'));


subPixErrY(:,1) = simVal(:,2)-floor(simVal(:,2));
subPixErrY(:,2) = deltaY;
subPixErrY = sortrows(subPixErrY,1);
clear i count bin histX
bin = 1;
count = 0;
for i = 1:size(subPixErrX,1)
	if subPixErrX(i,1)<binning(bin+1)
		count = count+1;
		histY(count,bin) = subPixErrY(i,2);
	else 
		bin = bin+1;
		count = 0;
	end
end

histY(histY==0)=NaN;

figure;boxplot(histY,xlegend); title('deltaY vs sub pix pos (abs)','FontSize',15);
xlabel('Y subPix position (pixel)','FontSize',10); ylabel('deltaY','FontSize',10);
saveas(gcf,strcat('deltaY_vs_sub_pix_pos_(abs).png'));
saveas(gcf,strcat('deltaY_vs_sub_pix_pos_(abs).fig'));