
function msd_analysis_locus_v3p08()
% The program recquires trajectories of equal length

close all;
clear;

%%params
useGlobalConfidence = 0; % reject tracks with low confidence
useLocalConfidence = 1; % reject frames with low confidence
minGlobalConf = 1.4; % define treshold for global rejection
minLocalConf = 1.9; % define treshold for local rejection
rejectWindow = 5; % define rejection window around a local rejection not yet
minLength = 2; % minLength defines the minimum ratio of OK/NaN for a trajectory
dwnsmple = 1; % 1/dwnsmple of the traj is used MSD calculation
fit_dwnsmple = 3/2; % 1/fit_dwnsmple of the traj is used for fitting
verbose = 0; % How talkative should the program be ?
useNucDisp = 0; % use the center of the nucleoplasm to correct for a nucleus drift
errorMeasure = 1; % calculate the localisation error
simulated_data = 0; % calculate supplementary informations of measured vs absolute values
save_fit_ind = 1; % save each MSD and its associated fit in a separate file

if fit_dwnsmple<dwnsmple
	disp('minimum averaging of the trajectory has to be smaller than the fit downsampling');
	return
end

%% load the files containing the trajectories :

% test file
% - track_all : contains locus x,y (z in case of 3D) position (in micron) vs time(frame)
% - timestep (msec)
% load('rand_tracks.mat'); 

% jython produced file
adress = pwd;
folderName = uigetdir(adress,'Select folder where all the _data.txt files are');
%dimension = 2;
cd(folderName)
filesInFolder = dir('*_data.txt');

traj_number = {};
for i = 1:length(filesInFolder)
	clear datas
	datas = importdata(filesInFolder(i).name);
	traj_number{i} = regexprep(filesInFolder(i).name,'yeast_ROI','traj ');
	traj_number{i} = regexprep(traj_number{i},'_data.txt','');
	data_all{i} = datas.data;
end
header = datas.textdata;
clear datas

% load settings
settings = importdata('condition_settings.txt');
timeStep = settings.data(1)/1000; % in msec
pixelSize = settings.data(2)/1000; % in um
do3D = settings.data(3); % 0 for no, 1 for yes


% mise a l'echelle et sauvegarde des trajectoires
if do3D == 1
 	for i=1:length(filesInFolder)
		track_all_locus{i} = data_all{i}(:,1:3).*pixelSize;
		track_all_nucleus{i} = data_all{i}(:,4:5).*pixelSize;
		
 	end
elseif do3D == 0
	for i=1:length(filesInFolder)
		track_all_locus{i} = data_all{i}(:,1:2)*pixelSize;
		track_all_nucleus{i} = data_all{i}(:,4:5)*pixelSize;
	end
else
	disp('In which dimension are you living ???')
	return
end

% % mise a l'echelle
% for i=1:length(filesInFolder)
% 	track_all_locus{i} = data_all{i}(:,1:5).*pixelSize;
% end

% extraction des valeurs de confiance
for i=1:length(filesInFolder)
	track_all_conf{i} = data_all{i}(:,6);
end

% extraction des valeurs d'intensite
for i=1:length(filesInFolder)
	track_all_maxInt{i} = data_all{i}(:,7);
end


cd ..
workingFolder = regexprep(folderName,'.*/','');
dumpFolder = strcat(workingFolder,'_analysis');
Folder = dir();
for i = 1:length(Folder)
	if strcmp(Folder(i).name,dumpFolder)
		disp('deleting old results');
		rmdir(dumpFolder,'s');
		break
	end
end
 mkdir(dumpFolder)
 cd(dumpFolder)

%% open output_MSD_analysis.txt file
Output = fopen('output_MSD_analysis.txt', 'w');
fprintf(Output, 'Program version = %s\n', mfilename);
fprintf(Output, 'Use the global confidence parameter : %d\n', useGlobalConfidence);
fprintf(Output, 'Minimum value of the global confidence parameter : %d\n', minGlobalConf);
fprintf(Output, 'Use the global confidence parameter : %d\n', useLocalConfidence);
fprintf(Output, 'Minimum value of the local confidence parameter : %d\n', minLocalConf);
fprintf(Output, 'Minimum ratio DeltaT(OK)/DeltaT(NaN) : %d\n', minLength);
fprintf(Output, 'Use the nucleoplasm center to correct the drift = %d\n', useNucDisp);
fprintf(Output, 'Maximum time window is 1/%d of the complete trajectory\n', dwnsmple);
fprintf(Output, 'Proportion of the total trajectory used for the calculation of aplha and D: 1/%d\n', fit_dwnsmple);
fprintf(Output, 'Interframe time (in sec): %d\n', timeStep);
fprintf(Output, 'Pixel size (in um) : %d\n', pixelSize);
fprintf(Output, '3D images : %d\n', do3D);
fprintf(Output, 'Number of trajectories (total) = %d\n', length(track_all_locus));
fprintf(Output, 'Measure localisation error : %d\n', errorMeasure);



%% Correct for nucleus displacement
if useNucDisp == 1
	track_all_locus = drift_correction(track_all_locus,track_all_nucleus);
end

%% delete tracks with confidence coefficient too low (<=> MAX(locus)/MEAN(nucleoplasm)<minGlobalConf)
if useGlobalConfidence ==1
	deletedTraj = 0;
	%deletedTrajs = [''];
	deletedTrajectories = fopen('deleted_trajectories.txt', 'w');
	fprintf(deletedTrajectories, 'Deleted trajectories\n');
	for trackNbr = length(track_all_locus):-1:1
		if min(track_all_conf{trackNbr})<minGlobalConf
			if verbose>0; 
				fprintf('%s\nmin(track_all_conf{trackNbr}=%d)\n\n',traj_number{trackNbr}, ...
				min(track_all_conf{trackNbr}));
			end
			fprintf(deletedTrajectories, '%s\n',traj_number{trackNbr});
			deletedTraj = deletedTraj+1;
			track_all_locus(trackNbr)=[];
			traj_number(trackNbr)=[];
			track_all_nucleus(trackNbr)=[];
			track_all_conf(trackNbr)=[];
			%deletedTrajs = horzcat(deletedTrajs,int2str(trackNbr),' ');
		end
	end
	fprintf('%d trajectories got deleted (confidence test)\n', deletedTraj);
	fprintf(Output, 'Number of trajectories (kept) = %d\n', length(track_all_locus));
	fprintf(Output, 'Number of trajectories (deleted) = %d\n', deletedTraj);
	% 	fprintf('deleted trajectories : %s \n',deletedTrajs);
	fclose(deletedTrajectories);
end


%% Fill in low confidence frame in tracks with NaN positions
if useLocalConfidence ==1
	[track_all_conf,track_all_locus,track_all_maxInt,track_all_nucleus,traj_number] = testLocalConf(minLocalConf, ...
		rejectWindow,track_all_conf,track_all_locus,track_all_maxInt,track_all_nucleus,traj_number,minLength);
end

%% Measure the localisation error
if errorMeasure ==1
	loc_error = errorMeasurement(track_all_locus,track_all_conf,timeStep,track_all_maxInt,pixelSize,simulated_data);
	fprintf(Output, '2D localisation error = %d\n',loc_error);
end

%% Calcul of MSD
%MSD_calculation();

% Find length of longest track and the maximum step number
% that will be calculated
max_length = 1;
for trackNbr = 1:length(track_all_locus)
	if (length(track_all_locus{trackNbr})>max_length)
		max_length=length(track_all_locus{trackNbr});
	end	
end
max_window = floor(max_length/dwnsmple)-1;
deltaTs = (timeStep:timeStep:timeStep*(max_window));

clear trackNbr;


SD_track = cell(length(track_all_locus),1); % squared displacement NOT averaged
SD_DeltaT = cell(max_window,1);

%for each track all SDs are measured
tic
disp('SD calculation = ')
for trackNbr = 1:length(track_all_locus)
	current_track = track_all_locus{trackNbr};
	SD_track{trackNbr} = sd_calculation(current_track,dwnsmple);
	for deltaT = 1:length(SD_track{trackNbr})
		SD_DeltaT{deltaT} = [SD_DeltaT{deltaT};SD_track{trackNbr}{deltaT}];		
	end
end
clear trackNbr;
toc

% overall MSD calculation
tic
disp('overall MSD calculation = ')
for j = 1:max_window
	MSD_all.mean(j) = nanmean(SD_DeltaT{j});
	MSD_all.std(j) = nanstd(SD_DeltaT{j});
 	MSD_all.median(j) = nanmedian(SD_DeltaT{j});
 	MSD_all.iqr(j) = iqr(SD_DeltaT{j});	
end
toc

% single trajectories MSDs calculation
tic
disp('single trajectories MSDs calculation = ')
for i = 1:length(track_all_locus)
	for j = 1:length(SD_track{i})
		MSD_traj.mean(i,j) = nanmean(SD_track{i}{j});
		MSD_traj.std(i,j) = nanstd(SD_track{i}{j});
	end
end
toc

% % mean MSD calculation => is the same as long as the samples are the same size !!!
% tic
% disp('mean MSD calculation = ')
% mean_MSD = nanmean(MSD_traj.mean,);
% 	
	
%% calculation of the mean power law and diffusion coefficient

[coefs_fit diffs alphas fit_curve] = fitToModel(MSD_traj.mean,deltaTs,dwnsmple,fit_dwnsmple,do3D);

if save_fit_ind
	save_fit_indiv(MSD_traj.mean, traj_number, deltaTs, alphas, diffs, fit_curve, coefs_fit, do3D);
end

save('fit_values_short', 'diffs', 'alphas','traj_number');
fprintf(Output, 'Mean alpha coefficient : %d\n', mean(alphas));
fprintf(Output, 'Median alpha coefficient : %d\n', median(alphas));
fprintf(Output, 'Mean diffusion coefficient (um^2/sec): %d\n', mean(diffs));
fprintf(Output, 'Median diffusion coefficient (um^2/sec): %d\n', median(diffs));

%% Dump calculated values
save('MSDs','MSD_all','MSD_traj','SD_track','traj_number')

%% plot MSD
plotMSDs(deltaTs,MSD_all,mean(alphas),max_window,MSD_traj,mean(diffs),traj_number,fit_curve);

%% autocorrelation of angles


%% bayesian comparison of MSD versus model (free diffusion, constraint and subdiffusion)


%% sliding window for switching behaviour


%% closing the function
cd(folderName)
fclose(Output);

disp('Done !')

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%      End of the main program
%%%%%%%%%%%%%%%%      Beginning of the related functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function [track_all_conf,track_all_locus,track_all_maxInt,track_all_nucleus, traj_number] = testLocalConf ...
(minLocalConf,rejectWindow,track_all_conf,track_all_locus,track_all_maxInt,track_all_nucleus,traj_number, minLength)

delTrajsLoc = fopen('deleted_trajectories_local.txt', 'w');
fprintf(delTrajsLoc, 'Deleted trajectories due to local parameter (too many NaNs)\n');
fprintf(delTrajsLoc, 'minLocalConf = %.2d\n',minLocalConf);
fprintf(delTrajsLoc, 'minimum ratio length/NaNs = %d\n',minLength);

deletedTraj = 0;
for track = length(traj_number):-1:1
	if(length(track_all_conf{track})/sum(track_all_conf{track}<minLocalConf)<minLength)
		fprintf(delTrajsLoc, 'delete %s; Number of NaNs timepoints %i \n',traj_number{track},sum(track_all_conf{track}<minLocalConf));
		track_all_locus(track)=[];
		track_all_nucleus(track)=[];
		track_all_maxInt(track)=[];
		track_all_conf(track)=[];
		traj_number(track)=[];
		deletedTraj = deletedTraj +1;
		continue
	end
	for tp = 1:length(track_all_conf{track}) % timepoint
		if track_all_conf{track}(tp)<minLocalConf
			track_all_locus{track}(tp,:)=nan;
			track_all_nucleus{track}(tp,:)=nan;
			track_all_maxInt{track}(tp,:)=nan;
			track_all_conf{track}(tp,:)=nan;
		end
	end
end

fprintf(delTrajsLoc,'%d trajectories got deleted (confidence test)\n', deletedTraj);
fprintf(delTrajsLoc, 'Number of trajectories (kept) = %d\n', length(traj_number));
fprintf(delTrajsLoc, 'Number of trajectories (deleted) = %d\n', deletedTraj);
fclose(delTrajsLoc);

end

function save_fit_indiv(MSD_traj, traj_number, deltaTs, alphas, diffs, fit_curve, coefs_fit, do3D)
% plots and save plots of the individual trajectories

mkdir individual_plots
cd individual_plots

for i = 1:length(traj_number)
	fit_traj = 2*(2+do3D)*diffs(i)*fit_curve(:,1).^alphas(i);
	figure('Name','traj_fit','NumberTitle','off');
	figTitle = sprintf('Experimental and fitted MSDs of %s',traj_number{i});
	figName = sprintf('MSD_fit_exp_%s', regexprep(traj_number{i},' ','_'));
	
	subplot(2,1,1); % loglog graph (indiv + fit)
	loglog(deltaTs,MSD_traj(i,:)); 
	hold on; loglog(fit_curve(:,1),fit_traj,'Color','r');
	title(strcat(figTitle,' (loglog)'),'FontSize',15);
	xlabel('DeltaT(sec)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);
	
	legend(1) = {sprintf('alpha = %.2f',alphas(i))};
	legend(2) = {sprintf('D = %.2eum/sec',diffs(i))};
	legend(3) = {sprintf('sse = %.2e',coefs_fit{1,i}.gof.sse)};
	text(deltaTs(1),max(MSD_traj(i,:)),legend,'Color','k','BackGround','w');
	clear legend
	
	
	subplot(2,1,2); % linear graph (indiv + fit)
	plot(deltaTs,MSD_traj(i,:)); 
	hold on; plot(fit_curve(:,1),fit_traj,'Color','r');
	title(strcat(figTitle,' (linear)'),'FontSize',15);
	xlabel('DeltaT(sec)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);

	% erreur moindres carres
	
	
	saveas(gcf,strcat(figName,'.png'));
	saveas(gcf,strcat(figName,'.fig'));
	close traj_fit
	clear fit_traj;
end

cd ..
end

function [coefs_fit diffs alphas fit_curve] = fitToModel(MSDs,deltaTs,dwnsmple,fit_dwnsmple,do3D)
% coefs_fit -> full information of the fits (errors, params, values, ...)
% diffs -> list of the diffusion values
% alphas -> list of the alpha values
% fit_curve -> matrix : column 1 = DeltaT; column 2 = mean value of the fit for corresponding
% DeltaT; column 3 = mean value of the fit for corresponding DeltaT
% The fitting equation is function of the number of dimensions

if do3D==1
	equation = '6*D*x^alpha';
else
	equation = '4*D*x^alpha';
end

coefs_fit = {};
maxStep = floor(size(MSDs,2)*dwnsmple/fit_dwnsmple);
for i=1:size(MSDs,1)
	idxValid = ~isnan(MSDs(i,1:maxStep));
	[coefs_fit{i}.cfun coefs_fit{i}.gof coefs_fit{i}.output coefs_fit{i}.xdata coefs_fit{i}.ydata] ...
		=  fit(deltaTs(idxValid)',MSDs(i,idxValid)',equation);
	temp = confint(coefs_fit{i}.cfun);
	diffs(i) = sum(temp(:,1))/2;
	alphas(i) = sum(temp(:,2))/2;
end

save('fit_values_full_infos', 'coefs_fit');

figure;
subplot(2,1,1);
hist(alphas,15); title('alpha dispersion','FontSize',15);
xlabel('alpha','FontSize',10); ylabel('population','FontSize',10);
subplot(2,1,2);
boxplot(alphas);
ylabel('alpha','FontSize',10);
legend(1) = {sprintf('mean(alpha) = %.2f',nanmean(alphas))};
legend(2) = {sprintf('median(alpha) = %.2f',nanmedian(alphas))};
text(0.6,0.6,legend,'Color','k','BackGround','w');
saveas(gcf,strcat('alpha_dispersion.png'));
saveas(gcf,strcat('alpha_dispersion.fig'));

clear legend

figure;
subplot(2,1,1);
hist(diffs,15); title('diff dispersion','FontSize',15);
xlabel('diff (um^2/sec)','FontSize',10); ylabel('population','FontSize',10);
subplot(2,1,2);
boxplot(diffs);
ylabel('diff (um^2/sec)','FontSize',10);
legend(1) = {sprintf('mean(D) = %.2eum/sec^{alpha}',nanmean(diffs))};
legend(2) = {sprintf('median(D) = %.2eum/sec^{alpha}',nanmedian(diffs))};
text(0.5,nanmean(diffs),legend,'Color','k','BackGround','w');
saveas(gcf,strcat('diff_boxplot.png'));
saveas(gcf,strcat('diff_boxplot.fig'));

clear legend



fprintf('Mean alpha coefficient : %d\n', nanmean(alphas));
fprintf('Mean diffusion coefficient (um^2/sec): %d\n', nanmean(diffs));

fit_curve(:,1) = deltaTs(1:maxStep);
fit_curve(:,2) = 2*(2+do3D)*nanmean(diffs)*fit_curve(:,1).^nanmean(alphas);
fit_curve(:,3) = 2*(2+do3D)*nanmedian(diffs)*fit_curve(:,1).^nanmedian(alphas);

end

function mean_FWHM = errorMeasurement(track_all_locus,track_all_conf,timeStep,track_all_maxInt,pixelSize,simulated_data)


mkdir errorplots
cd errorplots


clear sig*;
clear timeStamp;
sigX = [];
sigY = [];
for trackNbr = 1:length(track_all_locus)
 	for timePoint = 1:length(track_all_locus{trackNbr})-1
		sigConf(trackNbr,timePoint) = track_all_conf{trackNbr}(timePoint);
		sigMaxInt(trackNbr,timePoint) = track_all_maxInt{trackNbr}(timePoint);
		timeStamp(trackNbr,timePoint) = timePoint*timeStep;
		
		sigX(trackNbr,timePoint) = track_all_locus{trackNbr}(timePoint,1)-track_all_locus{trackNbr}(timePoint+1,1);
		sigY(trackNbr,timePoint) = track_all_locus{trackNbr}(timePoint,2)-track_all_locus{trackNbr}(timePoint+1,2);
		eucDists(trackNbr,timePoint) = sqrt(sigX(trackNbr,timePoint)^2+sigY(trackNbr,timePoint)^2);
		%sigX(length(sigX)+1) = track_all_locus{trackNbr}(timePoint,1)-track_all_locus{trackNbr}(timePoint+1,1);
		%sigY(length(sigY)+1) = track_all_locus{trackNbr}(timePoint,2)-track_all_locus{trackNbr}(timePoint+1,2);
 	end
end
distX = nanmean(abs(sigX(:)));
distY = nanmean(abs(sigY(:)));
eucDist = nanmean(sqrt(sigX(:).^2+sigY(:).^2));
%fprintf('mean dist along X = %dum\n',distX);
%fprintf('mean dist along Y = %dum\n',distY);
%fprintf('mean 2D distance = %dum\n',eucDist);

mean_deltaX = nanmean(sigX(:));
mean_deltaY = nanmean(sigY(:));
std_deltaX = nanstd(sigX(:));
std_deltaY = nanstd(sigY(:));
%mean_eucDelta = nanmean(sqrt(sigX(:).^2+sigY(:).^2));
%std_eucDelta = nanstd(sqrt(sigX(:).^2+sigY(:).^2));
fprintf('mean delta along X (bias) = %dum\n',mean_deltaX);
fprintf('mean delta along Y (bias) = %dum\n',mean_deltaY);
%fprintf('std delta along X = %dum\n',std_deltaX);
%fprintf('std delta along Y = %dum\n',std_deltaY);
fprintf('FWHM delta along X = %dum\n',(std_deltaX*2.354));
fprintf('FWHM delta along Y = %dum\n',(std_deltaY*2.354));
mean_FWHM = (std_deltaY+std_deltaX)*2.354/2;
fprintf('mean FWHM 2D = %dum\n',mean_FWHM);

%% plot hist of errors
figure; hist(sigX(:),100); title('Sigma X','FontSize',15);
xlabel('Distance(um)','FontSize',10); ylabel('Population','FontSize',10);
saveas(gcf,strcat('Sigma_X.png'));
saveas(gcf,strcat('Sigma_X.fig'));

figure; hist(sigY(:),100); title('Sigma Y','FontSize',15);
xlabel('Distance(um)','FontSize',10); ylabel('Population','FontSize',10);
saveas(gcf,strcat('Sigma_Y.png'));
saveas(gcf,strcat('Sigma_Y.fig'));

figure; hist(eucDists(:),100); title('Interframe distance','FontSize',15);
xlabel('Distance(um)','FontSize',10); ylabel('Population','FontSize',10);
saveas(gcf,strcat('Interframe_distance.png'));
saveas(gcf,strcat('Interframe_distance.fig'));

figure; plot(timeStamp,eucDists,'*'); title('Dist vs time','FontSize',15);
xlabel('Time(sec)','FontSize',10); ylabel('Distance(um)','FontSize',10);
saveas(gcf,strcat('Dist_vs_time.png'));
saveas(gcf,strcat('Dist_vs_time.fig'));

figure; plot(sigConf,eucDists,'*'); title('Dist vs Intensity ratio (maxLocus/meanNuc)','FontSize',15);
xlabel('Intensity ratio','FontSize',10); ylabel('Distance(um)','FontSize',10);
saveas(gcf,strcat('Dist_vs_Iratio.png'));
saveas(gcf,strcat('Dist_vs_Iratio.fig'));

figure; plot(sigMaxInt,eucDists,'*'); title('Dist vs Intensity Max','FontSize',15);
xlabel('Locus Max intensity','FontSize',10); ylabel('Distance(um)','FontSize',10);
saveas(gcf,strcat('Dist_vs_Imax.png'));
saveas(gcf,strcat('Dist_vs_Imax.fig'));

% boxplots (average for 50bins) of distances, Iratio and Imax as a function of time
% downRatio
downRatio = 25;
miniSize = size(eucDists,2)-mod(size(eucDists,2),downRatio);
miniEucDists = reshape(eucDists(:,1:miniSize),[],floor(size(eucDists,2)/downRatio));
miniTimeStamp = timeStamp(1,floor(downRatio/2):downRatio:end);
miniTimeStamp = miniTimeStamp(1:size(miniEucDists,2));
%figure;boxplot(miniEucDists,miniTimeStamp,'symbol',''); title('Dist vs time','FontSize',15);
figure;boxplot(miniEucDists,miniTimeStamp); title('Dist vs time','FontSize',15);
set(gca,'YLim',[0  max(iqr(miniEucDists))*5]);
xlabel('Time(sec)','FontSize',10); ylabel('Distance(um)','FontSize',10);
saveas(gcf,strcat('Dist_vs_time_boxplots.png'));
saveas(gcf,strcat('Dist_vs_time_boxplots.fig'));

miniSigConf = reshape(sigConf(:,1:miniSize),[],floor(size(sigConf,2)/downRatio));
% figure; boxplot(miniSigConf,miniTimeStamp,'symbol',''); title('Intensity ratio (maxLocus/meanNuc) vs time','FontSize',15);
figure; boxplot(miniSigConf,miniTimeStamp); title('Intensity ratio (maxLocus/meanNuc) vs time','FontSize',15);
xlabel('Time(sec)','FontSize',10); ylabel('Intensity ratio','FontSize',10);
saveas(gcf,strcat('Iratio_vs_time_boxplot.png'));
saveas(gcf,strcat('Iratio_vs_time_boxplot.fig'));

miniSigMax = reshape(sigMaxInt(:,1:miniSize),[],floor(size(sigConf,2)/downRatio));
% figure; boxplot(miniSigMax,miniTimeStamp,'symbol',''); title('Locus Max Intensity vs time','FontSize',15);
figure; boxplot(miniSigMax,miniTimeStamp); title('Locus Max Intensity vs time','FontSize',15);
xlabel('Time(sec)','FontSize',10); ylabel('Locus Max intensity','FontSize',10);
saveas(gcf,strcat('Imax_vs_time_boxplot.png'));
saveas(gcf,strcat('Imax_vs_time_boxplot.fig'));


% create sorted matrices distConf and distMaxInt
binNbr = 20; % number of bin for the boxplot/histogram
distConf(:,1) = reshape(eucDists,[],1);
distConf(:,2) = reshape(sigConf,[],1);
distConf = sortrows(distConf,2);

distMaxInt(:,1) = reshape(eucDists,[],1);
distMaxInt(:,2) = reshape(sigMaxInt,[],1);
distMaxInt = sortrows(distMaxInt,2);


[distConf_box xLabeldc] = boxplotting(distConf, binNbr);
figure; boxplot(distConf_box,xLabeldc); title('Dist vs Intensity ratio (maxLocus/meanNuc)','FontSize',15);
xlabel('Intensity ratio','FontSize',10); ylabel('Dist(um)','FontSize',10);
saveas(gcf,strcat('Dist_vs_Iratio_boxplot.png'));
saveas(gcf,strcat('Dist_vs_Iratio_boxplot.fig'));

[distMaxInt_box xLabeldmi] = boxplotting(distMaxInt, binNbr);
figure; boxplot(distMaxInt_box,xLabeldmi); title('Dist vs Intensity Max','FontSize',15);
xlabel('Intensity max','FontSize',10); ylabel('Dist(um)','FontSize',10);
saveas(gcf,strcat('Dist_vs_Imax_boxplot.png'));
saveas(gcf,strcat('Dist_vs_Imax_boxplot.fig'));

% create a boxplot of dist vs conf and maxInt




%% ecriture des resultats
if simulated_data==1
	errorplot;
end

loc_error = fopen('localisation_error.txt', 'w');
fprintf(loc_error,'mean dist along X = %dum\n',distX);
fprintf(loc_error,'mean dist along Y = %dum\n',distY);
fprintf(loc_error,'mean 2D distance = %dum\n',eucDist);
fprintf(loc_error,'mean delta along X (bias) = %dum\n',mean_deltaX);
fprintf(loc_error,'mean delta along Y (bias) = %dum\n',mean_deltaY);
fprintf(loc_error,'std delta along X = %dum\n',std_deltaX);
fprintf(loc_error,'std delta along Y = %dum\n',std_deltaY);
fprintf(loc_error,'FWHM delta along X = %dum\n',(std_deltaX*2.354));
fprintf(loc_error,'FWHM delta along Y = %dum\n',(std_deltaY*2.354));
fprintf(loc_error,'mean FWHM 2D = %dum\n',mean_FWHM);

fclose(loc_error);

cd ..
end

function [matrix xlabel] = boxplotting(sortMat, binNbr)
% creates a tab at the right format for a boxplot graph with a single sorted sortMat
% sortMat(:,1) -> y
% sortMat(:,2) -> x (boxplot based on this column)

% get rid of NaN values
condition = isnan(sortMat(:,1));
sortMat(condition,:) = [];
condition = isnan(sortMat(:,2));
sortMat(condition,:) = [];

delta = (sortMat(end,2)-sortMat(1,2))/binNbr;
binning = [sortMat(1,2):delta:sortMat(end,2)];
xlabel = binning(1:end-1)+delta/2;
bin = 1;
count = 0;
for i = 1:size(sortMat,1)
	if sortMat(i,2)<=binning(bin+1)
		count = count+1;
		matrix(count,bin) = sortMat(i,1);
	else 
		bin = bin+1;
		count = 1;
		matrix(count,bin) = sortMat(i,1);
	end
end

matrix(matrix==0)=NaN;

end

function track_all_locus_corrected = drift_correction(track_all_locus,track_all_nucleus)
% Corrects the locus position for nucleoplasm xy drift
% center nucleoplasm positions around 0.0
for trackNbr = 1:length(track_all_locus)
	track_all_nucleus_centered{trackNbr}(:,1) = track_all_nucleus{trackNbr}(:,1)-track_all_nucleus{trackNbr}(1,1);
	track_all_nucleus_centered{trackNbr}(:,2) = track_all_nucleus{trackNbr}(:,2)-track_all_nucleus{trackNbr}(1,2);
end
% apply correction to the locus position
for trackNbr = 1:length(track_all_locus)
	track_all_locus_corrected{trackNbr}(:,1) = track_all_locus{trackNbr}(:,1)-track_all_nucleus_centered{trackNbr}(:,1);
	track_all_locus_corrected{trackNbr}(:,2) = track_all_locus{trackNbr}(:,2)-track_all_nucleus_centered{trackNbr}(:,2);
end
end

function plotMSDs(deltaTs,MSD_all,alpha,max_window,MSD_traj,D,traj_number,fit_curve)
% Plots 2 figures (log log/linear plots and precise linear plot + traj number)
% log log
figure; 
% loglog axis, MSD for all DeltaT + fit
subplot(2,2,2)
loglog(deltaTs,MSD_all.mean);
hold on; loglog(fit_curve(:,1),fit_curve(:,2),'Color','r');
loglog(fit_curve(:,1),fit_curve(:,3),'Color','k');
title('MSD (loglog)','FontSize',15)
xlabel('DeltaT(sec)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);
legend(1) = {sprintf('alpha = %.2f',alpha)};
legend(2) = {'Fitted curve (mean) - Red'};
legend(3) = {'Fitted curve (median) - Black'};
legend(4) = {'Experimental curve - Blue'};
text(deltaTs(2),MSD_all.mean(max_window),legend,'Color','k','BackGround','w','FontSize',7);
clear legend;

% loglog axis, MSD for all DeltaT + separate MSDs
subplot(2,2,1);
loglog(deltaTs,MSD_traj.mean'); hold on; loglog(deltaTs,MSD_all.mean,'LineWidth',2)
title('MSD (all traj loglog)','FontSize',15)
xlabel('DeltaT(sec)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);

% linear axis, MSD for all DeltaT + fit
subplot(2,2,4);
plot(deltaTs,MSD_all.mean);
hold on; plot(fit_curve(:,1),fit_curve(:,2),'Color','r');
plot(fit_curve(:,1),fit_curve(:,3),'Color','k');
title('MSD (linear)','FontSize',15)
xlabel('DeltaT(sec)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);
legend(1) = {sprintf('D = %.2eum/sec^{alpha}',D)};
legend(2) = {'Fitted curve (mean) - Red'};
legend(3) = {'Fitted curve (median) - Black'};
legend(4) = {'Experimental curve - Blue'};
text(deltaTs(10),MSD_all.mean(max_window)*96/100,legend,'Color','k','BackGround','w','FontSize',7);

% linear axis, MSD for all DeltaT + separate MSDs
subplot(2,2,3);
plot(deltaTs,MSD_traj.mean'); hold on; plot(deltaTs,MSD_all.mean,'LineWidth',2)
title('MSD (all traj linear)','FontSize',15)
xlabel('DeltaT(sec)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);

saveas(gcf,strcat('MSDs_loglog_lineaire.png'));
saveas(gcf,strcat('MSDs_loglog_lineaire.fig'));

figure;
% linear axis, MSD for all DeltaT + separate MSDs
% figure;
plot(deltaTs,MSD_traj.mean'); 
hold on; plot(deltaTs,MSD_all.mean,'LineWidth',2);
title('MSD (all traj linear)','FontSize',15)
xlabel('DeltaT(sec)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);

for i =1:size(MSD_traj.mean,1)
	text(deltaTs(end),MSD_traj.mean(i,end),traj_number(i),'FontSize',12)
end
title('All trajectories');

saveas(gcf,strcat('All_trajectories.png'));
saveas(gcf,strcat('All_trajectories.fig'));

end



