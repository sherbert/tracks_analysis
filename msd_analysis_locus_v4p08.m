
function msd_analysis_locus_vBitbucket(full_data_1_locus,exp_params,dumpFolder,origin)
% The program recquires trajectories of equal length

%% this program has been issued on the 15th of April 2013
% equal to the version msd_analysis_locus_v4p08

close all;

%% PARAMS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

folderName = pwd;

if strcmp(origin,'experimental')
	useGlobalConfidence = 0; % reject tracks with low confidence
	useLocalConfidence = 1; % reject frames with low confidence
	minGlobalConf = 1.4; % define treshold for global rejection
	% minLocalConf = 1.6; % define treshold for local rejection (intensity ratio)
	minLocalConf = 5.0; % define treshold for local rejection (SNR max)
	errorMeasure = 1; % calculate the localisation error
	save_fit_ind = 1; % save each MSD and its associated fit in a separate file
	save_traj_ind = 1; % save each trajectory in a separate file
	verbose = 1; % How talkative should the program be ?
elseif strcmp(origin,'model_Hua')
	useGlobalConfidence = 0; % reject tracks with low confidence
	useLocalConfidence = 0; % reject frames with low confidence
	minGlobalConf = 1.4; % define treshold for global rejection
	minLocalConf = 1.9; % define treshold for local rejection
	errorMeasure = 0; % calculate the localisation error
	save_fit_ind = 0; % save each MSD and its associated fit in a separate file
	save_traj_ind = 0; % save each trajectory in a separate file
	verbose = 0; % How talkative should the program be ?
elseif strcmp(origin,'simu_numeric')
	useGlobalConfidence = 0; % reject tracks with low confidence
	useLocalConfidence = 0; % reject frames with low confidence
	minGlobalConf = 1.4; % define treshold for global rejection
	minLocalConf = 1.9; % define treshold for local rejection
	errorMeasure = 0; % calculate the localisation error
	save_fit_ind = 1; % save each MSD and its associated fit in a separate file
	save_traj_ind = 1; % save each trajectory in a separate file
	verbose = 0; % How talkative should the program be ?
elseif strcmp(origin,'Sage')
	useGlobalConfidence = 0; % reject tracks with low confidence
	useLocalConfidence = 0; % reject frames with low confidence
	minGlobalConf = 1.4; % define treshold for global rejection
	minLocalConf = 1.9; % define treshold for local rejection
	errorMeasure = 0; % calculate the localisation error
	save_fit_ind = 1; % save each MSD and its associated fit in a separate file
	save_traj_ind = 1; % save each trajectory in a separate file
	verbose = 0; % How talkative should the program be ?	
else
	disp('What is the origin of datas ?')
	return
end
	

nonOverlappingMSD = 0; % overlapping of DeltaT windows in MSD calculation
rejectWindow = 1; % define rejection window around a local rejection. Not applied yet
minLength = 2; % minimum ratio length/NaNs
dwnsmple = 1; % 1/dwnsmple of the traj is used MSD calculation
fit_dwnsmple = 3; % 1/fit_dwnsmple of the traj is used for fitting
useNucDisp = 0; % use the center of the nucleoplasm to correct for a nucleus drift
simulated_data = 0; % calculate supplementary informations of measured vs absolute values



if fit_dwnsmple<dwnsmple
	disp('minimum averaging of the trajectory has to be smaller than the fit downsampling');
	return
end

%% decondensing input variables
track_all_conf = full_data_1_locus.track_all_conf;
track_all_locus = full_data_1_locus.track_all_locus;
track_all_maxInt = full_data_1_locus.track_all_maxInt;
track_all_nucleus = full_data_1_locus.track_all_nucleus;
traj_number = full_data_1_locus.traj_number;
clear full_data_1_locus;

timeStep = exp_params.timeStep; % in sec
pixelSize = exp_params.pixelSize; % in um
do3D = exp_params.do3D;
clear exp_params;


%% test for an older version of the results
Folder = dir();
for i = 1:length(Folder)
	if strcmp(Folder(i).name,dumpFolder)
		disp('deleting old results');
		rmdir(dumpFolder,'s');
		break
	end
end
mkdir(dumpFolder)
cd(dumpFolder)

%% open output_MSD_analysis.txt file
Output = fopen('output_MSD_analysis.txt', 'w');
fprintf(Output, 'Program version = %s\n', mfilename);
fprintf(Output, 'Use the global confidence parameter : %d\n', useGlobalConfidence);
fprintf(Output, 'Minimum value of the global confidence parameter : %d\n', minGlobalConf);
fprintf(Output, 'Use the local confidence parameter : %d\n', useLocalConfidence);
fprintf(Output, 'Minimum value of the local confidence parameter : %d\n', minLocalConf);
fprintf(Output, 'Minimum ratio length/NaNs : %d\n', minLength);
fprintf(Output, 'Use the nucleoplasm center to correct the drift = %d\n', useNucDisp);
fprintf(Output, 'Maximum time window is 1/%d of the complete trajectory\n', dwnsmple);
fprintf(Output, 'Proportion of the total trajectory used for the calculation of aplha and B: 1/%d\n', fit_dwnsmple);
fprintf(Output, 'Interframe time (in sec): %d\n', timeStep);
fprintf(Output, 'Pixel size (in um) : %d\n', pixelSize);
fprintf(Output, '3D images : %d\n', do3D);
fprintf(Output, 'Number of trajectories (total) = %d\n', length(track_all_locus));
fprintf(Output, 'Measure localisation error : %d\n', errorMeasure);
fprintf(Output, 'No overlapping of DeltaT windows in MSD calculation : %d\n', nonOverlappingMSD);


%% Correct for nucleus displacement
if useNucDisp == 1
	track_all_locus = drift_correction(track_all_locus,track_all_nucleus);
end

%% delete tracks with confidence coefficient too low (<=> MAX(locus)/MEAN(nucleoplasm)<minGlobalConf)
if useGlobalConfidence ==1
	deletedTraj = 0;
	%deletedTrajs = [''];
	deletedTrajectories = fopen('deleted_trajectories.txt', 'w');
	fprintf(deletedTrajectories, 'Deleted trajectories\n');
	for trackNbr = length(track_all_locus):-1:1
		if min(track_all_conf{trackNbr})<minGlobalConf
			if verbose>1; 
				fprintf('%s\nmin(track_all_conf{trackNbr}=%d)\n\n',traj_number{trackNbr}, ...
				min(track_all_conf{trackNbr}));
			end
			fprintf(deletedTrajectories, '%s\n',traj_number{trackNbr});
			deletedTraj = deletedTraj+1;
			track_all_locus(trackNbr)=[];
			traj_number(trackNbr)=[];
			track_all_nucleus(trackNbr)=[];
			track_all_conf(trackNbr)=[];
			%deletedTrajs = horzcat(deletedTrajs,int2str(trackNbr),' ');
		end
	end
	fprintf('%d trajectories got deleted (confidence test)\n', deletedTraj);
	fprintf(Output, 'Number of trajectories (kept) = %d\n', length(track_all_locus));
	fprintf(Output, 'Number of trajectories (deleted) = %d\n', deletedTraj);
	% 	fprintf('deleted trajectories : %s \n',deletedTrajs);
	fclose(deletedTrajectories);
end


%% Fill in low confidence frame in tracks with NaN positions
if useLocalConfidence == 1
	[track_all_conf,track_all_locus,track_all_maxInt,track_all_nucleus,traj_number] = testLocalConf(minLocalConf, ...
		rejectWindow,track_all_conf,track_all_locus,track_all_maxInt,track_all_nucleus,traj_number,minLength);
	if length(track_all_locus)==0
		disp('Warning : 0 track past the local confidence test, stopping the analysis');
		return
	end
end


%% Save individual trajectories
if save_traj_ind == 1
	saveTrajIndiv(track_all_locus, traj_number);
end


%% Measure the localisation error
if errorMeasure == 1
	loc_error = errorMeasurement(track_all_locus,track_all_conf,timeStep,track_all_maxInt,pixelSize,simulated_data);
	fprintf(Output, '2D localisation error = %d\n',loc_error);
end

%% Calcul of MSD
%MSD_calculation();

% Find length of longest track and the maximum step number
% that will be calculated
max_length = 1;
for trackNbr = 1:length(track_all_locus)
	if (length(track_all_locus{trackNbr})>max_length)
		max_length=length(track_all_locus{trackNbr});
	end	
end
max_window = floor(max_length/dwnsmple)-1;
deltaTs = (timeStep:timeStep:timeStep*(max_window));

clear trackNbr;


SD_track = cell(length(track_all_locus),1); % squared displacement NOT averaged
SD_DeltaT = cell(max_window,1);

%for each track all SDs are measured
if verbose>0
	tic;
	disp('SD calculation = '); 
end
for trackNbr = 1:length(track_all_locus)
	current_track = track_all_locus{trackNbr};
	SD_track{trackNbr} = sd_calculation(current_track,dwnsmple,nonOverlappingMSD);
	for deltaT = 1:length(SD_track{trackNbr})
		SD_DeltaT{deltaT} = [SD_DeltaT{deltaT};SD_track{trackNbr}{deltaT}];		
	end
end
clear trackNbr;
if verbose>0; toc; end

% overall MSD calculation
if verbose>0 
	tic;
	disp('overall MSD calculation = '); 
end

for j = 1:max_window
	MSD_all.mean(j) = nanmean(SD_DeltaT{j});
	MSD_all.std(j) = nanstd(SD_DeltaT{j})/sqrt(length(SD_DeltaT{j})); % measure of the error on the mean not the dispersion
 	MSD_all.median(j) = nanmedian(SD_DeltaT{j});
 	MSD_all.iqr(j) = iqr(SD_DeltaT{j});	
	MSD_all.weight(j) = sum(~isnan(SD_DeltaT{j}));
end
% parfor j = 1:max_window
% 	tempa(j) = nanmean(SD_DeltaT{j});
% 	tempb(j) = nanstd(SD_DeltaT{j});
%  	tempc(j) = nanmedian(SD_DeltaT{j});
%  	tempd(j) = iqr(SD_DeltaT{j});	
% end
% MSD_all.mean = tempa;
% MSD_all.std = tempb;
% MSD_all.median = tempc;
% MSD_all.iqr = tempd;
if verbose>0; toc; end

% single trajectories MSDs calculation
if verbose>0 
	tic; 
	disp('single trajectories MSDs calculation = ');
end
for i = 1:length(track_all_locus)
	for j = 1:length(SD_track{i})
		MSD_traj.mean(i,j) = nanmean(SD_track{i}{j});
		MSD_traj.std(i,j) = nanstd(SD_track{i}{j})/sqrt(length(SD_track{i}{j})); % measure of the error on the mean not the dispersion
		MSD_traj.weight(i,j) = sum(~isnan(SD_track{i}{j}));
	end
end
if verbose>0; toc; end

% % mean MSD calculation => is the same as long as the samples are the same size !!!
% tic
% disp('mean MSD calculation = ')
% mean_MSD = nanmean(MSD_traj.mean,);
% 	
	
%% calculation of the mean power law and diffusion coefficient



if do3D==1
	equation = '6*D*x^alpha';
else
	equation = '4*D*x^alpha';
end

[coefs_fit diffs alphas fit_curve] = fitToModel(MSD_traj,deltaTs,dwnsmple,fit_dwnsmple,do3D,origin,equation);

if save_fit_ind
	save_fit_indiv(MSD_traj, traj_number, deltaTs, alphas, diffs, fit_curve, coefs_fit, do3D);
end

save('fit_values_short', 'diffs', 'alphas','traj_number');
fprintf(Output, '\nEquation of the model : %s\n', equation);
fprintf(Output, 'Mean alpha coefficient : %d\n', nanmean(alphas));
fprintf(Output, 'Median alpha coefficient : %d\n', median(alphas));
fprintf(Output, 'Mean diffusion coefficient (um^2/sec): %d\n', nanmean(diffs));
fprintf(Output, 'Median diffusion coefficient (um^2/sec): %d\n', median(diffs));

% fit of the global MSD
maxStep = floor(size(MSD_all.mean,2)*dwnsmple/fit_dwnsmple);
idxValid = ~isnan(MSD_all.mean(1:maxStep));
[coefs_fit_global.cfun coefs_fit_global.gof coefs_fit_global.output ] ...
	= fit(deltaTs(idxValid)',MSD_all.mean(idxValid)',equation,'StartPoint',[1.0e-04,0.5],'Weight',MSD_all.weight(idxValid)');
temp = confint(coefs_fit_global.cfun);
diff_global = sum(temp(:,1))/2;
alpha_global = sum(temp(:,2))/2;
clear temp
save('fit_values_short_global', 'diff_global', 'alpha_global');
fprintf(Output, 'Global alpha coefficient : %d\n', alpha_global);
fprintf(Output, 'Global diffusion coefficient : %d\n', diff_global);
traj_name = {'Global'};
save_fit_indiv(MSD_all, traj_name, deltaTs, alpha_global, diff_global, fit_curve, coefs_fit_global, do3D);




%% Dump calculated values

if strcmp(origin,'experimental');
	save('MSDs','MSD_traj','deltaTs','MSD_all','alphas','max_window','diffs','traj_number','fit_curve','SD_track');
else strcmp(origin,'simu_Hua');
	save('MSDs_simu','MSD_traj','deltaTs','MSD_all','alphas','max_window','diffs','traj_number','fit_curve');
end

%% plot MSD
if strcmp(origin,'experimental')
	plotMSDs(deltaTs,MSD_all,median(alphas),max_window,MSD_traj,median(diffs),traj_number,fit_curve);
end

%% autocorrelation of angles


%% bayesian comparison of MSD versus model (free diffusion, constraint and subdiffusion)


%% sliding window for switching behaviour


%% closing the function
cd(folderName);
fclose(Output);

if verbose>0 disp('Done !'); end

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%      End of the main program
%%%%%%%%%%%%%%%%      Beginning of the related functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function [track_all_conf,track_all_locus,track_all_maxInt,track_all_nucleus, traj_number] = testLocalConf ...
(minLocalConf,rejectWindow,track_all_conf,track_all_locus,track_all_maxInt,track_all_nucleus,traj_number, minLength)

delTrajsLoc = fopen('deleted_trajectories_local.txt', 'w');
fprintf(delTrajsLoc, 'Deleted trajectories due to local parameter (too many NaNs)\n');
fprintf(delTrajsLoc, 'minLocalConf = %.2d\n',minLocalConf);
fprintf(delTrajsLoc, 'minimum ratio length/NaNs = %d\n',minLength);

deletedTraj = 0;
for track = length(traj_number):-1:1
	fprintf(delTrajsLoc, '%s; Number of NaNs timepoints %i \n',traj_number{track},sum(track_all_conf{track}<minLocalConf));
	if(length(track_all_conf{track})/(sum(track_all_conf{track}<minLocalConf)+sum(isnan(track_all_conf{track}))) ...
			<=minLength)
		fprintf(delTrajsLoc, 'delete %s; Number of NaNs timepoints %i \n',traj_number{track},sum(track_all_conf{track}<minLocalConf));
		track_all_locus(track)=[];
		track_all_nucleus(track)=[];
		track_all_maxInt(track)=[];
		track_all_conf(track)=[];
		traj_number(track)=[];
		deletedTraj = deletedTraj +1;
		continue
	end
	for tp = 1:length(track_all_conf{track}) % timepoint
		if track_all_conf{track}(tp)<minLocalConf
			track_all_locus{track}(tp,:)=nan;
			track_all_nucleus{track}(tp,:)=nan;
			track_all_maxInt{track}(tp,:)=nan;
			track_all_conf{track}(tp,:)=nan;
		end
	end
end

fprintf(delTrajsLoc,'%d trajectories got deleted (confidence test)\n', deletedTraj);
fprintf(delTrajsLoc, 'Number of trajectories (kept) = %d\n', length(traj_number));
fprintf(delTrajsLoc, 'Number of trajectories (deleted) = %d\n', deletedTraj);
fclose(delTrajsLoc);

end

function saveTrajIndiv(track_all_locus,traj_number)
mkdir individual_tracks
cd individual_tracks

for i = 1:length(track_all_locus)
	figure('Name','trajectory','NumberTitle','off');
	figTitle = sprintf('Trajectory %s',traj_number{i});
	figName = sprintf('Trajectory_%s', regexprep(traj_number{i},' ','_'));
	subplot(2,1,1)
	title(strcat(figTitle),'FontSize',15);
	plot(track_all_locus{i}(:,1));
	xlabel('time (frame)','FontSize',10);ylabel('position along X (um)','FontSize',10);
	subplot(2,1,2)
	plot(track_all_locus{i}(:,2));
	xlabel('time (frame)','FontSize',10);ylabel('position along Y (um)','FontSize',10);
	
	saveas(gcf,strcat(figName,'.png'));
	saveas(gcf,strcat(figName,'.fig'));
	close trajectory
end

cd ..
end

function save_fit_indiv(MSD_traj, traj_number, deltaTs, alphas, diffs, fit_curve, coefs_fit, do3D)
% plots and save plots of the individual trajectories


if ~exist('individual_plots', 'dir')
  mkdir('individual_plots');
end
cd individual_plots

for i = 1:length(traj_number)
	fit_traj = 2*(2+do3D)*diffs(i)*fit_curve(:,1).^alphas(i);
	figure('Name','traj_fit','NumberTitle','off');
	figTitle = sprintf('Experimental and fitted MSDs of %s',traj_number{i});
	figName = sprintf('MSD_fit_exp_%s', regexprep(traj_number{i},' ','_'));
	
	subplot(2,1,1); % loglog graph (indiv + fit)
	loglog(deltaTs,MSD_traj.mean(i,:)); 
	hold on; loglog(fit_curve(:,1),fit_traj,'Color','r');
	title(strcat(figTitle,' (loglog)'),'FontSize',15);
	xlabel('DeltaT(sec)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);
	
	legend(1) = {sprintf('alpha = %.2f',alphas(i))};
	legend(2) = {sprintf('B = %.2eum/sec',diffs(i))};
	if isfield(coefs_fit,'gof')
		legend(3) = {sprintf('sse = %.2e',coefs_fit.gof.sse)}; % global MSD
	else
		legend(3) = {sprintf('sse = %.2e',coefs_fit{1,i}.gof.sse)}; % indiv MSD
	end
	text(deltaTs(1),max(MSD_traj.mean(i,:)),legend,'Color','k','BackGround','w');
	clear legend
	
	
	subplot(2,1,2); % linear graph (indiv + fit)
	shadedErrorBar(deltaTs,MSD_traj.mean(i,:),MSD_traj.std(i,:),'b'); 
	hold on; plot(fit_curve(:,1),fit_traj,'Color','r');
	title(strcat(figTitle,' (linear)'),'FontSize',15);
	xlabel('DeltaT(sec)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);

	% erreur moindres carres
	
	
	saveas(gcf,strcat(figName,'.png'));
	saveas(gcf,strcat(figName,'.fig'));
	close traj_fit
	clear fit_traj;
end

cd ..
end

function [coefs_fit diffs alphas fit_curve] = fitToModel(MSD_traj,deltaTs,dwnsmple,fit_dwnsmple,do3D,origin,equation)
% coefs_fit -> full information of the fits (errors, params, values, ...)
% diffs -> list of the diffusion values
% alphas -> list of the alpha values
% fit_curve -> matrix : column 1 = DeltaT; column 2 = mean value of the fit for corresponding
% DeltaT; column 3 = median value of the fit for corresponding DeltaT
% The fitting equation is function of the number of dimensions

MSDs = MSD_traj.mean;

coefs_fit = {};
maxStep = floor(size(MSDs,2)*dwnsmple/fit_dwnsmple);
for i=1:size(MSDs,1)
	
	% StartPoint calibration
	startDiff = (MSDs(i,5)-MSDs(i,1))/(deltaTs(5)-deltaTs(1));
	startAlpha = log(MSDs(i,round(end/fit_dwnsmple))/MSDs(i,1))/log(deltaTs(round(end/fit_dwnsmple))-deltaTs(1));
	
	idxValid = ~isnan(MSDs(i,1:maxStep));
% 	[coefs_fit{i}.cfun coefs_fit{i}.gof coefs_fit{i}.output coefs_fit{i}.xdata coefs_fit{i}.ydata] ...
% 		=  fit(deltaTs(idxValid)',MSDs(i,idxValid)',equation,'StartPoint',[startDiff,startAlpha],'Weight',MSD_traj.weight(i,idxValid)');
	%%%% check start values !!!!!!
	
 	[coefs_fit{i}.cfun coefs_fit{i}.gof coefs_fit{i}.output coefs_fit{i}.xdata coefs_fit{i}.ydata] ...
 		=  fit(deltaTs(idxValid)',MSDs(i,idxValid)',equation,'StartPoint',[1.0e-04,0.5],'Weight',MSD_traj.weight(i,idxValid)');
	temp = confint(coefs_fit{i}.cfun);
	diffs(i) = sum(temp(:,1))/2;
	alphas(i) = sum(temp(:,2))/2;
end

save('fit_values_full_infos', 'coefs_fit');


if strcmp(origin,'experimental')
	figure;
	subplot(2,1,1);
	hist(alphas,15); title('alpha dispersion','FontSize',15);
	xlabel('alpha','FontSize',10); ylabel('population','FontSize',10);
	subplot(2,1,2);
	boxplot(alphas);
	ylabel('alpha','FontSize',10);
	legend(1) = {sprintf('mean(alpha) = %.2f',nanmean(alphas))};
	legend(2) = {sprintf('median(alpha) = %.2f',nanmedian(alphas))};
	text(0.6,0.6,legend,'Color','k','BackGround','w');
	saveas(gcf,strcat('alpha_dispersion.png'));
	saveas(gcf,strcat('alpha_dispersion.fig'));
	
	clear legend
	
	figure;
	subplot(2,1,1);
	hist(diffs,15); title('diff dispersion','FontSize',15);
	xlabel('diff (um^2/sec)','FontSize',10); ylabel('population','FontSize',10);
	subplot(2,1,2);
	boxplot(diffs);
	ylabel('diff (um^2/sec)','FontSize',10);
	legend(1) = {sprintf('mean(B) = %.2eum/sec^{alpha}',nanmean(diffs))};
	legend(2) = {sprintf('median(B) = %.2eum/sec^{alpha}',nanmedian(diffs))};
	text(0.5,nanmean(diffs),legend,'Color','k','BackGround','w');
	saveas(gcf,strcat('diff_boxplot.png'));
	saveas(gcf,strcat('diff_boxplot.fig'));
	
	clear legend
end
% fprintf('Mean alpha coefficient : %d\n', nanmean(alphas));
% fprintf('Mean diffusion coefficient (um^2/sec): %d\n', nanmean(diffs));

fit_curve(:,1) = deltaTs(1:maxStep);
fit_curve(:,2) = 2*(2+do3D)*nanmean(diffs)*fit_curve(:,1).^nanmean(alphas);
fit_curve(:,3) = 2*(2+do3D)*nanmedian(diffs)*fit_curve(:,1).^nanmedian(alphas);

end

function mean_FWHM = errorMeasurement(track_all_locus,track_all_conf,timeStep,track_all_maxInt,pixelSize,simulated_data)


mkdir errorplots
cd errorplots


clear sig*;
clear timeStamp;
sigX = [];
sigY = [];
for trackNbr = 1:length(track_all_locus)
 	for timePoint = 1:length(track_all_locus{trackNbr})-1
		sigConf(trackNbr,timePoint) = track_all_conf{trackNbr}(timePoint);
		sigMaxInt(trackNbr,timePoint) = track_all_maxInt{trackNbr}(timePoint);
		timeStamp(trackNbr,timePoint) = timePoint*timeStep;
		
		sigX(trackNbr,timePoint) = track_all_locus{trackNbr}(timePoint,1)-track_all_locus{trackNbr}(timePoint+1,1);
		sigY(trackNbr,timePoint) = track_all_locus{trackNbr}(timePoint,2)-track_all_locus{trackNbr}(timePoint+1,2);
		eucDists(trackNbr,timePoint) = sqrt(sigX(trackNbr,timePoint)^2+sigY(trackNbr,timePoint)^2);
		%sigX(length(sigX)+1) = track_all_locus{trackNbr}(timePoint,1)-track_all_locus{trackNbr}(timePoint+1,1);
		%sigY(length(sigY)+1) = track_all_locus{trackNbr}(timePoint,2)-track_all_locus{trackNbr}(timePoint+1,2);
 	end
end
distX = nanmean(abs(sigX(:)));
distY = nanmean(abs(sigY(:)));
eucDist = nanmean(sqrt(sigX(:).^2+sigY(:).^2));
%fprintf('mean dist along X = %dum\n',distX);
%fprintf('mean dist along Y = %dum\n',distY);
%fprintf('mean 2D distance = %dum\n',eucDist);

mean_deltaX = nanmean(sigX(:));
mean_deltaY = nanmean(sigY(:));
std_deltaX = nanstd(sigX(:));
std_deltaY = nanstd(sigY(:));
%mean_eucDelta = nanmean(sqrt(sigX(:).^2+sigY(:).^2));
%std_eucDelta = nanstd(sqrt(sigX(:).^2+sigY(:).^2));
fprintf('mean delta along X (bias) = %dum\n',mean_deltaX);
fprintf('mean delta along Y (bias) = %dum\n',mean_deltaY);
%fprintf('std delta along X = %dum\n',std_deltaX);
%fprintf('std delta along Y = %dum\n',std_deltaY);
fprintf('FWHM delta along X = %dum\n',(std_deltaX*2.354));
fprintf('FWHM delta along Y = %dum\n',(std_deltaY*2.354));
mean_FWHM = (std_deltaY+std_deltaX)*2.354/2;
fprintf('mean FWHM 2D = %dum\n',mean_FWHM);

%% plot hist of errors
figure; hist(sigX(:),100); title('DeltaX','FontSize',15);
xlabel('Distance(um)','FontSize',10); ylabel('Population','FontSize',10);
saveas(gcf,strcat('Sigma_X.png'));
saveas(gcf,strcat('Sigma_X.fig'));

figure; hist(sigY(:),100); title('DeltaY','FontSize',15);
xlabel('Distance(um)','FontSize',10); ylabel('Population','FontSize',10);
saveas(gcf,strcat('Sigma_Y.png'));
saveas(gcf,strcat('Sigma_Y.fig'));

figure; hist(eucDists(:),100); title('Interframe distance','FontSize',15);
xlabel('Distance(um)','FontSize',10); ylabel('Population','FontSize',10);
saveas(gcf,strcat('Interframe_distance.png'));
saveas(gcf,strcat('Interframe_distance.fig'));

figure; plot(timeStamp,eucDists,'*'); title('Dist vs time','FontSize',15);
xlabel('Time(sec)','FontSize',10); ylabel('Distance(um)','FontSize',10);
saveas(gcf,strcat('Dist_vs_time.png'));
saveas(gcf,strcat('Dist_vs_time.fig'));

figure; plot(sigConf,eucDists,'*'); title('Dist vs Confidence Criteria','FontSize',15);
xlabel('Confidence Criteria','FontSize',10); ylabel('Distance(um)','FontSize',10);
saveas(gcf,strcat('Dist_vs_ConfCrit.png'));
saveas(gcf,strcat('Dist_vs_ConfCrit.fig'));

figure; plot(sigMaxInt,eucDists,'*'); title('Dist vs Intensity Max','FontSize',15);
xlabel('Locus Max intensity','FontSize',10); ylabel('Distance(um)','FontSize',10);
saveas(gcf,strcat('Dist_vs_Imax.png'));
saveas(gcf,strcat('Dist_vs_Imax.fig'));

% boxplots (average for 50bins) of distances, Iratio and Imax as a function of time
% downRatio
downRatio = 25;
miniSize = size(eucDists,2)-mod(size(eucDists,2),downRatio);
miniEucDists = reshape(eucDists(:,1:miniSize),[],floor(size(eucDists,2)/downRatio));
miniTimeStamp = timeStamp(1,floor(downRatio/2):downRatio:end);
miniTimeStamp = miniTimeStamp(1:size(miniEucDists,2));
%figure;boxplot(miniEucDists,miniTimeStamp,'symbol',''); title('Dist vs time','FontSize',15);
figure;boxplot(miniEucDists,miniTimeStamp); title('Dist vs time','FontSize',15);
set(gca,'YLim',[0  max(iqr(miniEucDists))*5]);
xlabel('Time(sec)','FontSize',10); ylabel('Distance(um)','FontSize',10);
saveas(gcf,strcat('Dist_vs_time_boxplots.png'));
saveas(gcf,strcat('Dist_vs_time_boxplots.fig'));

miniSigConf = reshape(sigConf(:,1:miniSize),[],floor(size(sigConf,2)/downRatio));
% figure; boxplot(miniSigConf,miniTimeStamp,'symbol',''); title('Intensity ratio (maxLocus/meanNuc) vs time','FontSize',15);
figure; boxplot(miniSigConf,miniTimeStamp); title('Confidence Criteria vs time','FontSize',15);
xlabel('Time(sec)','FontSize',10); ylabel('Confidence Criteria','FontSize',10);
saveas(gcf,strcat('ConfCrit_vs_time_boxplot.png'));
saveas(gcf,strcat('ConfCrit_vs_time_boxplot.fig'));

miniSigMax = reshape(sigMaxInt(:,1:miniSize),[],floor(size(sigConf,2)/downRatio));
% figure; boxplot(miniSigMax,miniTimeStamp,'symbol',''); title('Locus Max Intensity vs time','FontSize',15);
figure; boxplot(miniSigMax,miniTimeStamp); title('Locus Max Intensity vs time','FontSize',15);
xlabel('Time(sec)','FontSize',10); ylabel('Locus Max intensity','FontSize',10);
saveas(gcf,strcat('Imax_vs_time_boxplot.png'));
saveas(gcf,strcat('Imax_vs_time_boxplot.fig'));


% create sorted matrices distConf and distMaxInt
binNbr = 20; % number of bin for the boxplot/histogram
distConf(:,1) = reshape(eucDists,[],1);
distConf(:,2) = reshape(sigConf,[],1);
distConf = sortrows(distConf,2);

distMaxInt(:,1) = reshape(eucDists,[],1);
distMaxInt(:,2) = reshape(sigMaxInt,[],1);
distMaxInt = sortrows(distMaxInt,2);


[distConf_box xLabeldc] = boxplotting(distConf, binNbr);
figure; boxplot(distConf_box,xLabeldc); title('Dist vs Confidence Criteria','FontSize',15);
xlabel('Confidence Criteria','FontSize',10); ylabel('Dist(um)','FontSize',10);
saveas(gcf,strcat('Dist_vs_ConfCrit_boxplot.png'));
saveas(gcf,strcat('Dist_vs_ConfCrit_boxplot.fig'));

[distMaxInt_box xLabeldmi] = boxplotting(distMaxInt, binNbr);
figure; boxplot(distMaxInt_box,xLabeldmi); title('Dist vs Intensity Max','FontSize',15);
xlabel('Intensity max','FontSize',10); ylabel('Dist(um)','FontSize',10);
saveas(gcf,strcat('Dist_vs_Imax_boxplot.png'));
saveas(gcf,strcat('Dist_vs_Imax_boxplot.fig'));

% create a boxplot of dist vs conf and maxInt




%% ecriture des resultats
if simulated_data==1
	errorplot;
end

loc_error = fopen('localisation_error.txt', 'w');
fprintf(loc_error,'mean dist along X = %dum\n',distX);
fprintf(loc_error,'mean dist along Y = %dum\n',distY);
fprintf(loc_error,'mean 2D distance = %dum\n',eucDist);
fprintf(loc_error,'mean delta along X (bias) = %dum\n',mean_deltaX);
fprintf(loc_error,'mean delta along Y (bias) = %dum\n',mean_deltaY);
fprintf(loc_error,'std delta along X = %dum\n',std_deltaX);
fprintf(loc_error,'std delta along Y = %dum\n',std_deltaY);
fprintf(loc_error,'FWHM delta along X = %dum\n',(std_deltaX*2.354));
fprintf(loc_error,'FWHM delta along Y = %dum\n',(std_deltaY*2.354));
fprintf(loc_error,'mean FWHM 2D = %dum\n',mean_FWHM);

fclose(loc_error);

cd ..
end

function [matrix xlabel] = boxplotting(sortMat, binNbr)
% creates a tab at the right format for a boxplot graph with a single sorted sortMat
% sortMat(:,1) -> y
% sortMat(:,2) -> x (boxplot based on this column)

% get rid of NaN values
condition = isnan(sortMat(:,1));
sortMat(condition,:) = [];
condition = isnan(sortMat(:,2));
sortMat(condition,:) = [];

delta = (sortMat(end,2)-sortMat(1,2))/binNbr;
binning = [sortMat(1,2):delta:sortMat(end,2)];
xlabel = binning(1:end-1)+delta/2;

line = 1;
for bin = 2:length(binning)
	count = 1;
	while sortMat(line,2)<=binning(bin)
		matrix(count,bin-1) = sortMat(line,1);
		count = count+1;
		if line<size(sortMat,1)
			line = line+1;
		else
			break;
		end
	end
end
		
matrix(matrix==0)=NaN;

% 
% 
% for i = 1:size(sortMat,1)
% 	if sortMat(i,2)<=binning(bin+1)
% 		count = count+1;
% 		matrix(count,bin) = sortMat(i,1);
% 	else 
% 		bin = bin+1;
% 		count = 1;
% 		matrix(count,bin) = sortMat(i,1);
% 	end
% end
% 
% matrix(matrix==0)=NaN;
% 
end

function track_all_locus_corrected = drift_correction(track_all_locus,track_all_nucleus)
% Corrects the locus position for nucleoplasm xy drift
% center nucleoplasm positions around 0.0
for trackNbr = 1:length(track_all_locus)
	track_all_nucleus_centered{trackNbr}(:,1) = track_all_nucleus{trackNbr}(:,1)-track_all_nucleus{trackNbr}(1,1);
	track_all_nucleus_centered{trackNbr}(:,2) = track_all_nucleus{trackNbr}(:,2)-track_all_nucleus{trackNbr}(1,2);
end
% apply correction to the locus position
for trackNbr = 1:length(track_all_locus)
	track_all_locus_corrected{trackNbr}(:,1) = track_all_locus{trackNbr}(:,1)-track_all_nucleus_centered{trackNbr}(:,1);
	track_all_locus_corrected{trackNbr}(:,2) = track_all_locus{trackNbr}(:,2)-track_all_nucleus_centered{trackNbr}(:,2);
end
end

