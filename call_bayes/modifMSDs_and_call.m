






for i=size(nI_full_MSD,1):-1:1
	somme = sum(nI_full_MSD(i,:));
	if isnan(somme)
		nI_full_MSD(i,:) = [];
	else
		nI_full_MSD(i,:) = nI_full_MSD(i,:)*3/2;	
	end	
end
msd_params.model = {'DA','DR'};
results = msd_curves_bayes(deltaTs, nI_full_MSD', msd_params);
save('results','results');
