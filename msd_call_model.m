
function msd_call_model()
%% load the files produced by the simulation of Hua, that are containing the simulated 
% trajectories :
tic

clear;
close all;

isOpen = matlabpool('size');
if isOpen==0
    matlabpool(4);
end



% file containing the simulated trajectories
folderName = uigetdir(pwd,'Select folder where all the trajectories are');
cd(folderName);

inFolder = dir();

for i = 3:length(inFolder)
	if inFolder(i).isdir == 1 
		cd(inFolder(i).name);
		trajInFolder = dir('trajectory.mat');
		if length(trajInFolder)==1
			%fprintf('target is in folder %s\n',inFolder(i).name);
 			MSDlauncher(trajInFolder.name,inFolder(i).name)
		end
		cd(folderName);
	end
end



matlabpool close;

disp('Analysis terminated');

toc
end


function MSDlauncher(FileName,trajName)

tic
disp('importing simulated datas');
fprintf('beginning the analysis of %s\n\n',trajName);
simu = importdata(FileName);

%% test for an older version of the results
dumpFolder = strcat(regexprep(FileName,'.mat',''),'_analysis_noOverlap');
Folder = dir();
for i = 1:length(Folder)
	if strcmp(Folder(i).name,dumpFolder)
		disp('deleting old results');
		rmdir(dumpFolder,'s');
		break
	end
end
mkdir(dumpFolder)
cd(dumpFolder)



exp_params = {};
exp_params.timeStep = 1; % in sec (ARBITRARY UNITS !!!!!!!)
exp_params.pixelSize = 1.0e06; % in um
exp_params.do3D = 0;


for chr = 1:length(simu.coords) % chromosome
	if chr ~= 4;
		continue;
	end
	dumpFolder = strcat('MSD_simu_chr',num2str(chr),'_analysis');
	mkdir(dumpFolder)
	cd(dumpFolder)

	parfor seg = 1:size(simu.coords{chr},1) % segment of the polymer 
		%if the number of timesteps is	too high, RAM gets full
% 	for seg = 1:size(simu.coords{chr},1) % segment of the polymer
% 		if seg~=4
% 			continue;
% 		end
		
		% prepare the structure of the variables
		full_data_1_locus = {};
		
		% only 1 long trajectory,beginner error... to be changed if time...
		full_data_1_locus.traj_number = {'traj_1'};
		
		tp = size(simu.coords{chr},3); % Nbr of timepoints
		% force high confidence (simulation...)
		full_data_1_locus.track_all_conf{1}(1:tp,1) = 10;
		% force high max intensity (simulation...)
		full_data_1_locus.track_all_maxInt{1}(1:tp,1) = 10000;
		% fake constant nucleus position (simulation...)
		full_data_1_locus.track_all_nucleus{1}(1:tp,1:2) = 10*exp_params.pixelSize;
		
		% save segment position
		full_data_1_locus.track_all_locus{1} = squeeze(simu.coords{chr}(seg,1:2,:)*exp_params.pixelSize)';
		
		% create a name for the dumping (chromosome, segment)
		%workingFolder = regexprep(folderName,'.*/','');
		dumpFolder = strcat('MSD_simu_chr',num2str(chr),'_segment',num2str(seg),'_analysis');
		disp(strcat('analysing chr',num2str(chr),', segment',num2str(seg)));
		msd_analysis_locus_vBitbucket(full_data_1_locus,exp_params,dumpFolder,'model_Hua');
	end
	cd ..
end

toc
end