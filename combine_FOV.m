


function combine_FOV()
%% requires preconcatenated data using this structure
% % save every result in a single structure
% manip_140724_YHB59_Sc_nZ_pop1_1_1_rW1_ldrift.MSD_all = MSD_all;
% manip_140724_YHB59_Sc_nZ_pop1_1_1_rW1_ldrift.MSD_traj = MSD_traj;
% manip_140724_YHB59_Sc_nZ_pop1_1_1_rW1_ldrift.SD_track = SD_track;
% manip_140724_YHB59_Sc_nZ_pop1_1_1_rW1_ldrift.alphas = alphas;
% manip_140724_YHB59_Sc_nZ_pop1_1_1_rW1_ldrift.diffs = diffs;
% manip_140724_YHB59_Sc_nZ_pop1_1_1_rW1_ldrift.track_all_nucleus = track_all_nucleus;

datasAdress = uipickfiles('Prompt','Select concatenated data file');
datas = load(datasAdress{1});
deltaTs = (0.1:0.1:299.9);

fields = fieldnames(datas);
SD_global = {};

for FOV=1:numel(fields)
	currentDatas = datas.(fields{FOV});
	for traj = 1:length(currentDatas.SD_track)
		for dt = 1:length(currentDatas.SD_track{traj})
			if length(SD_global) < dt
				SD_global{dt} = currentDatas.SD_track{traj}{dt};
			else
				SD_global{dt} = vertcat(SD_global{dt},currentDatas.SD_track{traj}{dt});
			end
		end
	end
end

MSD_global = zeros(1,length(SD_global));

for dt = 1:length(SD_global)
	MSD_global(dt) = nanmean(SD_global{1,dt});
end


% loglog fit of the given vector
equation = 'alpha*x+const';
ft = fittype(equation);
% set startpoints for the fit as long as make sure of the presence of a nonNaN value everywhere
startDiff = (MSD_global(5)-MSD_global(1))/(deltaTs(5)-deltaTs(1));
startAlpha = log(MSD_global(round(end/3))/MSD_global(1))/log(deltaTs(round(end/3))/deltaTs(1));
% idxValid = ~isnan(MSD(1:maxStep));

% calculate fit per se
[coefs_fit.cfun coefs_fit.gof coefs_fit.output ] ...
    = fit(log(deltaTs)',log(MSD_global)',ft,'StartPoint',[startAlpha,startDiff]);

% extract results
temp = confint(coefs_fit.cfun);
alpha = mean(temp(:,1));
diff = exp(mean(temp(:,2)))/4;
clear temp


end

% Display global MSDs
figure
hold on;
plot((0.1:0.1:10),nZ.MSD_global(:,1:100),'b');
plot((0.1:0.1:10),Z.MSD_global(:,1:100),'r');
title('Global MSD 140724 YHB59 +/- Zeo','FontSize',18);
xlabel('\Deltat (sec)','FontSize',18); ylabel('MSD (\mum^2)','FontSize',18);
h_legend = legend('-Zeo (N=835)','+Zeo (N=682)','Location','NorthWest');
set(h_legend,'FontSize',16);
% 
% %% MSD -2*5% ouliers
% figure;
% hold on;
% plot((0.1:0.1:100),MSD_global_FOV0_stripped_mean_5(:,1:1000),'m');
% plot((0.1:0.1:100),MSD_global_FOV1_stripped_mean_5(:,1:1000),'r');
% plot((0.1:0.1:100),MSD_global_FOV2_stripped_mean_5(:,1:1000),'k');
% plot((0.1:0.1:100),MSD_global_FOV3_stripped_mean_5(:,1:1000),'b');
% title('MSD -2*5% outliers (FOV0123 140512)','FontSize',18);
% xlabel('\Deltat (sec)','FontSize',18); ylabel('MSD (\mum^2)','FontSize',18);
% h_legend = legend('MSD FOV0 -2*5% outliers','MSD FOV1 -2*5% outliers','MSD FOV2 -2*5% outliers',...
% 	'MSD FOV3 -2*5% outliers','Location','NorthWest');
% set(h_legend,'FontSize',16);
% 
% %% MSD -2*10% ouliers
% figure;
% hold on;
% plot((0.1:0.1:100),MSD_global_FOV0_stripped_mean_10(:,1:1000),'m');
% plot((0.1:0.1:100),MSD_global_FOV1_stripped_mean_10(:,1:1000),'r');
% plot((0.1:0.1:100),MSD_global_FOV2_stripped_mean_10(:,1:1000),'k');
% plot((0.1:0.1:100),MSD_global_FOV3_stripped_mean_10(:,1:1000),'b');
% title('MSD -2*10% outliers (FOV0123 140512)','FontSize',18);
% xlabel('\Deltat (sec)','FontSize',18); ylabel('MSD (\mum^2)','FontSize',18);
% h_legend = legend('MSD FOV0 -2*10% outliers','MSD FOV1 -2*10% outliers','MSD FOV2 -2*10% outliers',...
% 	'MSD FOV3 -2*10% outliers','Location','NorthWest');
% set(h_legend,'FontSize',16);
% 
% %% MSD -2*15% ouliers
% figure;
% hold on;
% plot((0.1:0.1:100),MSD_global_FOV0_stripped_mean_15(:,1:1000),'m');
% plot((0.1:0.1:100),MSD_global_FOV1_stripped_mean_15(:,1:1000),'r');
% plot((0.1:0.1:100),MSD_global_FOV2_stripped_mean_15(:,1:1000),'k');
% plot((0.1:0.1:100),MSD_global_FOV3_stripped_mean_15(:,1:1000),'b');
% title('MSD -2*15% outliers (FOV0123 140512)','FontSize',18);
% xlabel('\Deltat (sec)','FontSize',18); ylabel('MSD (\mum^2)','FontSize',18);
% h_legend = legend('MSD FOV0 -2*15% outliers','MSD FOV1 -2*15% outliers','MSD FOV2 -2*15% outliers',...
% 	'MSD FOV3 -2*15% outliers','Location','NorthWest');
% set(h_legend,'FontSize',16);
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
