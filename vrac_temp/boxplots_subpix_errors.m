


data_CM_raw = load('/media/data2/Datas_microscopie/dynamique/simulation/Yeast5_results/results_PSF_3_raw_CM_analysis/variables.mat');
data_CM_raw_name = 'CM_raw';
data_CM_smooth = load('/media/data2/Datas_microscopie/dynamique/simulation/Yeast5_results/results_PSF_3_smooth_CM_analysis/variables.mat');
data_CM_smooth_name = 'CM_smooth';
data_GF_raw = load('/media/data2/Datas_microscopie/dynamique/simulation/Yeast5_results/results_PSF_3_raw_GF_analysis/variables.mat');
data_GF_raw_name = 'GF_raw';
data_GF_smooth = load('/media/data2/Datas_microscopie/dynamique/simulation/Yeast5_results/results_PSF_3_smooth_GF_analysis/variables.mat');
data_GF_smooth_name = 'GF_smooth';



auxgroup = [ones(size(data_CM_raw.floatPosX),1); 2*ones(size(data_CM_smooth.floatPosX),1); 3*ones(size(data_GF_raw.floatPosX),1); ...
	4*ones(size(data_GF_smooth.floatPosX),1)];

%% X position
auxX = [data_CM_raw.floatPosX; data_CM_smooth.floatPosX; data_GF_raw.floatPosX; data_GF_smooth.floatPosX];

%% Y position
auxY = [data_CM_raw.floatPosY; data_CM_smooth.floatPosY; data_GF_raw.floatPosY; data_GF_smooth.floatPosY];

%% X error
errX = [data_CM_raw.errorSimX; data_CM_smooth.errorSimX; data_GF_raw.errorSimX; data_GF_smooth.errorSimX];

%% X error
errY = [data_CM_raw.errorSimY; data_CM_smooth.errorSimY; data_GF_raw.errorSimY; data_GF_smooth.errorSimY];

%% plot
labels = {data_CM_raw_name, data_CM_smooth_name, data_GF_raw_name, data_GF_smooth_name};


fig_subpixel_boxplots = figure('Name','subpixel Xpositions boxplots');
boxplot(auxX,auxgroup,'labels', labels); title('subpixel Xpositions boxplots');
ylabel('intrapixel position (pixel)','FontSize',10);

fig_subpixel_boxplots = figure('Name','subpixel Ypositions boxplots');
boxplot(auxY,auxgroup,'labels', labels); title('subpixel Ypositions boxplots');
ylabel('intrapixel position (pixel)','FontSize',10);

fig_subpixel_boxplots = figure('Name','errorX boxplots');
boxplot(errX,auxgroup,'labels', labels); title('errorX boxplots');
ylabel('dist(absolute-measured) (pixel)','FontSize',10);

fig_subpixel_boxplots = figure('Name','errorY boxplots');
boxplot(errY,auxgroup,'labels', labels); title('errorY boxplots');
ylabel('dist(absolute-measured) (pixel)','FontSize',10);



