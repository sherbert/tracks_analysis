



data_YHB46 = load('/media/data2/Analyses_MSD/4R/fit_values_short_121031_YHB46_100msec_.mat');
data_YHB46_name = 'YHB46';
data_YHB53 = load('/media/data2/Analyses_MSD/4R/fit_values_short_120828_YHB53_100msec_.mat');
data_YHB53_name = 'YHB53';
data_YHB62 = load('/media/data2/Analyses_MSD/4R/fit_values_short_121031_YHB62_100msec_1.mat');
data_YHB62_name = 'YHB62';
data_YPT240 = load('/media/data2/Analyses_MSD/4R/fit_values_short_120828_YPT240_100msec_1.mat');
data_YPT240_name = 'YPT_240';


auxgroup = [ones(size(data_YHB46.alphas,2),1); 2*ones(size(data_YHB53.alphas,2),1); 3*ones(size(data_YHB62.alphas,2),1); ...
	4*ones(size(data_YPT240.alphas,2),1)];

%% alphas
auxAlphas = [data_YHB46.alphas'; data_YHB53.alphas'; data_YHB62.alphas'; data_YPT240.alphas'];

%% Y position
auxDiffs = [data_YHB46.diffs'; data_YHB53.diffs'; data_YHB62.diffs'; data_YPT240.diffs'];
% 
% %% X error
% errX = [data_YHB46.errorSimX; data_YHB53.errorSimX; data_YHB62.errorSimX; data_YPT240.errorSimX];
% 
% %% X error
% errY = [data_YHB46.errorSimY; data_YHB53.errorSimY; data_YHB62.errorSimY; data_YPT240.errorSimY];

%% plot
labels = {data_YHB46_name, data_YHB53_name, data_YHB62_name, data_YPT240_name};


fig_subpixel_boxplots = figure('Name','Alphas dispersions');
boxplot(auxAlphas,auxgroup,'labels', labels); title('Alphas dispersions');
xlabel('strain','FontSize',10);

fig_subpixel_boxplots = figure('Name','Diffusion dispersions');
boxplot(auxDiffs,auxgroup,'labels', labels); title('Diffusion dispersions');
xlabel('strain','FontSize',10); ylabel('Diffusion (um^2/sec^{alpha})','FontSize',10);

% fig_subpixel_boxplots = figure('Name','errorX boxplots');
% boxplot(errX,auxgroup,'labels', labels); title('errorX boxplots');
% ylabel('dist(absolute-measured) (pixel)','FontSize',10);
% 
% fig_subpixel_boxplots = figure('Name','errorY boxplots');
% boxplot(errY,auxgroup,'labels', labels); title('errorY boxplots');
% ylabel('dist(absolute-measured) (pixel)','FontSize',10);



