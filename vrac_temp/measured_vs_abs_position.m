


close all;
clear;

load('variables.mat');


measX = floatPosX;
measY = floatPosY;

simX = simVal(:,1)-floor(simVal(:,1));
simY = simVal(:,1)-floor(simVal(:,1));


figure; plot(simX,measX,'*'); title('measured\_vs\_abs\_position','FontSize',15);
xlabel('absolute position (pixel)','FontSize',10); ylabel('measured position (pixel)','FontSize',10);
saveas(gcf,'measured_vs_abs_position.png');
saveas(gcf,'measured_vs_abs_position.fig');

cd ..