


data2_raw = load('/media/data2/Datas_microscopie/dynamique/simulation/Yeast5_results/results_PSF_2_raw_analysis/variables.mat');
data2_raw_name = 'radPSF_2_raw';
data2_smooth = load('/media/data2/Datas_microscopie/dynamique/simulation/Yeast5_results/results_PSF_2_smooth_analysis/variables.mat');
data2_smooth_name = 'radPSF_2_smooth';

data3_raw = load('/media/data2/Datas_microscopie/dynamique/simulation/Yeast5_results/results_PSF_3_raw_analysis/variables.mat');
data3_raw_name = 'radPSF_3_raw';
data3_smooth = load('/media/data2/Datas_microscopie/dynamique/simulation/Yeast5_results/results_PSF_3_smooth_analysis/variables.mat');
data3_smooth_name = 'radPSF_3_smooth';

data4_raw = load('/media/data2/Datas_microscopie/dynamique/simulation/Yeast5_results/results_PSF_4_raw_analysis/variables.mat');
data4_raw_name = 'radPSF_4_raw';
data4_smooth = load('/media/data2/Datas_microscopie/dynamique/simulation/Yeast5_results/results_PSF_4_smooth_analysis/variables.mat');
data4_smooth_name = 'radPSF_4_smooth';

data5_raw = load('/media/data2/Datas_microscopie/dynamique/simulation/Yeast5_results/results_PSF_5_raw_analysis/variables.mat');
data5_raw_name = 'radPSF_5_raw';
data5_smooth = load('/media/data2/Datas_microscopie/dynamique/simulation/Yeast5_results/results_PSF_5_smooth_analysis/variables.mat');
data5_smooth_name = 'radPSF_5_smooth';



auxgroup = [ones(size(data2_raw.floatPosX),1); 2*ones(size(data2_smooth.floatPosX),1); 3*ones(size(data3_raw.floatPosX),1); ...
	4*ones(size(data3_smooth.floatPosX),1); 5*ones(size(data4_raw.floatPosX),1); 6*ones(size(data4_smooth.floatPosX),1); ...
	7*ones(size(data5_raw.floatPosX),1); 8*ones(size(data5_smooth.floatPosX),1)];

%% X position
auxX = [data2_raw.floatPosX; data2_smooth.floatPosX; data3_raw.floatPosX; data3_smooth.floatPosX; ...
	data4_raw.floatPosX; data4_smooth.floatPosX; data5_raw.floatPosX; data5_smooth.floatPosX];

%% Y position
auxY = [data2_raw.floatPosY; data2_smooth.floatPosY; data3_raw.floatPosY; data3_smooth.floatPosY; ...
	data4_raw.floatPosY; data4_smooth.floatPosY; data5_raw.floatPosY; data5_smooth.floatPosY];

%% X error
errX = [data2_raw.errorSimX; data2_smooth.errorSimX; data3_raw.errorSimX; data3_smooth.errorSimX; ...
	data4_raw.errorSimX; data4_smooth.errorSimX; data5_raw.errorSimX; data5_smooth.errorSimX];

%% X error
errY = [data2_raw.errorSimY; data2_smooth.errorSimY; data3_raw.errorSimY; data3_smooth.errorSimY; ...
	data4_raw.errorSimY; data4_smooth.errorSimY; data5_raw.errorSimY; data5_smooth.errorSimY];

%% plot
labels = {data2_raw_name, data2_smooth_name, data3_raw_name, data3_smooth_name, data4_raw_name, ...
	data4_smooth_name, data5_raw_name, data5_smooth_name};


fig_subpixel_boxplots = figure('Name','subpixel Xpositions boxplots');
boxplot(auxX,auxgroup,'labels', labels); title('X axis');

fig_subpixel_boxplots = figure('Name','subpixel Ypositions boxplots');
boxplot(auxY,auxgroup,'labels', labels); title('Y axis');

fig_subpixel_boxplots = figure('Name','errorX boxplots');
boxplot(errX,auxgroup,'labels', labels); title('X axis');

fig_subpixel_boxplots = figure('Name','errorY boxplots');
boxplot(errY,auxgroup,'labels', labels); title('Y axis');


% 
% load('/Users/czimmer/Dropbox/Wong_et_al/Wong_et_al-data-and-scripts/telteldistances_experimental/locloc_distance.mat');
% 
% d_6R3L_norapa = locus_pair.distance{1};
% 
% name_6R3L_norapa = locus_pair.name{1};
% 
% d_6R3L_rapa = locus_pair.distance{2};
% 
% name_6R3L_rapa = locus_pair.name{2};
% 
% d_6R4R_norapa = locus_pair.distance{3};
% 
% name_6R4R_norapa = locus_pair.name{3};
% 
% d_6R4R_rapa = locus_pair.distance{4};
% 
% name_6R4R_rapa = locus_pair.name{4};
% 
% 
%     fig_teltel_boxplots = figure('Name','tel-tel distances boxplots');
% 
%     aux = [d_6R3L_norapa(:); d_6R3L_rapa(:); d_6R4R_norapa(:); d_6R4R_rapa(:)];
% 
%        auxgroup = [ones(size(d_6R3L_norapa(:))); 2*ones(size(d_6R3L_rapa(:))); 3*ones(size(d_6R4R_norapa(:))); 4*ones(size(d_6R4R_rapa(:)))];
% 
%     boxplot(aux,auxgroup,'labels',{name_6R3L_norapa{1}, name_6R3L_rapa{1}, name_6R4R_norapa{1},  name_6R4R_rapa{1}});
% 
%     ylabel('distance (mu)');
% 
%     title('MEASURED');





