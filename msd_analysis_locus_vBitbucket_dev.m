
function msd_analysis_locus_vBitbucket_dev(full_data_1_locus,exp_params,dumpFolder,origin)
% The program recquires trajectories of equal length

close all;

%% PARAMS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

folderName = pwd;

if strcmp(origin,'experimental')
	useGlobalConfidence = 0; % reject tracks with low confidence
	useLocalConfidence = 1; % reject frames with low confidence
	minGlobalConf = 1.4; % define treshold for global rejection
	% minLocalConf = 1.6; % define treshold for local rejection (intensity ratio)
	minLocalConf = 5.0; % define treshold for local rejection (SNR max)
	errorMeasure = 0; % calculate the localisation error
	save_fit_ind = 1; % save each MSD and its associated fit in a separate file
	save_traj_ind = 0; % save each trajectory in a separate graph
	save_trajs = 1; % save trajectories values
	save_2D_traj = 0; % save each 2D trajectory in a subplot
	verbose = 0; % How talkative should the program be ?
elseif strcmp(origin,'model_Hua')
	useGlobalConfidence = 0; % reject tracks with low confidence
	useLocalConfidence = 1; % reject frames with low confidence
	minGlobalConf = 1.4; % define treshold for global rejection
	minLocalConf = 1.9; % define treshold for local rejection
	errorMeasure = 0; % calculate the localisation error
	save_fit_ind = 1; % save each MSD and its associated fit in a separate file
	save_traj_ind = 1; % save each trajectory in a separate graph
	save_trajs = 1; % save trajectories values
	save_2D_traj = 0; % save each 2D trajectory in a subplot
	verbose = 0; % How talkative should the program be ?
elseif strcmp(origin,'simu_numeric')
	useGlobalConfidence = 0; % reject tracks with low confidence
	useLocalConfidence = 0; % reject frames with low confidence
	minGlobalConf = 1.4; % define treshold for global rejection
	minLocalConf = 1.9; % define treshold for local rejection
	errorMeasure = 0; % calculate the localisation error
	save_fit_ind = 1; % save each MSD and its associated fit in a separate file
	save_traj_ind = 1; % save each trajectory in a separate graph
	save_trajs = 1; % save trajectories values
	save_2D_traj = 1; % save each 2D trajectory in a subplot
	verbose = 0; % How talkative should the program be ?
elseif strcmp(origin,'Sage')
	useGlobalConfidence = 0; % reject tracks with low confidence
	useLocalConfidence = 0; % reject frames with low confidence
	minGlobalConf = 1.4; % define treshold for global rejection
	minLocalConf = 1.9; % define treshold for local rejection
	errorMeasure = 0; % calculate the localisation error
	save_fit_ind = 1; % save each MSD and its associated fit in a separate file
	save_traj_ind = 1; % save each trajectory in a separate graph
	save_trajs = 1; % save trajectories values
	save_2D_traj = 1; % save each 2D trajectory in a subplot
	verbose = 0; % How talkative should the program be ?	
else
	disp('What is the origin of datas ?')
	return
end
	
testConfDiff = 1; % calculate the fit values for a model of normal confined diffusion 
testSsdifConf = 1; % calculate the fit values for a model of sbdiffusive confined diffusion 
nonOverlappingMSD = 1; % overlapping of DeltaT windows in MSD calculation
rejectWindow = 0; % define rejection window around a local rejection.
minLength = 2; % minimum ratio length/NaNs
dwnsmple = 1; % 1/dwnsmple of the traj is used MSD calculation
fit_dwnsmple = 3; % 1/fit_dwnsmple of the traj maxDeltais used for fitting
% maxDeltaT = 10; % use a max number of Deltat different from a proportion of the trajectory (sec)
useNucDisp = 0; % use the center of the nucleoplasm to correct for a nucleus drift
simulated_data = 0; % calculate supplementary informations of measured vs absolute values
run_MSDBayes = 0; % automatically run the MSD_Bayes program
% GLOBAL DRIFT OPTIONS ARE ONLY APPLICABLE UPON 2D TRACKS
measure_global_drift = 1; % measure the overall displacement as a function of time for the whole population
correct_global_drift = 1; % correct the trajectories with the global drift vector
alphaFix = 1; % fit alpha and B or only alpha


%% make sure that parameters are consistent
if correct_global_drift == 1 && measure_global_drift == 0
	measure_global_drift = 1;
	disp('Warning: You must authorize the program to measure the global drift if you want to correct for it');
	disp('Warning: setting measure_global_drift to 1');
end


if fit_dwnsmple<dwnsmple
	disp('minimum averaging of the trajectory has to be smaller than the fit downsampling');
	return
end

%% decondensing input variables
track_all_conf = full_data_1_locus.track_all_conf;
track_all_locus = full_data_1_locus.track_all_locus;
track_all_maxInt = full_data_1_locus.track_all_maxInt;
track_all_nucleus = full_data_1_locus.track_all_nucleus;
traj_number = full_data_1_locus.traj_number;
clear full_data_1_locus;

timeStep = exp_params.timeStep; % in sec
pixelSize = exp_params.pixelSize; % in um
do3D = exp_params.do3D;
clear exp_params;


%% test for an older version of the results
Folder = dir();
for i = 1:length(Folder)
	if strcmp(Folder(i).name,dumpFolder)
		disp('deleting old results');
		rmdir(dumpFolder,'s');
		break
	end
end
mkdir(dumpFolder)
cd(dumpFolder)

%% open output_MSD_analysis.txt file
Output = fopen('output_MSD_analysis.txt', 'w');
fprintf(Output, 'Program version = %s\n', mfilename);
fprintf(Output, 'Use the global confidence parameter : %d\n', useGlobalConfidence);
fprintf(Output, 'Minimum value of the global confidence parameter : %d\n', minGlobalConf);
fprintf(Output, 'Use the local confidence parameter : %d\n', useLocalConfidence);
fprintf(Output, 'Minimum value of the local confidence parameter : %d\n', minLocalConf);
fprintf(Output, 'Minimum window of positive confidence : %d\n', rejectWindow);
fprintf(Output, 'Minimum ratio length/NaNs : %d\n', minLength);
fprintf(Output, 'Use the nucleoplasm center to correct the drift = %d\n', useNucDisp);
fprintf(Output, 'Measure the global drift : %d\n', measure_global_drift);
fprintf(Output, 'Correct the global drift : %d\n', correct_global_drift);
fprintf(Output, 'Maximum time window is 1/%d of the complete trajectory\n', dwnsmple);
fprintf(Output, 'Proportion of the total trajectory used for the calculation of aplha and B : 1/%d\n', fit_dwnsmple);
fprintf(Output, 'Interframe time (in sec) : %d\n', timeStep);
fprintf(Output, 'Pixel size (in um) : %d\n', pixelSize);
fprintf(Output, '3D images : %d\n', do3D);
fprintf(Output, 'Number of trajectories (total) = %d\n', length(track_all_locus));
fprintf(Output, 'Measure localisation error : %d\n', errorMeasure);
fprintf(Output, 'No overlapping of DeltaT windows in MSD calculation : %d\n', nonOverlappingMSD);
fprintf(Output, 'Using the MSD Bayes : %d\n', run_MSDBayes);
fprintf(Output, 'Calculate the fit values for a model of normal confined diffusion : %d\n', testConfDiff);
fprintf(Output, 'Use a fix value of alpha = 0.5 : %d\n', alphaFix);



%% Correct for nucleus displacement
if useNucDisp == 1
	track_all_locus = drift_correction(track_all_locus,track_all_nucleus);
end

%% delete tracks with confidence coefficient too low (<=> MAX(locus)/MEAN(nucleoplasm)<minGlobalConf)
if useGlobalConfidence ==1
	deletedTraj = 0;
	%deletedTrajs = [''];
	deletedTrajectories = fopen('deleted_trajectories.txt', 'w');
	fprintf(deletedTrajectories, 'Deleted trajectories\n');
	for trackNbr = length(track_all_locus):-1:1
		if min(track_all_conf{trackNbr})<minGlobalConf
			if verbose>1; 
				fprintf('%s\nmin(track_all_conf{trackNbr}=%d)\n\n',traj_number{trackNbr}, ...
				min(track_all_conf{trackNbr}));
			end
			fprintf(deletedTrajectories, '%s\n',traj_number{trackNbr});
			deletedTraj = deletedTraj+1;
			track_all_locus(trackNbr)=[];
			traj_number(trackNbr)=[];
			track_all_nucleus(trackNbr)=[];
			track_all_conf(trackNbr)=[];
			%deletedTrajs = horzcat(deletedTrajs,int2str(trackNbr),' ');
		end
	end
	fprintf('%d trajectories got deleted (confidence test)\n', deletedTraj);
	fprintf(Output, 'Number of trajectories (kept) = %d\n', length(track_all_locus));
	fprintf(Output, 'Number of trajectories (deleted) = %d\n', deletedTraj);
	% 	fprintf('deleted trajectories : %s \n',deletedTrajs);
	fclose(deletedTrajectories);
end

%% Zap individual positive confidence tests
if rejectWindow >0
	track_all_conf = zapIndiv(rejectWindow,minLocalConf,track_all_conf);
end


%% Fill in low confidence frame in tracks with NaN positions
if useLocalConfidence == 1
	[track_all_conf,track_all_locus,track_all_maxInt,track_all_nucleus,traj_number] = testLocalConf(minLocalConf, ...
		track_all_conf,track_all_locus,track_all_maxInt,track_all_nucleus,traj_number,minLength);
	if length(track_all_locus)==0
		disp('Warning : 0 track past the local confidence test, stopping the analysis');
		return
	end
end

%% Measurement of the global drift
% measure_global_drift = 1; % measure the overall displacement as a function of time for the whole population
if measure_global_drift == 1
	disp('measuring global drift');
	global_2D_drift = measureglobalDrift(track_all_locus,timeStep);
end


%% Correction of the global drift
if correct_global_drift == 1 % correct the trajectories with the global drift vector
	disp('correcting for global drift');
	track_all_locus = correctGlobalDrift(track_all_locus,global_2D_drift);
end


%% Save individual trajectories
if save_traj_ind == 1
	saveTrajIndiv(track_all_locus, traj_number);
end


%% Save 2D trajectories (subplot 5x5)
if save_2D_traj == 1
	save2DTraj(track_all_locus, traj_number);
end


%% Save trajectories values in a single .mat file
if save_trajs==1
	save('tracks.mat','track_all_locus','traj_number');
end


%% Measure the localisation error
if errorMeasure == 1
	loc_error = errorMeasurement(track_all_locus,track_all_conf,timeStep,track_all_maxInt,pixelSize,simulated_data);
	fprintf(Output, '2D localisation error = %d\n',loc_error);
end

%% Calcul of MSD
%MSD_calculation();

% Find length of longest track and the maximum step number
% that will be calculated
max_length = 1;
for trackNbr = 1:length(track_all_locus)
	if (length(track_all_locus{trackNbr})>max_length)
		max_length=length(track_all_locus{trackNbr});
	end	
end
max_window = floor(max_length/dwnsmple)-1;
deltaTs = (timeStep:timeStep:timeStep*(max_window));

clear trackNbr;


SD_track = cell(length(track_all_locus),1); % squared displacement NOT averaged
SD_DeltaT = cell(max_window,1);

%for each track all SDs are measured
if verbose>0
	tic;
	disp('SD calculation = '); 
end
for trackNbr = 1:length(track_all_locus)
	current_track = track_all_locus{trackNbr};
	SD_track{trackNbr} = sd_calculation(current_track,dwnsmple,nonOverlappingMSD);
	for deltaT = 1:length(SD_track{trackNbr})
		SD_DeltaT{deltaT} = [SD_DeltaT{deltaT};SD_track{trackNbr}{deltaT}];		
	end
end
clear trackNbr;
if verbose>0; toc; end

% overall MSD calculation
if verbose>0 
	tic;
	disp('overall MSD calculation = '); 
end

for j = 1:max_window
	MSD_all.mean(j) = nanmean(SD_DeltaT{j});
	MSD_all.SEM(j) = nanstd(SD_DeltaT{j})/sqrt(length(SD_DeltaT{j})); % measure of the error on the mean not the dispersion
	MSD_all.std(j) = nanstd(SD_DeltaT{j}); % measure of the dispersion
 	MSD_all.median(j) = nanmedian(SD_DeltaT{j});
 	MSD_all.iqr(j) = iqr(SD_DeltaT{j});	
	MSD_all.weight(j) = sum(~isnan(SD_DeltaT{j}));
end
% parfor j = 1:max_window
% 	tempa(j) = nanmean(SD_DeltaT{j});
% 	tempb(j) = nanstd(SD_DeltaT{j});
%  	tempc(j) = nanmedian(SD_DeltaT{j});
%  	tempd(j) = iqr(SD_DeltaT{j});	
% end
% MSD_all.mean = tempa;
% MSD_all.SEM = tempb;
% MSD_all.median = tempc;
% MSD_all.iqr = tempd;
if verbose>0; toc; end

% single trajectories MSDs calculation
if verbose>0 
	tic; 
	disp('single trajectories MSDs calculation = ');
end
for i = 1:length(track_all_locus)
	for j = 1:length(SD_track{i})
		MSD_traj.mean(i,j) = nanmean(SD_track{i}{j});
		MSD_traj.SEM(i,j) = nanstd(SD_track{i}{j})/sqrt(length(SD_track{i}{j})); % measure of the error on the mean not the dispersion
		MSD_traj.std(i,j) = nanstd(SD_track{i}{j}); % measure of the dispersion
		MSD_traj.weight(i,j) = sum(~isnan(SD_track{i}{j}));
	end
end
if verbose>0; toc; end

% % mean MSD calculation => is the same as long as the samples are the same size !!!
% tic
% disp('mean MSD calculation = ')
% mean_MSD = nanmean(MSD_traj.mean,);
% 	
	
%% calculation of the mean power law and diffusion coefficient
if alphaFix == 1;
	equation = '0.5*x+const';
else
	equation = 'alpha*x+const';
end
ft = fittype(equation);
varLoc = 0; % localisation error (if needed)

%% fit individual MSDs
% maxStep = floor(size(MSD_all.mean,2)*dwnsmple/fit_dwnsmple);
maxStep = 100;
[coefs_fit diffs alphas fit_curve] = fitToModel(MSD_traj,deltaTs,dwnsmple,fit_dwnsmple,do3D,origin,ft,alphaFix,varLoc,maxStep);

if save_fit_ind
	save_fit_indiv(MSD_traj, traj_number, deltaTs, alphas, diffs, fit_curve, coefs_fit, do3D);
end

save('fit_values_short', 'diffs', 'alphas','traj_number');
fprintf(Output, '\nEquation of the model : %s\n', equation);
fprintf(Output, 'Mean alpha coefficient : %d\n', mean(alphas));
fprintf(Output, 'Std alpha coefficient : %d\n', std(alphas));
fprintf(Output, 'Median alpha coefficient : %d\n', median(alphas));
fprintf(Output, 'Iqr alpha coefficient : %d\n', iqr(alphas));
fprintf(Output, 'Mean diffusion coefficient (um^2/sec): %d\n', mean(diffs));
fprintf(Output, 'Std diffusion coefficient (um^2/sec): %d\n', std(diffs));
fprintf(Output, 'Median diffusion coefficient (um^2/sec): %d\n', median(diffs));
fprintf(Output, 'Iqr diffusion coefficient (um^2/sec): %d\n', iqr(diffs));
fprintf(Output, 'Localisation precision (um^2): 0\n');

% fit global MSD
if alphaFix == 0;
	[diff_global alpha_global coefs_fit_global]=loglogFit(MSD_all.mean,deltaTs,fit_dwnsmple,maxStep,ft,MSD_all.weight,do3D);
else 
	[diff_global alpha_global coefs_fit_global]=loglogFitFixAlpha(MSD_all.mean,deltaTs,fit_dwnsmple,maxStep,ft,MSD_all.weight,do3D);
end

% calculate the fit curve
fit_curve_global(:,1) = deltaTs(1:maxStep);
fit_curve_global(:,2) = 2*(2+do3D)*diff_global*fit_curve(:,1).^(alpha_global);

save('fit_values_short_global', 'diff_global', 'alpha_global');
fprintf(Output, 'Global alpha coefficient : %d\n', alpha_global);
fprintf(Output, 'Global diffusion coefficient : %d\n', diff_global);
traj_name = {'Global'};
save_fit_indiv(MSD_all, traj_name, deltaTs, alpha_global, diff_global, fit_curve_global, coefs_fit_global, do3D);


%% Fit for a constrained normal diffusion; !!!  doesn't take into account varLoc!!!
if testConfDiff==1
	[fit_const_indiv fit_const_global] = fitConstDiff(MSD_traj,deltaTs,dwnsmple,fit_dwnsmple,do3D,origin,varLoc,MSD_all);
	save('fit_const_diffs','fit_const_indiv','fit_const_global');
end


%% Fit for a constrained subdiffusion; !!!  doesn't take into account varLoc!!!
if testSsdifConf==1
	[fit_SsdifConf_indiv] = fitSsdifConst(MSD_traj,deltaTs,dwnsmple,fit_dwnsmple,do3D,origin,varLoc,MSD_all,traj_number,maxStep);
	save('fit_SsdifConf_diffs','fit_SsdifConf_indiv');
end


%% Dump calculated values

if strcmp(origin,'experimental');
	save('MSDs','MSD_traj','deltaTs','MSD_all','alphas','max_window','diffs','traj_number','fit_curve','SD_track',...
		'track_all_nucleus');
else strcmp(origin,'simu_Hua');
	save('MSDs_simu','MSD_traj','deltaTs','MSD_all','alphas','max_window','diffs','traj_number','fit_curve');
end

%% plot MSD
if strcmp(origin,'experimental')
	plotMSDs(deltaTs,MSD_all,median(alphas),max_window,MSD_traj,median(diffs),traj_number,fit_curve);
end

%% autocorrelation of angles


%% bayesian comparison of MSD versus model (free diffusion, constraint and subdiffusion)
if run_MSDBayes==1
	disp('Begin MSD Bayes analysis')
% 	maxStep = floor(size(MSD_all.mean,2)*dwnsmple/fit_dwnsmple);
	for i=size(MSD_traj.mean,1):-1:1
		somme = sum(MSD_traj.mean(i,1:maxStep));
		if ~isnan(somme)
			MSD_traj_bayes.mean(i,1:maxStep) = MSD_traj.mean(i,1:maxStep)*3/2;
		end
	end
	if size(MSD_traj_bayes.mean,1)~=0
		msd_params.model = {'DA','DR'};
		results_MSD_Bayes = msd_curves_bayes(deltaTs(1:maxStep), MSD_traj_bayes.mean', msd_params);
		save('results_MSD_Bayes','results_MSD_Bayes');
		clear results_MSD_Bayes
	else
		disp('MSD Bayes: no MSD without NaN => NO ANALYSIS');
	end
end


%% sliding window for switching behaviour 


%% closing the function
cd(folderName);
fclose(Output);

if verbose>0; disp('Done !'); end

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%      End of the main program
%%%%%%%%%%%%%%%%      Beginning of the related functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function track_all_conf = zapIndiv(rejectWindow,minLocalConf,track_all_conf)
for traj = 1:length(track_all_conf)
	countPos = 0; countNeg = 0;
	for time = 1:length(track_all_conf{traj})
% 		disp(sprintf('time = %d ; countPos = %d ; countNeg = %d', time, countPos, countNeg));
		if track_all_conf{traj}(time)>=minLocalConf
			countPos = countPos+1;
			countNeg = 0;
		else
			if (countPos <= rejectWindow && countNeg == 0)
				track_all_conf{traj}(max(time-rejectWindow,1):max(time-1,1)) = 0.1;
			end
			countPos = 0;
			countNeg = countNeg+1;
		end
	end	
	for time = length(track_all_conf{traj})-rejectWindow : length(track_all_conf{traj})
		if track_all_conf{traj}(time)<minLocalConf
			track_all_conf{traj}(time:end)=0.1;
		end
	end
end

end

function global_2D_drift = measureglobalDrift(track_all_locus,timeStep)

for traj = 1:length(track_all_locus)
	driftX_ind(:,traj) = track_all_locus{traj}(:,1)-track_all_locus{traj}(1,1);
	driftY_ind(:,traj) = track_all_locus{traj}(:,2)-track_all_locus{traj}(1,2);
end
meanDispX = nanmean(driftX_ind,2);
meanDispY = nanmean(driftY_ind,2);

h(1) = shadedErrorBar([timeStep:timeStep:timeStep*length(meanDispX)],meanDispX,nanstd(driftX_ind')','-b',1);
hold on; 
h(2) = shadedErrorBar([timeStep:timeStep:timeStep*length(meanDispY)],meanDispY,nanstd(driftY_ind')','-r',1);
title('overall x and y mean displacement','Fontsize',18)
xlabel('DeltaT(sec)','FontSize',12);ylabel('Overall mean displacement (um)','FontSize',12);
legend([h(1).mainLine,h(2).mainLine],'X position','Y position','Location','SouthWest');
saveas(gcf,strcat('global_2D_displacement.png'));
saveas(gcf,strcat('global_2D_displacement.fig'));

global_2D_drift = [smooth(meanDispX,15) smooth(meanDispY,15)];
save('global_2D_drift','global_2D_drift');

end

function track_all_locus = correctGlobalDrift(track_all_locus,global_2D_drift)

for traj = 1:length(track_all_locus)
	track_all_locus{traj}(:,1) = track_all_locus{traj}(:,1)-global_2D_drift(:,1);
	track_all_locus{traj}(:,2) = track_all_locus{traj}(:,2)-global_2D_drift(:,2);
end

end

function [track_all_conf,track_all_locus,track_all_maxInt,track_all_nucleus, traj_number] = testLocalConf ...
(minLocalConf,track_all_conf,track_all_locus,track_all_maxInt,track_all_nucleus,traj_number, minLength)

delTrajsLoc = fopen('deleted_trajectories_local.txt', 'w');
fprintf(delTrajsLoc, 'Deleted trajectories due to local parameter (too many NaNs)\n');
fprintf(delTrajsLoc, 'minLocalConf = %.2d\n',minLocalConf);
fprintf(delTrajsLoc, 'minimum ratio length/NaNs = %d\n',minLength);

deletedTraj = 0;
for track = length(traj_number):-1:1
	fprintf(delTrajsLoc, '%s; Number of NaNs timepoints %i \n',traj_number{track},sum(track_all_conf{track}<minLocalConf));
	if(length(track_all_conf{track})/(sum(track_all_conf{track}<minLocalConf)+sum(isnan(track_all_conf{track}))) ...
			<=minLength)
		fprintf(delTrajsLoc, 'delete %s; Number of NaNs timepoints %i \n',traj_number{track},sum(track_all_conf{track}<minLocalConf));
		track_all_locus(track)=[];
		track_all_nucleus(track)=[];
		track_all_maxInt(track)=[];
		track_all_conf(track)=[];
		traj_number(track)=[];
		deletedTraj = deletedTraj +1;
		continue
	end
	for tp = 1:length(track_all_conf{track}) % timepoint
		if track_all_conf{track}(tp)<minLocalConf
			track_all_locus{track}(tp,:)=nan;
			track_all_nucleus{track}(tp,:)=nan;
			track_all_maxInt{track}(tp,:)=nan;
			track_all_conf{track}(tp,:)=nan;
		end
	end
end

fprintf(delTrajsLoc,'%d trajectories got deleted (confidence test)\n', deletedTraj);
fprintf(delTrajsLoc, 'Number of trajectories (kept) = %d\n', length(traj_number));
fprintf(delTrajsLoc, 'Number of trajectories (deleted) = %d\n', deletedTraj);
fclose(delTrajsLoc);

end

function saveTrajIndiv(track_all_locus,traj_number)
mkdir individual_tracks
cd individual_tracks

for i = 1:length(track_all_locus)
	%if strcmp(traj_number{i},'traj 31')
	%	disp('31')
	%end
	figure('Name','trajectory','NumberTitle','off');
	figTitle = sprintf('Trajectory %s',traj_number{i});
	figName = sprintf('Trajectory_%s', regexprep(traj_number{i},' ','_'));
	subplot(2,1,1)
	title(strcat(figTitle),'FontSize',15);
	plot(track_all_locus{i}(:,1));
	xlabel('time (frame)','FontSize',10);ylabel('position along X (um)','FontSize',10);
	subplot(2,1,2)
	plot(track_all_locus{i}(:,2));
	xlabel('time (frame)','FontSize',10);ylabel('position along Y (um)','FontSize',10);
	
	saveas(gcf,strcat(figName,'.png'));
	saveas(gcf,strcat(figName,'.fig'));
	close trajectory
end

cd ..
end

function save2DTraj(track_all_locus, traj_number)
mkdir 2D_subplot
cd 2D_subplot

subymax = 4; % nombre de colonnes du subplot
subxmax = 4; % nombre de lignes du subplot

traj = 0;
traj_ori = 1;
flag = 0;
while traj<=length(track_all_locus)
	figure('Name','2D_subplot','NumberTitle','off');
	for subx = 1:subxmax
		for suby = 1:subymax
			traj = traj+1;
			if traj > length(track_all_locus)
				flag = 1;
				traj = traj-1;
				break
			end
			subplot(subymax,subxmax,subx+suby*subxmax-subxmax)
			plot(track_all_locus{traj}(:,1),track_all_locus{traj}(:,2))
			ylabel('y(um)')
			xlabel('x(um)')
			title(strcat('traj',num2str(traj)));
		end
		if flag == 1
			break
		end
	end
	traj_fin = traj;
	figName = sprintf('2D_subplot_traj_%d_to_%d',traj_ori,traj_fin);
	saveas(gcf,strcat(figName,'.png'));
	saveas(gcf,strcat(figName,'.fig'));
	close 2D_subplot
	traj_ori = traj+1;
	if flag == 1
		break
	end
end
cd ..
end

function save_fit_indiv(MSD_traj, traj_number, deltaTs, alphas, diffs, fit_curve, coefs_fit, do3D)
% plots and save plots of the individual trajectories


if ~exist('individual_plots', 'dir')
  mkdir('individual_plots');
end
cd individual_plots

for i = 1:length(traj_number)
% 	fit_traj = 2*(2+do3D)*diffs(i)*fit_curve(:,1).^alphas(i)+0.008; precision de pointe
	fit_traj = 2*(2+do3D)*diffs(i)*fit_curve(:,1).^alphas(i);
	figure('Name','traj_fit','NumberTitle','off');
	figTitle = sprintf('Experimental and fitted MSDs of %s',traj_number{i});
	figName = sprintf('MSD_fit_exp_%s', regexprep(traj_number{i},' ','_'));
	
	subplot(2,1,1); % loglog graph (indiv + fit)
	loglog(deltaTs,MSD_traj.mean(i,:)); 
	hold on; loglog(fit_curve(:,1),fit_traj,'Color','r');
	title(strcat(figTitle,' (loglog)'),'FontSize',15);
	xlabel('DeltaT(sec)','FontSize',10);ylabel('MSD(\mum^2)','FontSize',10);
	
	leg(1) = {sprintf('alpha = %.2f',alphas(i))};
	leg(2) = {sprintf('B = %.2eum^2/sec^{alpha}',diffs(i))};
	if isfield(coefs_fit,'gof')
		leg(3) = {sprintf('sse = %.2e',coefs_fit.gof.sse)}; % global MSD
	else
		leg(3) = {sprintf('sse = %.2e',coefs_fit{1,i}.gof.sse)}; % indiv MSD
	end
	% 	text(deltaTs(1),max(MSD_traj.mean(i,:)),leg,'Color','k','BackGround','w');
	textbp(leg,'Color','k','BackGround','w');
	clear leg
	
	
	subplot(2,1,2); % linear graph (indiv + fit)
	shadedErrorBar(deltaTs,MSD_traj.mean(i,:),MSD_traj.SEM(i,:),'b'); 
	hold on; plot(fit_curve(:,1),fit_traj,'Color','r');
	title(strcat(figTitle,' (linear)'),'FontSize',15);
	xlabel('DeltaT(sec)','FontSize',10);ylabel('MSD(\mum^2)','FontSize',10);

	% erreur moindres carres
	
	
	saveas(gcf,strcat(figName,'.png'));
	saveas(gcf,strcat(figName,'.fig'));
	close traj_fit
	clear fit_traj;
end

cd ..
end

function [diff alpha coefs_fit]=loglogFit(MSD,deltaTs,fit_dwnsmple,maxStep,ft,weight,do3D)
% loglog fit of the given vector

% set startpoints for the fit as long as make sure of the presence of a nonNaN value everywhere
startDiff = (MSD(5)-MSD(1))/(deltaTs(5)-deltaTs(1));
startAlpha = log(MSD(round(end/fit_dwnsmple))/MSD(1))/log(deltaTs(round(end/fit_dwnsmple))/deltaTs(1));
idxValid = ~isnan(MSD(1:maxStep));

% calculate fit per se
[coefs_fit.cfun coefs_fit.gof coefs_fit.output ] ...
    = fit(log(deltaTs(idxValid))',log(MSD(idxValid))',ft,'StartPoint',[startAlpha,startDiff],'Weight',weight(idxValid)');

% extract results
temp = confint(coefs_fit.cfun);
alpha = mean(temp(:,1));
if do3D
	diff = exp(mean(temp(:,2)))/6;
else
	diff = exp(mean(temp(:,2)))/4;
end
clear temp
end

function [diff alpha coefs_fit]=loglogFitFixAlpha(MSD,deltaTs,fit_dwnsmple,maxStep,ft,weight,do3D)
% loglog fit of the given vector with a fixed alpha = 0.5

% set startpoints for the fit as long as make sure of the presence of a nonNaN value everywhere
startDiff = (MSD(5)-MSD(1))/(deltaTs(5)-deltaTs(1));
idxValid = ~isnan(MSD(1:maxStep));

% calculate fit per se
[coefs_fit.cfun coefs_fit.gof coefs_fit.output ] ...
    = fit(log(deltaTs(idxValid))',log(MSD(idxValid))',ft,'StartPoint',[startDiff],'Weight',weight(idxValid)');

% extract results
temp = confint(coefs_fit.cfun);
alpha = 0.5;
if do3D
	diff = exp(mean(temp(:,1)))/6;
else
	diff = exp(mean(temp(:,1)))/4;
end
clear temp
end

	

function [coefs_fit diffs alphas fit_curve] = fitToModel(MSD_traj,deltaTs,dwnsmple,fit_dwnsmple,do3D,origin,ft,alphaFix,varLoc,maxStep)
% coefs_fit -> full information of the fits (errors, params, values, ...)
% diffs -> list of the diffusion values
% alphas -> list of the alpha values
% fit_curve -> matrix : column 1 = DeltaT; column 2 = mean value of the fit for corresponding
% DeltaT; column 3 = median value of the fit for corresponding DeltaT
% The fitting equation is function of the number of dimensions
% varLoc is the offset of the MSD due to precision localisation - Not used anymore

coefs_fit = {};

for i=1:size(MSD_traj.mean,1)
	if alphaFix ==0
		[diffs(i) alphas(i) coefs_fit{i}]=loglogFit(MSD_traj.mean(i,:),deltaTs,fit_dwnsmple,maxStep,ft,MSD_traj.weight,do3D);
	else
		[diffs(i) alphas(i) coefs_fit{i}]=loglogFitFixAlpha(MSD_traj.mean(i,:),deltaTs,fit_dwnsmple,maxStep,ft,MSD_traj.weight,do3D);
	end
end

save('fit_values_full_infos', 'coefs_fit');


if strcmp(origin,'experimental')
	figure;
	subplot(2,1,1);
	hist(alphas,15); title('alpha dispersion','FontSize',15);
	xlabel('alpha','FontSize',10); ylabel('population','FontSize',10);
	subplot(2,1,2);
	boxplot(alphas);
	ylabel('alpha','FontSize',10);
	leg(1) = {sprintf('mean(alpha) = %.2f',nanmean(alphas))};
	leg(2) = {sprintf('median(alpha) = %.2f',nanmedian(alphas))};
	text(0.6,0.6,leg,'Color','k','BackGround','w');
	saveas(gcf,strcat('alpha_dispersion.png'));
	saveas(gcf,strcat('alpha_dispersion.fig'));
	
	clear leg
	
	figure;
	subplot(2,1,1);
	hist(diffs,15); title('diff dispersion','FontSize',15);
	xlabel('diff (um^2/sec)','FontSize',10); ylabel('population','FontSize',10);
	subplot(2,1,2);
	boxplot(diffs);
	ylabel('diff (um^2/sec)','FontSize',10);
	leg(1) = {sprintf('mean(B) = %.2eum/sec^{alpha}',nanmean(diffs))};
	leg(2) = {sprintf('median(B) = %.2eum/sec^{alpha}',nanmedian(diffs))};
	text(0.5,nanmean(diffs),leg,'Color','k','BackGround','w');
	saveas(gcf,strcat('diff_boxplot.png'));
	saveas(gcf,strcat('diff_boxplot.fig'));
	
	clear leg
end
% fprintf('Mean alpha coefficient : %d\n', nanmean(alphas));
% fprintf('Mean diffusion coefficient (um^2/sec): %d\n', nanmean(diffs));

fit_curve(:,1) = deltaTs(1:maxStep);
fit_curve(:,2) = 2*(2+do3D)*nanmean(diffs)*fit_curve(:,1).^nanmean(alphas);
fit_curve(:,3) = 2*(2+do3D)*nanmedian(diffs)*fit_curve(:,1).^nanmedian(alphas);
% fit_curve(:,2) = 2*(2+do3D)*nanmean(diffs)*fit_curve(:,1).^nanmean(alphas)+0.008; % + precision de pointe
% fit_curve(:,3) = 2*(2+do3D)*nanmedian(diffs)*fit_curve(:,1).^nanmedian(alphas)+0.008; % + precision de pointe 

end

function [fit_const_indiv fit_const_global] = fitConstDiff(MSD_traj,deltaTs,dwnsmple,fit_dwnsmple,do3D,origin,varLoc,MSD_all)

% considering the linear fit ax+b for the first 10sec
% D = a/(2*Nbre dimension) 
% Rc = sqrt((5*plateau MSD)/(2*NbreDimensions))
% Protocol described in Neuman et al 2012.....

MSDs = MSD_traj.mean;
maxStep = floor(size(MSDs,2)*dwnsmple/fit_dwnsmple);
for traj=1:size(MSDs,1)
	currentMaxStep = maxStep;
	while (isnan(MSDs(traj,currentMaxStep)) && currentMaxStep>1)
		currentMaxStep = currentMaxStep-1;
	end
	p_indiv = polyfit(deltaTs(deltaTs<10),MSDs(traj,1:length(deltaTs(deltaTs<10))),1);
	fit_const_indiv{traj}.D_indiv = p_indiv(1)/(2*(2+do3D));
	fit_const_indiv{traj}.Rc_indiv = sqrt(5*MSDs(traj,currentMaxStep)/(2*(2+do3D)));
end

currentMaxStep = maxStep;
while(isnan(MSD_all.mean(1,currentMaxStep)) && currentMaxStep>1)
	currentMaxStep = currentMaxStep-1;
end
p_global = polyfit(deltaTs(deltaTs<10),MSD_all.mean(1,1:length(deltaTs(deltaTs<10))),1);
fit_const_global.D_global = p_global(1)/(2*(2+do3D));
fit_const_global.Rc_global = sqrt(5*MSD_all.mean(currentMaxStep)/(2*(2+do3D)));
end

function [fit_const_indiv] = fitSsdifConst(MSD_traj,deltaTs,dwnsmple,fit_dwnsmple,do3D,origin,varLoc,MSD_all,traj_number,maxStep)

% considering the linear fit Rc^2*(1-exp(-4*B*t^alpha/Rc^2)
% B diffusion prefactor
% Rc confinement radius
% alpha 



equation = 'Rc^2*(1-exp(-4*B*x^alpha/Rc^2))';
ft = fittype(equation);
% coeffnames(ft);
options = fitoptions(ft);
options.Lower = [0 0 0];
options.Upper = [1 10 5];
varLoc = 0; % localisation error (if needed)

for i=1:size(MSD_traj.mean,1)
	MSD = MSD_traj.mean(i,:);
	% 	[diffs(i) alphas(i) coefs_fit{i}]=loglogFit(MSD_traj.mean(i,:),deltaTs,fit_dwnsmple,maxStep,ft,MSD_traj.weight,do3D);
	% set startpoints for the fit as long as make sure of the presence of a nonNaN value everywhere
	startDiff = 0.003;
	startAlpha = 0.5;
	startRc = max(MSD(round(maxStep)));
	options.StartPoint = [startDiff,startRc,startAlpha];
	options.robust = 'on';
	idxValid = ~isnan(MSD(1:maxStep));
	options.Weights = MSD_traj.weight(i,idxValid)';
	% calculate fit per se
	[coefs_fit.cfun coefs_fit.gof coefs_fit.output ] ...
		= fit(deltaTs(idxValid)',MSD(idxValid)',ft,options);	
	% extract results
	temp = confint(coefs_fit.cfun);
	fit_const_indiv{i}.diffs = mean(temp(:,1))/4;
	diffs_SDC(i) = mean(temp(:,1))/4;
	fit_const_indiv{i}.Rcs = mean(temp(:,2));
	Rcs_SDC(i) = mean(temp(:,2));
	fit_const_indiv{i}.alphas = mean(temp(:,3));
	alphas_SDC(i) = mean(temp(:,3));
	clear temp
end

save('fit_values_short_ssdifConf', 'diffs_SDC', 'alphas_SDC', 'Rcs_SDC','traj_number');

end

function mean_FWHM = errorMeasurement(track_all_locus,track_all_conf,timeStep,track_all_maxInt,pixelSize,simulated_data)

mkdir errorplots
cd errorplots


clear sig*;
clear timeStamp;
sigX = [];
sigY = [];
for trackNbr = 1:length(track_all_locus)
 	for timePoint = 1:length(track_all_locus{trackNbr})-1
		sigConf(trackNbr,timePoint) = track_all_conf{trackNbr}(timePoint);
		sigMaxInt(trackNbr,timePoint) = track_all_maxInt{trackNbr}(timePoint);
		timeStamp(trackNbr,timePoint) = timePoint*timeStep;
		
		sigX(trackNbr,timePoint) = track_all_locus{trackNbr}(timePoint,1)-track_all_locus{trackNbr}(timePoint+1,1);
		sigY(trackNbr,timePoint) = track_all_locus{trackNbr}(timePoint,2)-track_all_locus{trackNbr}(timePoint+1,2);
		eucDists(trackNbr,timePoint) = sqrt(sigX(trackNbr,timePoint)^2+sigY(trackNbr,timePoint)^2);
		%sigX(length(sigX)+1) = track_all_locus{trackNbr}(timePoint,1)-track_all_locus{trackNbr}(timePoint+1,1);
		%sigY(length(sigY)+1) = track_all_locus{trackNbr}(timePoint,2)-track_all_locus{trackNbr}(timePoint+1,2);
 	end
end
distX = nanmean(abs(sigX(:)));
distY = nanmean(abs(sigY(:)));
eucDist = nanmean(sqrt(sigX(:).^2+sigY(:).^2));
%fprintf('mean dist along X = %dum\n',distX);
%fprintf('mean dist along Y = %dum\n',distY);
%fprintf('mean 2D distance = %dum\n',eucDist);

mean_deltaX = nanmean(sigX(:));
mean_deltaY = nanmean(sigY(:));
std_deltaX = nanstd(sigX(:));
std_deltaY = nanstd(sigY(:));
%mean_eucDelta = nanmean(sqrt(sigX(:).^2+sigY(:).^2));
%std_eucDelta = nanstd(sqrt(sigX(:).^2+sigY(:).^2));
fprintf('mean delta along X (bias) = %dum\n',mean_deltaX);
fprintf('mean delta along Y (bias) = %dum\n',mean_deltaY);
%fprintf('std delta along X = %dum\n',std_deltaX);
%fprintf('std delta along Y = %dum\n',std_deltaY);
fprintf('FWHM delta along X = %dum\n',(std_deltaX*2.354));
fprintf('FWHM delta along Y = %dum\n',(std_deltaY*2.354));
mean_FWHM = (std_deltaY+std_deltaX)*2.354/2;
fprintf('mean FWHM 2D = %dum\n',mean_FWHM);

%% plot hist of errors
figure; hist(sigX(:),100); title('DeltaX','FontSize',15);
xlabel('Distance(um)','FontSize',10); ylabel('Population','FontSize',10);
saveas(gcf,strcat('Sigma_X.png'));
saveas(gcf,strcat('Sigma_X.fig'));

figure; hist(sigY(:),100); title('DeltaY','FontSize',15);
xlabel('Distance(um)','FontSize',10); ylabel('Population','FontSize',10);
saveas(gcf,strcat('Sigma_Y.png'));
saveas(gcf,strcat('Sigma_Y.fig'));

figure; hist(eucDists(:),100); title('Interframe distance','FontSize',15);
xlabel('Distance(um)','FontSize',10); ylabel('Population','FontSize',10);
saveas(gcf,strcat('Interframe_distance.png'));
saveas(gcf,strcat('Interframe_distance.fig'));

figure; plot(timeStamp,eucDists,'*'); title('Dist vs time','FontSize',15);
xlabel('Time(sec)','FontSize',10); ylabel('Distance(um)','FontSize',10);
saveas(gcf,strcat('Dist_vs_time.png'));
saveas(gcf,strcat('Dist_vs_time.fig'));

figure; plot(sigConf,eucDists,'*'); title('Dist vs Confidence Criteria','FontSize',15);
xlabel('Confidence Criteria','FontSize',10); ylabel('Distance(um)','FontSize',10);
saveas(gcf,strcat('Dist_vs_ConfCrit.png'));
saveas(gcf,strcat('Dist_vs_ConfCrit.fig'));

figure; plot(sigMaxInt,eucDists,'*'); title('Dist vs Intensity Max','FontSize',15);
xlabel('Locus Max intensity','FontSize',10); ylabel('Distance(um)','FontSize',10);
saveas(gcf,strcat('Dist_vs_Imax.png'));
saveas(gcf,strcat('Dist_vs_Imax.fig'));

% boxplots (average for 50bins) of distances, Iratio and Imax as a function of time
% downRatio
downRatio = 25;
miniSize = size(eucDists,2)-mod(size(eucDists,2),downRatio);
miniEucDists = reshape(eucDists(:,1:miniSize),[],floor(size(eucDists,2)/downRatio));
miniTimeStamp = timeStamp(1,floor(downRatio/2):downRatio:end);
miniTimeStamp = miniTimeStamp(1:size(miniEucDists,2));
%figure;boxplot(miniEucDists,miniTimeStamp,'symbol',''); title('Dist vs time','FontSize',15);
figure;boxplot(miniEucDists,miniTimeStamp); title('Dist vs time','FontSize',15);
set(gca,'YLim',[0  max(iqr(miniEucDists))*5]);
xlabel('Time(sec)','FontSize',10); ylabel('Distance(um)','FontSize',10);
saveas(gcf,strcat('Dist_vs_time_boxplots.png'));
saveas(gcf,strcat('Dist_vs_time_boxplots.fig'));

miniSigConf = reshape(sigConf(:,1:miniSize),[],floor(size(sigConf,2)/downRatio));
% figure; boxplot(miniSigConf,miniTimeStamp,'symbol',''); title('Intensity ratio (maxLocus/meanNuc) vs time','FontSize',15);
figure; boxplot(miniSigConf,miniTimeStamp); title('Confidence Criteria vs time','FontSize',15);
xlabel('Time(sec)','FontSize',10); ylabel('Confidence Criteria','FontSize',10);
saveas(gcf,strcat('ConfCrit_vs_time_boxplot.png'));
saveas(gcf,strcat('ConfCrit_vs_time_boxplot.fig'));

miniSigMax = reshape(sigMaxInt(:,1:miniSize),[],floor(size(sigConf,2)/downRatio));
% figure; boxplot(miniSigMax,miniTimeStamp,'symbol',''); title('Locus Max Intensity vs time','FontSize',15);
figure; boxplot(miniSigMax,miniTimeStamp); title('Locus Max Intensity vs time','FontSize',15);
xlabel('Time(sec)','FontSize',10); ylabel('Locus Max intensity','FontSize',10);
saveas(gcf,strcat('Imax_vs_time_boxplot.png'));
saveas(gcf,strcat('Imax_vs_time_boxplot.fig'));


% create sorted matrices distConf and distMaxInt
binNbr = 20; % number of bin for the boxplot/histogram
distConf(:,1) = reshape(eucDists,[],1);
distConf(:,2) = reshape(sigConf,[],1);
distConf = sortrows(distConf,2);

distMaxInt(:,1) = reshape(eucDists,[],1);
distMaxInt(:,2) = reshape(sigMaxInt,[],1);
distMaxInt = sortrows(distMaxInt,2);


[distConf_box xLabeldc] = boxplotting(distConf, binNbr);
figure; boxplot(distConf_box,xLabeldc); title('Dist vs Confidence Criteria','FontSize',15);
xlabel('Confidence Criteria','FontSize',10); ylabel('Dist(um)','FontSize',10);
saveas(gcf,strcat('Dist_vs_ConfCrit_boxplot.png'));
saveas(gcf,strcat('Dist_vs_ConfCrit_boxplot.fig'));

[distMaxInt_box xLabeldmi] = boxplotting(distMaxInt, binNbr);
figure; boxplot(distMaxInt_box,xLabeldmi); title('Dist vs Intensity Max','FontSize',15);
xlabel('Intensity max','FontSize',10); ylabel('Dist(um)','FontSize',10);
saveas(gcf,strcat('Dist_vs_Imax_boxplot.png'));
saveas(gcf,strcat('Dist_vs_Imax_boxplot.fig'));

% create a boxplot of dist vs conf and maxInt




%% ecriture des resultats
if simulated_data==1
	errorplot;
end

loc_error = fopen('localisation_error.txt', 'w');
fprintf(loc_error,'mean dist along X = %dum\n',distX);
fprintf(loc_error,'mean dist along Y = %dum\n',distY);
fprintf(loc_error,'mean 2D distance = %dum\n',eucDist);
fprintf(loc_error,'mean delta along X (bias) = %dum\n',mean_deltaX);
fprintf(loc_error,'mean delta along Y (bias) = %dum\n',mean_deltaY);
fprintf(loc_error,'std delta along X = %dum\n',std_deltaX);
fprintf(loc_error,'std delta along Y = %dum\n',std_deltaY);
fprintf(loc_error,'FWHM delta along X = %dum\n',(std_deltaX*2.354));
fprintf(loc_error,'FWHM delta along Y = %dum\n',(std_deltaY*2.354));
fprintf(loc_error,'mean FWHM 2D = %dum\n',mean_FWHM);

fclose(loc_error);

cd ..
end

function [matrix xlabel] = boxplotting(sortMat, binNbr)
% creates a tab at the right format for a boxplot graph with a single sorted sortMat
% sortMat(:,1) -> y
% sortMat(:,2) -> x (boxplot based on this column)

% get rid of NaN values
condition = isnan(sortMat(:,1));
sortMat(condition,:) = [];
condition = isnan(sortMat(:,2));
sortMat(condition,:) = [];

delta = (sortMat(end,2)-sortMat(1,2))/binNbr;
binning = [sortMat(1,2):delta:sortMat(end,2)];
xlabel = binning(1:end-1)+delta/2;

line = 1;
for bin = 2:length(binning)
	count = 1;
	while sortMat(line,2)<=binning(bin)
		matrix(count,bin-1) = sortMat(line,1);
		count = count+1;
		if line<size(sortMat,1)
			line = line+1;
		else
			break;
		end
	end
end
		
matrix(matrix==0)=NaN;

% 
% 
% for i = 1:size(sortMat,1)
% 	if sortMat(i,2)<=binning(bin+1)
% 		count = count+1;
% 		matrix(count,bin) = sortMat(i,1);
% 	else 
% 		bin = bin+1;
% 		count = 1;
% 		matrix(count,bin) = sortMat(i,1);
% 	end
% end
% 
% matrix(matrix==0)=NaN;
% 
end

function track_all_locus_corrected = drift_correction(track_all_locus,track_all_nucleus)
% Corrects the locus position for nucleoplasm xy drift

% center nucleoplasm positions around 0.0
for trackNbr = 1:length(track_all_locus)
	track_all_nucleus_centered{trackNbr}(:,1) = track_all_nucleus{trackNbr}(:,1)-track_all_nucleus{trackNbr}(1,1);
	track_all_nucleus_centered{trackNbr}(:,2) = track_all_nucleus{trackNbr}(:,2)-track_all_nucleus{trackNbr}(1,2);
end
% smooth nucleus localisations in time (15dt)
for trackNbr = 1:length(track_all_locus)
	track_all_nucleus_centered{trackNbr}(:,1) = smooth(track_all_nucleus_centered{trackNbr}(:,1),15);
	track_all_nucleus_centered{trackNbr}(:,2) = smooth(track_all_nucleus_centered{trackNbr}(:,2),15);
end
% apply correction to the locus position
for trackNbr = 1:length(track_all_locus)
	track_all_locus_corrected{trackNbr}(:,1) = track_all_locus{trackNbr}(:,1)-track_all_nucleus_centered{trackNbr}(:,1);
	track_all_locus_corrected{trackNbr}(:,2) = track_all_locus{trackNbr}(:,2)-track_all_nucleus_centered{trackNbr}(:,2);
end
end

