

for i = 1:4
	for j = i+1:5
		[H, pValue, KSstatistic] = kstest_2s_2d([allMSDs{i}.alpha; allMSDs{i}.diffs]', [allMSDs{j}.alpha; allMSDs{j}.diffs]', 0.05);
		testp(i,j) = pValue;
		testKS(i,j) = KSstatistic;		
	end
end



h(1) = plot(nI_full_alphas,nI_full_diffs,'+','color','b');
hold on
h(2) = plot(I_full_alphas,I_full_diffs,'*','color','r');
title('anomalous exponent vs generalized diffusion coefficient','FontSize',15);
xlabel('anomalous exponent \alpha','FontSize',12);ylabel('Generalized diffusion coefficient (\mum^{2}/sec^{\alpha})','FontSize',12);
leg =legend([h(1),h(2)],'Not induced','Induced','location','NorthEast'); % add a legend for different pop
set(leg,'FontSize',12);



leg(1) = {'\alpha = 0.57'};
leg(2) = {'B = 3.08e^{-3}\mum/sec^{\alpha}'};
text(1,1,leg,'Color','k','BackGround','w','FontSize',12);
xlabel('\Deltat(sec)','FontSize',12);ylabel('MSD(\mum^{2})','FontSize',12);
