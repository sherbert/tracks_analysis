
function msd_call_exp()
%% load the files produced by the jython program (Fiji), that are containing the experimental 
%trajectories :

clear;
close all;

% isOpen = matlabpool('size');
% if isOpen==0
%     matlabpool(8);
% end

%% jython produced file
folderName = uipickfiles('REFilter','_results','REDirs',1,'Prompt','Select _results folders');



for i=1:length(folderName)
	cd(folderName{i});
	channels = dir('results_channel*');
	if length(channels)==0
		cd results
		if ispc
			launchAnalysis(strcat(folderName{i},'\results'));
		else
			launchAnalysis(strcat(folderName{i},'/results'));
		end
	else
		for j=1:length(channels)
			if isempty(regexp(channels(j).name, '.*analysis*'))
				cd(folderName{i})
				cd(channels(j).name)
				if ispc
					launchAnalysis(strcat(folderName{i},'\',channels(j).name));
				else
					launchAnalysis(strcat(folderName{i},'/',channels(j).name));
				end
			end
		end
	end
	close all
end

end

function launchAnalysis(folderName)
cd(folderName)
filesInFolder = dir('*_data.txt');

traj_number = {};
for i = 1:length(filesInFolder)
	clear datas
	datas = importdata(filesInFolder(i).name);
	traj_number{i} = regexprep(filesInFolder(i).name,'yeast_ROI','traj ');
	traj_number{i} = regexprep(traj_number{i},'_data.txt','');
	data_all{i} = datas.data;
end
header = datas.textdata;
clear datas

%% load settings
settings = importdata('condition_settings.txt');
timeStep = settings.data(1)/1000; % output is in sec
pixelSize = settings.data(2)/1000; % output is in um
do3D = settings.data(3); % 0 for no, 1 for yes


%% mise a l'echelle et sauvegarde des trajectoires

% import only a part of the trajectory
frameEnd = 9999;
frameEnd = min(frameEnd,size(data_all{1},1)); % test if frameEnd is not superior to the number of time point

if do3D == 1
 	for i=1:length(filesInFolder)
		track_all_locus{i} = data_all{i}(1:frameEnd,1:3).*pixelSize;
		track_all_nucleus{i} = data_all{i}(1:frameEnd,4:5).*pixelSize;
		
 	end
elseif do3D == 0
	for i=1:length(filesInFolder)
		track_all_locus{i} = data_all{i}(1:frameEnd,1:2)*pixelSize;
		track_all_nucleus{i} = data_all{i}(1:frameEnd,4:5)*pixelSize;
	end
else
	disp('In which dimension are you living ???')
	return
end

%% extraction des valeurs de confiance
for i=1:length(filesInFolder)
	% track_all_conf{i} = data_all{i}(1:frameEnd,6); % uses the intensity ratio
	track_all_conf{i} = data_all{i}(1:frameEnd,8); % uses the SNRmax
end

%% extraction des valeurs d'intensite max
for i=1:length(filesInFolder)
	track_all_maxInt{i} = data_all{i}(1:frameEnd,7);
end


full_data_1_locus = {};
full_data_1_locus.track_all_conf = track_all_conf;
full_data_1_locus.track_all_locus = track_all_locus;
full_data_1_locus.track_all_maxInt = track_all_maxInt;
full_data_1_locus.track_all_nucleus = track_all_nucleus;
full_data_1_locus.traj_number = traj_number;

exp_params = {};
exp_params.timeStep = timeStep;
exp_params.pixelSize = pixelSize;
exp_params.do3D = do3D;


% cd ..
% if ispc
% 	workingFolder = regexprep(folderName,'.*\','');
% else
% 	workingFolder = regexprep(folderName,'.*/','');
% end
% dumpFolder = strcat(workingFolder,'_full_dev_alphafix_overlapping_10s');
% msd_analysis_locus_vBitbucket_dev(full_data_1_locus,exp_params,dumpFolder,'experimental');

cd ..
prompt = {'Enter folder name for the results:'};
dlg_title = 'Dump folder';
num_lines = 1;
def = {'results_full'};
answer = inputdlg(prompt,dlg_title,num_lines,def);
if length(answer{1})==0
	folderName = 'results_full';
else
	folderName = answer{1};
end

msd_analysis_locus_vBitbucket(full_data_1_locus,exp_params,folderName,'experimental');

% matlabpool close;


end