
%%%%%%% toolbox for the different kinds of fit and different maxStep values


function toolbox_refit_maxStep()

datas = load('/media/HDD_2To_2/data2/Analyses_MSD/140724_YHB59_w_wo_Zeo_short_long_lamp/rW1_ldrift/datas_140724_rW1_ldrift.mat');
stepList = [10 100]; % #dt
dt = 0.1;

fields = fieldnames(datas);

for FOV=1:numel(fields)
	currentDatas = datas.(fields{FOV});
	deltaTs = (dt:dt:dt*length(currentDatas.MSD_all.mean));
% 	calculate_fit
	for stepLoop = 1:length(stepList)
		maxStep = stepList(stepLoop);
		for traj=1:size(currentDatas.MSD_traj.mean,1)
			idxValid = ~isnan(currentDatas.MSD_traj.mean(traj,1:maxStep));
			ft = fittype( @(D, alpha, x) log(4*D)+alpha*log(x) );
			[slope,intercept] = logfit(deltaTs(idxValid)',currentDatas.MSD_traj.mean(traj,idxValid)','loglog');
			diffs(traj) = 10^intercept/4;
			alphas(traj) = slope;
		end
		alphaField = sprintf('alpha%isec',stepLoop/dt);
		currentDatas.(alphaField)=alphas;
	end
end


