

% The program recquires trajectories of equal length

close all;
clear;

%%params
useConfidence = 1; % reject tracks with low confidence
minConf = 1.4; % define treshold for rejection
min_averaging = 2; % minimum number of windows
DParam = 2; % number of timepoints used for the calculation of the diffusion coefficient D
alphaParam = 3; % 1/alphaParam is the proportion of the total trajectory used for the calculation of the power low aplha
dumpAllMSDs = 1; % write all MSDs in a new file... not yet there
verbose = 0; % How talkative should the program be ?


%% load the files containing the trajectories :

% test file
% - track_all : contains locus x,y (z in case of 3D) position (in micron) vs time(frame)
% - timestep (msec)
% load('rand_tracks.mat'); 



% jython produced file
adress = pwd;
folderName = uigetdir(pwd,'Select folder where all the _data.txt files are');
%dimension = 2;
cd(folderName)
filesInFolder = dir('*_data.txt');

traj_number = {};
for i = 1:length(filesInFolder)
	clear datas

	datas = importdata(filesInFolder(i).name);
	traj_number{i} = regexprep(filesInFolder(i).name,'yeast_ROI','traj ');
	traj_number{i} = regexprep(traj_number{i},'_data.txt','');

	data_all{i} = datas.data;
end
header = datas.textdata;
clear datas

% load settings
settings = importdata('condition_settings.txt');
timeStep = settings.data(1)/1000; % in msec
pixelSize = settings.data(2)/1000; % in um
do3D = settings.data(3); % 0 for no, 1 for yes


% mise a l'echelle et sauvegarde des trajectoires
if do3D == 1
 	for i=1:length(filesInFolder)
		track_all_locus{i} = data_all{i}(:,1:3).*pixelSize;
		track_all_nucleus{i} = data_all{i}(:,4:5).*pixelSize;
 	end
elseif do3D == 0
	for i=1:length(filesInFolder)
		track_all_locus{i} = data_all{i}(:,1:2)*pixelSize;
		track_all_nucleus{i} = data_all{i}(:,4:5)*pixelSize;
	end
else
	disp('In which dimension are you living ???')
	return
end

% % mise a l'echelle
% for i=1:length(filesInFolder)
% 	track_all_locus{i} = data_all{i}(:,1:5).*pixelSize;
% end

% extraction des valeurs de confiance
for i=1:length(filesInFolder)
	track_all_conf{i} = data_all{i}(:,6);
end


cd ..
Folder = dir();
for i = 1:length(Folder)
	if strcmp(Folder(i).name,'results_analysis')
		disp('deleting old results');
		rmdir('results_analysis','s');
		break
	end
end
mkdir results_analysis
cd results_analysis/

%% simulate fake trajectories
% tic
% disp('simulation time = ');
% Ndim = 1; % Number of dimensions of the trajectory 
% maxstep = 1000;
% for i=1:100	
% 	track_all_locus{i} = track_simulation(Ndim,maxstep);
% end
% timestep = 15;
% toc

%% delete tracks with confidence coefficient too low (<=> MAX(locus)/MEAN(nucleoplasm)<minConf)

if useConfidence == 1
	deletedTraj = 0;
	%deletedTrajs = [''];
	deletedTrajectories = fopen('deleted_trajectories.txt', 'w');
	fprintf(deletedTrajectories, 'Deleted trajectories\n');
	for trackNbr = length(track_all_locus):-1:1
		if min(track_all_conf{trackNbr})<minConf
			if verbose>0; fprintf('%s\nmin(track_all_conf{trackNbr}=%d)\n\n',traj_number{trackNbr},min(track_all_conf{trackNbr})); end
			fprintf(deletedTrajectories, '%s\n',traj_number{trackNbr});
			deletedTraj = deletedTraj+1;
			track_all_locus(trackNbr)=[];
			traj_number(trackNbr)=[];
			%deletedTrajs = horzcat(deletedTrajs,int2str(trackNbr),' ');
		end
	end
	fprintf('%d trajectories got deleted (confidence test)\n',deletedTraj);
	% 	fprintf('deleted trajectories : %s \n',deletedTrajs);
	fclose(deletedTrajectories);
end



%% Fill in any gaps in track with NaN positions : external function


%% Correct for nucleus displacement



%% Calcul of MSD

% Find length of longest track and the maximum step number
% that will be calculated
max_length = 1;
for trackNbr = 1:length(track_all_locus)
	if (length(track_all_locus{trackNbr})>max_length)
		max_length=length(track_all_locus{trackNbr});
	end	
end
max_window = floor(max_length/min_averaging);
timeBar = [timeStep:timeStep:timeStep*max_window];
clear trackNbr;


SD_track = cell(length(track_all_locus),1); % squared displacement NOT averaged
SD_DeltaT = cell(max_window,1);

%for each track all SDs are measured
tic
disp('SD calculation = ')
for trackNbr = 1:length(track_all_locus)
	current_track = track_all_locus{trackNbr};
	SD_track{trackNbr} = sd_calculation(current_track,min_averaging);
	for deltaT = 1:length(SD_track{trackNbr})
		SD_DeltaT{deltaT} = [SD_DeltaT{deltaT};SD_track{trackNbr}{deltaT}];		
	end
end
clear trackNbr;
toc

% overall MSD calculation
tic
disp('overall MSD calculation = ')
for j = 1:max_window
	MSD_mean_all(j) = mean(SD_DeltaT{j});
	MSD_std_all(j) = std(SD_DeltaT{j});
 	MSD_median_all(j) = median(SD_DeltaT{j});
 	MSD_iqr_all(j) = iqr(SD_DeltaT{j});	
end
toc

% single trajectories MSDs calculation
tic
disp('single trajectories MSDs calculation = ')
for i = 1:length(track_all_locus)
	for j = 1:length(SD_track{i})
		MSD_mean_traj(i,j) = mean(SD_track{i}{j});
		MSD_std_traj(i,j) = std(SD_track{i}{j});
	end
end
toc

% % mean MSD calculation => is the same as long as the samples are the same size !!!
% tic
% disp('mean MSD calculation = ')
% mean_MSD = mean(MSD_mean_traj,);
% 	
	
% calculation of the mean power law and diffusion coefficient
D = (MSD_mean_all(1+DParam)-MSD_mean_all(1))/(timeBar(1+DParam)-timeBar(1));
alphaLength = ceil(min_averaging*length(timeBar)/alphaParam);
alpha = log(MSD_mean_all(alphaLength)/MSD_mean_all(1))/log(timeBar(alphaLength)/timeBar(1));

%% Dump calculated values
% dumpMSDs();

%% plot MSD

% log log
figure; 
subplot(2,2,2)
loglog(timeBar,MSD_mean_all);
title('MSD (loglog)','FontSize',15)
xlabel('DeltaT(sec)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);
legend = sprintf('alpha = %f',alpha);
text(timeBar(2),MSD_mean_all(max_window)*2,legend,'Color','k','BackGround','w');
clear legend;


subplot(2,2,1);
loglog(timeBar,MSD_mean_traj'); hold on; loglog(timeBar,MSD_mean_all,'LineWidth',2)
title('MSD (all traj loglog)','FontSize',15)
xlabel('DeltaT(sec)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);

subplot(2,2,4);
plot(timeBar,MSD_mean_all);
% hold on; plot(timeBar,MSD_median_all);
title('MSD (linear)','FontSize',15)
xlabel('DeltaT(sec)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);
legend = sprintf('D = %eum^2/sec',D);
text(timeBar(10),MSD_mean_all(max_window)*96/100,legend,'Color','k','BackGround','w');


% linear axis, MSD for all DeltaT + separate MSDs
subplot(2,2,3);
plot(timeBar,MSD_mean_traj'); hold on; plot(timeBar,MSD_mean_all,'LineWidth',2)
title('MSD (all traj linear)','FontSize',15)
xlabel('DeltaT(sec)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);

saveas(gcf,strcat('MSDs_loglog_lineaire.png'));
saveas(gcf,strcat('MSDs_loglog_lineaire.fig'));


figure;
% linear axis, MSD for all DeltaT + separate MSDs
% figure;
plot(timeBar,MSD_mean_traj'); hold on; plot(timeBar,MSD_mean_all,'LineWidth',2)
title('MSD (all traj linear)','FontSize',15)
xlabel('DeltaT(sec)','FontSize',10);ylabel('MSD(um^2)','FontSize',10);

for i =1:size(MSD_mean_traj,1)
	text(timeBar(end),MSD_mean_traj(i,end),traj_number(i),'FontSize',12)
end
title('All trajectories');

saveas(gcf,strcat('All_trajectories.png'));
saveas(gcf,strcat('All_trajectories.fig'));

% linear axis, mean MSD vs mean SD


%% autocorrelation of angles


%% bayesian comparison of MSD versus model (free diffusion, constraint and subdiffusion)


%% sliding window for switching behaviour

cd(folderName)

disp('Done !')

% 
% function dumpMSDs(MSDs, MSDmean)
% % 
% %   Detailed explanation goes here
% 
% 
% end


