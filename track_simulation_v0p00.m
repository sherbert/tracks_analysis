function [traj] = track_simulation_v0p00(dim,maxstep)
%
%

traj = zeros(maxstep,dim);
for D = 1:dim
	for step = 2:maxstep
		traj(step,D) = traj(step-1,D)+rand()*20-10;
		if abs(traj(step,D))>30 % confinement
			traj(step,D) = traj(step-1,D);
		end
	end
end


